package de.pidata.models.android;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.models.sql.SQLTableSequence;
import de.pidata.models.sql.DBConnection;
import de.pidata.models.sql.DBCursor;
import de.pidata.models.tree.Context;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.system.base.NumberSequence;
import de.pidata.system.base.SystemManager;

import java.io.IOException;

public class SQLiteConnection extends DBConnection  {

  private SQLiteDatabase sqLite;
  private static final boolean DEBUG = false;

  public SQLiteConnection(  ) {
    super( -1 );
  }

  /**
   * @param propertySection the propertySection to use for readign properties, e.g. "database"
   * @throws Exception
   */
  public void connect( Context context, String propertySection ) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();
    String db_url = sysMan.getProperty( propertySection+".url", null );
    if (db_url == null) {
      throw new IllegalArgumentException( propertySection+".url missing in both property files" );
    }
    android.content.Context androidContext = AndroidApplication.getInstance();
    this.sqLite = androidContext.openOrCreateDatabase( db_url, android.content.Context.MODE_PRIVATE, null );
    this.sqLite.setForeignKeyConstraintsEnabled( true );
  }

  /**
   * Closes current jdbc connection and tries to reconnect with same connection parameters
   *
   * @return true if reconnect was successful
   */
  public boolean reconnect() {
    return true;
  }

  /**
   * Executes the commit on the jdbc connection.
   *
   * @throws java.io.IOException
   */
  public void commit() throws IOException {
    if (sqLite.inTransaction()) {
      sqLite.setTransactionSuccessful();
      sqLite.endTransaction();
    }
    if (DEBUG) Logger.debug( "SQLiteConnection.commit thread="+Thread.currentThread().getId() );
  }

  /**
   * Executes the rollback on the jdbc connection and catches possible SQLException in
   * empty catch block.
   */
  public void rollback() {
    if (sqLite.inTransaction()) {
      sqLite.endTransaction();
    }
    if (DEBUG) Logger.debug( "SQLiteConnection.rollback thread="+Thread.currentThread().getId() );
  }

  public void finishSelect() {
    super.finishSelect();
    if (DEBUG) Logger.debug( "SQLiteConnection.finishSelect thread="+Thread.currentThread().getId() );
  }

  public void close() {
    sqLite.close();
  }

  public DBCursor query( String sql ) throws IOException {
//    if (!sqLite.inTransaction()) {
//      sqLite.beginTransaction();
//    }
    if (DEBUG) Logger.debug( "SQLiteConnection.query thread="+Thread.currentThread().getId()+", SQL="+sql );
    return new SQLiteCursor( this, sqLite.rawQuery( sql, null ) );
  }

  public void execute( String sql ) throws IOException {
    if (!sqLite.inTransaction()) {
      sqLite.beginTransaction();
    }
    if (DEBUG) Logger.debug( "SQLiteConnection.execute thread="+Thread.currentThread().getId()+", SQL="+sql );
    sqLite.execSQL( sql );
  }

  /**
   * Convert the internal representation of a date/time value into the database specific
   * representaion. If using a String it must be surrounded by single quotes.
   *
   * @param dateTimeType
   * @param value        the date value
   * @return the database specific representation of the date/time value
   */
  public String dateTime2SQL( DateTimeType dateTimeType, DateObject value ) {
    if (value == null) {
      return "null";
    }
    else {
      long dateValue = value.getTime();
      return Long.toString( dateValue );
    }
  }

  /**
   * Returns the database specific type for an internal date/time type
   *
   * @param dateTimeType
   * @return the database specific type
   */
  public String getDateTimeType( DateTimeType dateTimeType ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Creates a new Sequence
   *
   * @param name the sequence's name
   * @param min  the min (starting) value for the new sequence
   * @param max  the max value for the new sequence
   * @return the new sequence object
   */
  public NumberSequence createSequence( String name, long min, long max ) throws IOException {
    return new SQLTableSequence( name, this, min, max );
  }

  public NumberSequence getSequence( String name ) throws IOException {
    return new SQLTableSequence( name, this );
  }

  /**
   * Add a limit constraint to the sql string.
   *
   * @param sql
   * @param maxCountResult
   */
  public String addLimit( String sql, int maxCountResult ) {
    return sql + " LIMIT "+maxCountResult;
  }

  /**
   * Returns true if table named tableName exists
   *
   * @param tableName teh tabel to check
   * @return true if tableName exists
   */
  public boolean existsTable( String tableName ) throws IOException {
    Cursor cursor;
    String sql = "SELECT COUNT(*) FROM "+tableName;
    try {
      cursor = sqLite.rawQuery( sql, null );
    }
    catch (SQLiteException ex) {
      return false;
    }
    if (cursor == null) {
      return false;
    }
    else {
      cursor.close();
      return true;
    }
  }

  /**
   * Returns true if this DBConnection supports cascade definition for delete and update
   * like definded by CREATE TABLE ... ON ... CASCADE
   *
   * @return true if this DBConnection supports cascade definition
   */
  public boolean supportsCascade() {
    return false;
  }
}
