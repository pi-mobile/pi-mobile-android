package de.pidata.models.android;

import android.database.Cursor;
import de.pidata.log.Logger;
import de.pidata.models.sql.DBCursor;
import de.pidata.models.types.SimpleType;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.models.types.simple.DecimalType;
import de.pidata.models.types.simple.IntegerType;
import de.pidata.models.types.simple.QNameType;
import de.pidata.models.types.simple.StringType;
import de.pidata.qnames.QName;

import java.io.IOException;

public class SQLiteCursor implements DBCursor {

  private static final boolean DEBUG = false;

  private SQLiteConnection conn;
  private Cursor cursor;

  public SQLiteCursor( SQLiteConnection conn, Cursor cursor ) {
    this.conn = conn;
    this.cursor = cursor;
  }

  /**
   * Moves Cursor to the next data row
   *
   * @return true if a next data row exists, otherwise false
   */
  public boolean next() throws IOException {
    return cursor.moveToNext();
  }

  private int columnIndex( String columnName ) {
    return cursor.getColumnIndex( columnName.toLowerCase() );
  }

  /**
   * Returns string value for given columnName
   *
   * @param columnName column*s name
   * @return String value
   */
  public String getString( String columnName ) throws IOException {
    return cursor.getString( columnIndex( columnName ) );
  }

  /**
   * Returns long value for given columnName
   *
   * @param columnName column*s name
   * @return long value
   */
  public long getLong( String columnName ) throws IOException {
    return cursor.getLong( columnIndex( columnName ) );
  }

  /**
   * Returns the value of the attribute identified by name. The
   * result data type is defined by type.
   *
   * @param name         attribute's name
   * @param type         the result type
   * @param defaultValue returned if attribute name is not defined,
   *                     most be valid for type.
   * @return the value of the attribute identified by name
   */
  public Object getAttributeValue( QName name, SimpleType type, Object defaultValue ) throws IOException {
    Object result = null;
    String columnName = name.getName();

    if (type == null) {
      throw new IllegalArgumentException("Attribute type must not be null.");
    }

    try {
      int columnIndex = columnIndex( columnName );
      if (columnIndex < 0) {
        return defaultValue;
      }
      if (cursor.isNull( columnIndex )) {
        return defaultValue;
      }

      if (type instanceof QNameType) {
        String value = cursor.getString(columnIndex);
        if (value == null) result = null;
        else if (value.length() == 0) result = null;
        else result = conn.getNamespaces().getQName(value);
      }
      else if (type instanceof StringType) {
        result = cursor.getString(columnIndex);
      }
      else if (type instanceof IntegerType) {
        Class valueClass = type.getValueClass();
        if (valueClass.getName().endsWith("Integer")) {
          result = new Integer(cursor.getInt(columnIndex));
        }
        if (valueClass.getName().endsWith("Long")) {
          result = new Long(cursor.getLong(columnIndex));
        }
        if (valueClass.getName().endsWith("Short")) {
          result = new Short(cursor.getShort(columnIndex));
        }
        if (valueClass.getName().endsWith("Byte")) {
          result = new Byte((byte) cursor.getShort(columnIndex));
        }
      }
      else if (type instanceof DecimalType) {
        String strValue = cursor.getString(columnIndex);
        if ((strValue == null) || (strValue.length() == 0)) result = null;
        else result = new DecimalObject(strValue);
      }
      else if (type instanceof BooleanType) {
        boolean value = (cursor.getShort(columnIndex) != 0);
        if (value) result = BooleanType.TRUE;
        else result = BooleanType.FALSE;
      }
      else if (type instanceof DateTimeType) {
        long timeMillis = cursor.getLong( columnIndex );
        result = new DateObject( ((DateTimeType) type).getType(), timeMillis );
      }
      else {
        SimpleType base = (SimpleType) type.getBaseType();
        if (base == null) {
          throw new IllegalArgumentException("not implemented base type");
        }
        result = getAttributeValue( name, base, defaultValue );
      }
    }
    catch (Exception ex) {
      String msg = "Could not get value from result set, column=" + columnName;
      Logger.error(msg, ex);
      conn.rollback();
      throw new IOException(msg);
    }
    return result;
  }

  /**
   * Closes this DBCursor's resources
   */
  public void close() {
    if (DEBUG) Logger.debug( "SQLiteCursor.close thread="+Thread.currentThread().getId() );
    if (cursor != null) {
      cursor.close();
      cursor = null;
    }
  }
}
