/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import de.pidata.gui.android.activity.ExitActivity;
import de.pidata.gui.android.activity.PiMobileActivity;
import de.pidata.gui.android.adapter.AndroidUIAdapter;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.Root;
import de.pidata.system.android.AndroidSystem;
import de.pidata.system.android.FileStorage;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.SystemManager;

import java.util.Date;
import java.util.Hashtable;

public abstract class AndroidApplication extends Application implements Thread.UncaughtExceptionHandler, Application.ActivityLifecycleCallbacks {

  private static final boolean DEBUG_STATES = false;
  
  private static AndroidApplication instance;

  private Activity initialActivity;
  private Activity currentActivity;
  private Context context;
  private boolean exiting = false;

  private Hashtable cache = new Hashtable();

  public static AndroidApplication getInstance() {
    return instance;
  }

  public void onCreate() {
    super.onCreate();
    Logger.info( "AndroidApplication.onCreate: Begin init PI-Mobile" );
    instance = this;
    Thread.setDefaultUncaughtExceptionHandler( this );
    registerActivityLifecycleCallbacks( this );
    initPiMobile();
    Logger.info( "Finished init PI-Mobile" );
  }

  private void initPiMobile() {
    String basePath = getFilesDir().getAbsolutePath();
    String programName = null;
    String programVersion = null;
    String programDate = null;
    try {
      PackageManager packageMan = getPackageManager();
      PackageInfo info = packageMan.getPackageInfo( getPackageName(), 0 );
      programVersion = info.versionName;
      programName = packageMan.getApplicationLabel( info.applicationInfo ).toString();
      programDate = new Date( info.lastUpdateTime ).toString();
    }
    catch (Exception ex) {
      Logger.error( "Error reading android package info", ex );
    }
    try {
      SystemManager sysMan = new AndroidSystem( basePath, new FileStorage( getAssets(), "", basePath ), programName, programVersion, programDate );
      this.context = sysMan.createContext();
      AndroidPlatform platform = new AndroidPlatform( this.context );
      platform.setStartupProgress( 100 );
    }
    catch (Exception ex) {
      Logger.error( "Error initializing PI-Mobile", ex );
    }
  }

  /**
   * Called by AndroidLoader when having finished, i.e. Services and SystemSynchronizer are initialized.
   *
   * This is e.g. the right time to fetch status model from SystemSynchronizer.
   *
   * @param success false if an error occured while loading
   */
  public void finishedLoading( boolean success ) {
    BackgroundSynchronizer sync = BackgroundSynchronizer.getInstance();
    if (sync != null) {
      Model statusModel = sync.getStatusModel();
      Root dataRoot = context.getDataRoot();
      if (dataRoot.childCount( statusModel.type().name() ) == 0) {
        dataRoot.add( statusModel.type().name(), statusModel );
      }
    }
  }

  @Override
  public void onTerminate() {
    super.onTerminate();
    Logger.info( "AndroidApplication.onTerminate" );
  }

  public void exit() {
    if (currentActivity == null) {
      ExitActivity.exitApplication( getApplicationContext() );
    }
    else {
      exiting = true;
      if (!currentActivity.isFinishing()) {
        currentActivity.finish();
      }
    }
  }

  //----------------------- ActivityLifecycleCallbacks -----------------------------------

  @Override
  public void onActivityCreated( Activity activity, Bundle savedInstanceState ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityCreated" );
    this.currentActivity = activity;
    if (initialActivity == null) {
      this.initialActivity = activity;
      AndroidLoader loaderThread = new AndroidLoader( context, this );
      loaderThread.execute();
    }
  }

  @Override
  public void onActivityStarted( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityStarted" );
    synchronized (this) {
      currentActivity = activity;
    }
  }

  @Override
  public void onActivityResumed( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityResumed" );
  }

  @Override
  public void onActivityPaused( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityPaused" );
  }

  @Override
  public void onActivityStopped( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityStopped" );
  }

  @Override
  public void onActivitySaveInstanceState( Activity activity, Bundle outState ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivitySaveInstanceState" );
  }

  @Override
  public void onActivityDestroyed( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in AndroidApplication.onActivityDestroyed" );
    synchronized (this) {
      if (activity == currentActivity) {
        currentActivity = null;
      }
      if (!(activity instanceof ExitActivity) && (activity.isTaskRoot())) {
        exiting = false;
        ExitActivity.exitApplication( getApplicationContext() );
      }
    }
  }

  public PiMobileActivity getCurrentActivity() {
    synchronized (this) {
      if (currentActivity instanceof PiMobileActivity) {
        return (PiMobileActivity) currentActivity;
      }
      else {
        return null;
      }
    }
  }

  //------------------------------------------------------------------

  public Context getContext() {
    return context;
  }

  @Override
  public void uncaughtException( Thread thread, Throwable ex ) {
    Logger.error( "Uncaught exception", ex );
    android.os.Process.killProcess( android.os.Process.myPid() );
    System.exit( 1 );
  }

  public void addToCache( Uri dataUri, Model parameter ) {
    cache.put( dataUri, parameter );
  }

  public void addToCache( Uri dataUri, DialogController parameter ) {
    cache.put( dataUri, parameter );
  }

  public Object getFromCache( Uri dataUri ) {
    return cache.get( dataUri );
  }

  public void removeFromCache( Uri dataUri ) {
    cache.remove( dataUri );
  }
}
