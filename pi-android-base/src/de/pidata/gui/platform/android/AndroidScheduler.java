/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.os.Handler;
import android.os.HandlerThread;
import de.pidata.gui.component.base.PlatformScheduler;

public class AndroidScheduler implements PlatformScheduler {

  private static Handler handler;

  static {
    HandlerThread thread = new HandlerThread("AndroidSchedulerThread");
    thread.start();
    handler = new Handler( thread.getLooper() );
  }

  @Override
  public void post( Runnable task, boolean doLogging ) {
    handler.post( task );
  }

  @Override
  public void postDelayed( Runnable task, long afterMillis, boolean doLogging ) {
    handler.postDelayed( task, afterMillis );
  }
}
