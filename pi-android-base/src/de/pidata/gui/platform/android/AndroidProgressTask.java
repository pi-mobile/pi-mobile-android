/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.os.AsyncTask;
import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.controller.base.ProgressController;
import de.pidata.log.Logger;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public final class AndroidProgressTask extends AsyncTask implements TaskHandler {

  private ProgressController progressController;
  private ProgressTask progressTask;

  private String taskName;

  public AndroidProgressTask( ProgressTask progressTask ) {
    super();
    this.progressTask = progressTask;
    taskName = progressTask.getTaskName();
  }

  @Override
  public void startTask( ProgressController progressController ) {
    this.progressController = progressController;
    execute();
  }

  @Override
  public void abortTask() {
    cancel( true );
  }

  @Override
  public boolean isAbortRequested() {
    return isCancelled();
  }

  /**
   *
   * @param params
   * @return
   */
  @Override
  protected Object doInBackground( Object[] params ) {
    if (progressTask == null) {
      Logger.warn( "no background task provided" );
      return null;
    }
    Logger.info( "start background task [" + taskName + "]" );
    Object result = progressTask.performTask();
    return result;
  }

  public void updateProgress( double progress, String message ) {
    publishProgress( progress, message );
  }

  @Override
  protected void onProgressUpdate( Object[] values ) {
    if (progressController != null) {
      double progress = 0;
      String message = "";
      if (values.length > 0) {
        progress = (Double)values[0];
      }
      if (values.length > 1) {
        message = (String)values[1];
      }
      progressController.setProgress( progress, message );
    }
  }

  @Override
  protected void onPreExecute() {
    Logger.info( "reached pre execute background task [" + taskName + "]" );
    if (progressController != null) {
      progressController.show();
    }
    if (progressTask != null) {
      progressTask.doPrepare();
    }
  }

  @Override
  protected void onPostExecute( Object result ) {
    Logger.info( "reached finish background task [" + taskName + "], result [" + result + "]" );
    if (progressTask != null) {
      progressTask.doFinished( result );
    }
    if (progressController != null) {
      progressController.finish();
    }
  }

  @Override
  protected void onCancelled( Object result ) {
    Logger.info( "reached cancel background task [" + taskName + "], result [" + result + "]" );
    if (progressTask != null) {
      progressTask.doCancelled( result );
    }
    if (progressController != null) {
      progressController.finish();
    }
  }

  @Override
  public String getTaskName() {
    return taskName;
  }
}
