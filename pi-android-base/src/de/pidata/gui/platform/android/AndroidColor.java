/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import de.pidata.gui.component.base.ComponentColor;

import java.util.Objects;

public class AndroidColor implements ComponentColor {

  public static final String CONSTANT_HEX = "#";

  private Integer colorValue;

  public AndroidColor( int colorValue ) {
    this.colorValue = new Integer( colorValue );
  }

  @Override
  public Object getColor() {
    return colorValue;
  }

  @Override
  public void setColor( Object color ) {
    this.colorValue = (Integer) color;
  }

  public int getColorInt() {
    return colorValue.intValue();
  }

  @Override
  public boolean equals( Object o ) {
    if (!(o instanceof AndroidColor)) {
      return false;
    }
    else if (colorValue == null) {
      return (((AndroidColor) o).colorValue == null);
    }
    else {
      return (colorValue.equals( ((AndroidColor) o).colorValue ));
    }
  }

  @Override
  public int hashCode() {
    return Objects.hashCode( colorValue );
  }
}
