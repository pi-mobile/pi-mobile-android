/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.MediaInterface;
import de.pidata.gui.component.base.Platform;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.system.base.Storage;

public class AndroidMedia implements MediaInterface {

  private MediaRecorder mediaRecorder;
  private QName startSoundID;
  private QName stopSoundID;
  private int maxRecordingTime = 30000; // 30s
  MediaPlayer mediaPlayer = null;

  // Das hier fixt die "Mediaplayer completed without being released" Exceptions
  private final MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
    @Override
    public void onCompletion( MediaPlayer mp ) {
      if ( mp == mediaPlayer ) {
        mediaPlayer = null;
      }
      mp.reset();
      mp.release();
    }
  };

  @Override
  public void playSound( QName soundID ) {
    AndroidDialog androidDialog = (AndroidDialog) Platform.getInstance().getScreen().getFocusDialog();
    Activity activity = androidDialog.getActivity();
    int soundIdInt = UIFactoryAndroid.getRessourceID( "raw", soundID );
    mediaPlayer = MediaPlayer.create( activity, soundIdInt );
    mediaPlayer.setOnCompletionListener( onCompletionListener );
    mediaPlayer.setVolume( 100.0f, 100.0f );
    mediaPlayer.start();
  }

  // Das hier hätte eine status-Abfrage werden sollen, um das feste Intervall für Warntöne in der EQXX-App loszuwerden.
  // Problem:
  // Der obige OnCompletionListener wird verzögert aufgerufen, d.h. isPlaying() gibt noch eine Weile lang "true" zurück,
  // obwohl der Sound schon fertig ist.
  // Man kann das also nicht verwenden, um so schnell wie möglich den nächsten Sound abzuspielen.
  public boolean isPlaying() {
    return mediaPlayer != null && mediaPlayer.isPlaying();
  };

  @Override
  public void initRecording( QName startSoundID, QName stopSoundID, int maxRecordingTime ) {
    this.startSoundID = startSoundID;
    this.stopSoundID = stopSoundID;
    this.maxRecordingTime = maxRecordingTime;
  }

  @Override
  public boolean isRecording() {
    return mediaRecorder != null;
  }

  @Override
  public boolean startRecording( Storage recordingStorage, String fileName ) {
    try {
      AndroidDialog androidDialog = (AndroidDialog) Platform.getInstance().getScreen().getFocusDialog();
      int permission = androidDialog.getActivity().checkSelfPermission( Manifest.permission.RECORD_AUDIO );
      if(permission != PackageManager.PERMISSION_GRANTED){
       androidDialog.getActivity().requestPermissions( new String[]{Manifest.permission.RECORD_AUDIO}, 0 );
       return false;
      }

      mediaRecorder = new MediaRecorder();

      mediaRecorder.setAudioSource( MediaRecorder.AudioSource.MIC );
      mediaRecorder.setOutputFormat( MediaRecorder.OutputFormat.MPEG_4 );
      mediaRecorder.setOutputFile( recordingStorage.getPath( fileName ) );
      mediaRecorder.setAudioEncoder( MediaRecorder.AudioEncoder.AAC );
      mediaRecorder.setMaxDuration( maxRecordingTime );
      mediaRecorder.prepare();

      mediaRecorder.setOnInfoListener( new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo( MediaRecorder mr, int what, int extra ) {
          if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
            Logger.info("Voice recording: Maximum Duration Reached");
            if (stopSoundID != null) {
              playSound( stopSoundID );
            }
          }
        }
      } );

      if (startSoundID != null) {
        playSound( startSoundID );
      }
      mediaRecorder.start();
      return true;
    }
    catch (Exception e) {
      Logger.error( "Error starting mediaRecorder", e );
      if (mediaRecorder != null) {
        mediaRecorder.release();
        mediaRecorder = null;
      }
      return false;
    }
  }

  @Override
  public void stopRecording() {
    try {
      if (mediaRecorder != null) {
        mediaRecorder.stop();
      }
    }
    catch (IllegalStateException e) {
      Logger.error( "Error stopping mediaRecorder", e );
    }
    finally {
      if (mediaRecorder != null) {
        mediaRecorder.release();
        mediaRecorder = null;
      }
      if (stopSoundID != null) {
        playSound( stopSoundID );
      }
    }
  }
}
