/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.PowerManager;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import de.pidata.connect.android.bluetooth.SPPConnectionAndroid;
import de.pidata.connect.bluetooth.SPPConnection;
import de.pidata.connect.stream.MessageSplitter;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.component.AndroidBitmap;
import de.pidata.gui.android.controller.AndroidDialogControllerBuilder;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.android.AndroidScreen;
import de.pidata.gui.component.base.*;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.layout.Layouter;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.log.Logger;
import de.pidata.models.config.ConfigFactory;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.DeviceTable;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.system.android.AndroidConnectionController;
import de.pidata.connect.base.ConnectionController;
import de.pidata.system.android.AndroidNfcTool;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.Storage;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;
import java.util.Set;

public class AndroidPlatform extends Platform implements EventListener {

  private ComponentFactory compFactory;
  private AndroidDialogControllerBuilder controllerBuilder;
  private UIFactory   uiFactory;
  private AndroidScreen screen = new AndroidScreen();
  private PowerManager.WakeLock syncWakeLock;
  private Hashtable colors = new Hashtable();
  private boolean exiting = false;
  private MediaInterface mediaInterface;

  public AndroidPlatform( Context context ) throws Exception {
    //this.compFactory = new AndroidCompFactory();
    String[] args = new String[1];
    args[0] = "Application";
    init( context, args );
  }

  @Override
  protected void init( Context context, String[] args ) throws Exception {
    super.init( context, args );
    colors.put(ComponentColor.BLACK, new AndroidColor( Color.BLACK));
    colors.put(ComponentColor.GRAY, new AndroidColor(Color.GRAY));
    colors.put(ComponentColor.WHITE, new AndroidColor(Color.WHITE));
    colors.put(ComponentColor.BLUE, new AndroidColor(Color.BLUE));
    colors.put(ComponentColor.LIGHTGRAY, new AndroidColor(Color.LTGRAY));
    colors.put(ComponentColor.RED, new AndroidColor(Color.RED));
    colors.put(ComponentColor.ORANGE, new AndroidColor(Color.rgb(255,165,0)));
    colors.put(ComponentColor.CYAN, new AndroidColor(Color.CYAN));
    colors.put(ComponentColor.MAGENTA, new AndroidColor(Color.MAGENTA));
    colors.put(ComponentColor.GREEN, new AndroidColor(Color.GREEN));
    colors.put(ComponentColor.PINK, new AndroidColor(Color.rgb(255,192,203)));
    colors.put(ComponentColor.YELLOW, new AndroidColor(Color.YELLOW));
    colors.put(ComponentColor.DARKGRAY, new AndroidColor(Color.DKGRAY));
    colors.put(ComponentColor.DARKGREEN, new AndroidColor(Color.rgb(0,178,0)));
    colors.put(ComponentColor.TRANSPARENT, new AndroidColor( Color.TRANSPARENT) );

  }

  /**
   * Returns this platform's name
   *
   * @return this platform's name
   */
  public String getPlatformName() {
    return "AndroidPlatform";
  }

  protected void initComm( Context context ) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();

    String connCtrl = sysMan.getProperty("comm.connCtrl", null );
    if ((connCtrl == null) || (connCtrl.length() == 0)) {
      sysMan.setConnectionController( null );
    }
    else if ("DEFAULT".equals(connCtrl)) {
      sysMan.setConnectionController( new AndroidConnectionController( context ) );
    }
    else {
      sysMan.setConnectionController( (ConnectionController) Class.forName(connCtrl).newInstance() );
    }
    BackgroundSynchronizer synchronizer = BackgroundSynchronizer.getInstance();
    if (synchronizer != null) {
      Model statusModel = synchronizer.getStatusModel();
      statusModel.addListener( this );
    }
  }

  public ControllerBuilder getControllerBuilder() {
    if (controllerBuilder == null) {
      controllerBuilder = new AndroidDialogControllerBuilder();
    }
    return controllerBuilder;
  }

  protected void openInitialDialog( Context context, QName opID ) {
    // do nothing
  }

  /**
   * Returns the platform specific statup file name for programName.
   * For example a Windows desktop platfrom appends programName with ".bat"
   *
   * @param programName the program name
   * @return the platform specific statup file name for programName
   */
  public String getStartupFile( String programName ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the sorage wher startfiles are placed on this platform
   *
   * @return the sorage wher startfiles are placed on this platform
   */
  public Storage getStartupFileStorage() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Creates ComponentBitmap with platform dependant Graphics of size width x height
   *
   * @param width  the new ComponentBitmap's width
   * @param height the new ComponentBitmap's height
   * @return a platform depandant ComponentBitmap
   */
  public ComponentBitmap createBitmap( int width, int height ) {
    Bitmap bm = Bitmap.createBitmap( width, height, null );
    return new AndroidBitmap( bm );
  }

  /**
   * Loads a bitmap from the given file system path.
   *
   * @param path the path of a image file (gif or jpeg)
   * @return the bitmap loaded
   */
  public ComponentBitmap loadBitmapFile( String path ) {
    if ((path == null) || (path.length() == 0)) {
      return null;
    }
    if (path.startsWith( "/" )) {
      File file = new File( path );
      if (file.exists()) {
        InputStream imageStream = null;
        try {
          imageStream = new FileInputStream( path );
          return loadBitmap( imageStream );
        }
        catch (IOException ex) {
          Logger.error( "Could not load image, path=" + path, ex );
        }
        finally {
          StreamHelper.close( imageStream );
        }
      }
    }
    else {
      Storage storage = SystemManager.getInstance().getStorage( null );
      if (storage.exists( path )) {
        InputStream imageStream = null;
        try {
          imageStream = storage.read( path );
          return loadBitmap( imageStream );
        }
        catch (IOException ex) {
          Logger.error( "Could not load image, path=" + path, ex );
        }
        finally {
          StreamHelper.close( imageStream );
        }
      }
    }
    return null;
  }

  public ComponentBitmap loadBitmap( InputStream imageStream ) {
    Bitmap bm = BitmapFactory.decodeStream( imageStream );
    if (bm == null) {
      return Platform.getInstance().getBitmap( GuiBuilder.NAMESPACE.getQName( "icons/missing.png" ) );
    }
    return new AndroidBitmap( bm );
  }

  @Override
  public ComponentBitmap loadBitmapResource( QName bitmapID ) {
    try {
      int ressourceID = UIFactoryAndroid.getRessourceID( bitmapID );

      Resources resources = AndroidApplication.getInstance().getCurrentActivity().getResources();
      Bitmap bitmap = BitmapFactory.decodeResource( resources, ressourceID );
      return new AndroidBitmap( bitmap );
    }
    catch (Exception ex) {
      Logger.error( "could not load resource bitmapID=" + bitmapID, ex );
      return null;
    }
  }

  @Override
  public ComponentBitmap loadBitmapAsset( QName bitmapID ) {
    String path = bitmapID.getName();
    try {
      AssetManager assetManager =  AndroidApplication.getInstance().getAssets();
      InputStream bitmapStream = assetManager.open( path );
      return loadBitmap( bitmapStream );
    }
    catch (Exception ex) {
      Logger.warn( "Could not load asset bitmap from path=" + path + ", ex=" + ex.toString() );
      return null;
    }
  }

  private int calculateInSampleSize( BitmapFactory.Options options, int reqWidth, int reqHeight ) {
    // Raw height and width of image
    int height = options.outHeight;
    int width = options.outWidth;
    int inSampleSize = 1;

    if (height > reqHeight || width > reqWidth) {
      int halfHeight = height / 2;
      int halfWidth = width / 2;

      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // height and width larger than the requested height and width.
      while ((halfHeight / inSampleSize) > reqHeight
          && (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }
    return inSampleSize;
  }

  /**
   * Loads a thumb nail image, fitting into rectangle width, height
   *
   * @param imageStorage  storage conataining the image
   * @param imageFileName image file name withing storage
   * @param width         desired with
   * @param height        desired height
   * @return thumb nail for image from imageSteam
   */
  @Override
  public ComponentBitmap loadBitmapThumbnail( Storage imageStorage, String imageFileName, int width, int height ) {
    try {
      // First decode with inJustDecodeBounds=true to check dimensions
      final BitmapFactory.Options options = new BitmapFactory.Options();
      options.inJustDecodeBounds = true;
      InputStream imageStream = imageStorage.read( imageFileName );
      BitmapFactory.decodeStream( imageStream, null, options );
      imageStream.close();

      // Calculate inSampleSize
      options.inSampleSize = calculateInSampleSize( options, width, height );

      // Decode bitmap with inSampleSize set
      options.inJustDecodeBounds = false;
      imageStream = imageStorage.read( imageFileName );
      Bitmap bm = BitmapFactory.decodeStream( imageStream, null, options );
      imageStream.close();
      return new AndroidBitmap( bm );
    }
    catch (Exception ex) {
      Logger.error( "Exception loading image name="+imageFileName, ex );
      return null;
    }
  }

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @return
   */
  public ComponentColor getColor( int red, int green, int blue ) {
    return new AndroidColor( Color.rgb( red, green, blue ) );
  }

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  public ComponentColor getColor( int red, int green, int blue, double alpha ) {
    int alphaInt = (int) alpha * 255;
    return new AndroidColor( Color.argb( alphaInt, red, green, blue ) );
  }

  public ComponentColor getColor( QName colorID ) {
    ComponentColor color = (ComponentColor) this.colors.get(colorID);
    if (color == null) {
      int colorResID = UIFactoryAndroid.getRessourceID( "color", colorID );
      if (colorResID > 0) {
        int colInt = AndroidApplication.getInstance().getResources().getColor( colorResID );
        color = new AndroidColor( colInt );
        this.colors.put( colorID, color );
      }
      else {
        throw new IllegalArgumentException( "Unknown color ID=" + colorID );
      }
    }
    return color;
  }

  public void setColor( QName colorID, String value ) {
    AndroidColor color = (AndroidColor) getColor( value );
    colors.put( colorID, color );
  }

  @Override
  public ComponentColor setColor( QName name, int red, int green, int blue, double alpha ) {
    int alphaInt = (int) alpha * 255;
    AndroidColor color = new AndroidColor( Color.argb( alphaInt, red, green, blue ) );
    colors.put( name, color );
    return color;
  }

  public void setFont( QName fontID, String name, QName style, int size ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public ComponentFont createFont( String name, QName style, int size ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public ComponentFont getFont( QName fontID ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this platform's screen object
   *
   * @return this platform's screen object
   */
  public Screen getScreen() {
    while (screen == null) {
      try {
        Thread.sleep(1000);
      }
      catch (InterruptedException e) {
        e.printStackTrace();
        break;
      }
    }
    return this.screen;
  }

  public ComponentFactory getComponentFactory() {
    return this.compFactory;
  }

  @Override
  protected GuiBuilder createGuiBuilder() throws Exception {
    String builderClassName = SystemManager.getInstance().getProperty( "gui.builder", null );
    if ((builderClassName != null) && (builderClassName.length() > 0)) {
      return (GuiBuilder) Class.forName( builderClassName ).newInstance();
    }
    else {
      return null;
    }
  }

  @Override
  public UIFactory getUiFactory() {
    uiFactory = UIFactoryAndroid.getInstance();
    if (uiFactory == null) {
      uiFactory = new UIFactoryAndroid();
    }
    return uiFactory;
  }

  public InputManager getInputManager() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set some platform characteristics, e.g. make skinDialog focusable in JDK 1.4,
   * toggle keyboard on screen.
   * This method is needed since j2sdk1.4.* requires
   * calling setFocusable on Windows to allow keyboard input.
   *
   * @param dialog the dialog to make focusable
   */
  public void initWindow( Object dialog ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows or hides the on screen keyboard
   *
   * @param hasFocus if true the on screen keyboard is shown, otherwise hidden
   */
  public void toggleKeyboard( boolean hasFocus ) {
    AndroidDialog dialog = (AndroidDialog) screen.getFocusDialog();
    Activity activity = dialog.getActivity();
    InputMethodManager imm = (InputMethodManager) activity.getSystemService( android.content.Context.INPUT_METHOD_SERVICE);
    View focusView = activity.getCurrentFocus();
    if (hasFocus) {
      dialog.getActivity().getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE );
      if (focusView != null) {
        imm.showSoftInput( activity.getCurrentFocus(), 0 );
      }
    }
    else {
      if (focusView != null) {
        imm.hideSoftInputFromWindow( focusView.getWindowToken(), 0 );
      }
    }
  }

  public boolean isSingleWindow() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public boolean useDoubleBuffering() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the platform specific color specified by the given colorString.
   * The colorString should be in a platform neutral format which can be interpreted by all platforms. Be careful
   * in using platform specific formats here!
   *
   * @param colorString
   * @return
   */
  public ComponentColor getColor( String colorString ) {
    if (colorString == null) {
      return null;
    }
    else if (colorString.equals( "none" )) {
      return null;
    }
    else {
      String androidColorString;
      if (colorString.startsWith( ComponentColor.CONSTANT_HEX )) {
        if (colorString.length() == 10) {
          // contains alpha: standard is last pair, android wants it as first pair
          androidColorString = AndroidColor.CONSTANT_HEX + colorString.substring( 8 ) + colorString.substring( 2, 8 );
        }
        else {
          androidColorString = colorString.replace( ComponentColor.CONSTANT_HEX, AndroidColor.CONSTANT_HEX );
        }
      }
      else {
        androidColorString = colorString;
      }
      return new AndroidColor( Color.parseColor( androidColorString ) );
    }
  }

  @Override
  public boolean hasTableFirstRowForEmptySelection() {
    return false;
  }

  /**
   *
   */
  public Dialog createDialog( Layouter layouterX, Layouter layouterY, short x, short y, short width, short height ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this platform's navigation capability: full keyboard, 4-way or 2-way
   *
   * @return one of the constants NAVKEYS_*
   */
  public QName getNavigationKeys() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public boolean isExiting() {
    return exiting;
  }

  /**
   * Called to exit application, e.g. via System.exit(0) on J2SE desktop
   */
  public void exit( Context context ) {
    exiting = true;
    AndroidApplication.getInstance().exit();
  }

  /**
   * Returns true if FontMetrics.charWidth() is working on this Platform. On some platforms
   * charWidth returns 0 on others (e.g. J0 on WinMobile 5) it takes more than 500ms per character.
   *
   * @return true if FontMetrics.charWidth() is working on this Platform
   */
  public boolean isCharWidthUsable() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public void setConfigurator2( Configurator configurator ) throws Exception {
    setConfigurator( configurator );
  }

  public PlatformScheduler createScheduler() {
    return new AndroidScheduler();
  }

  /**
   * Adds paired Bluetooth devices to deviceTable
   *
   * @param deviceTable table for the resultui
   */
  public void getPairedBluetoothDevices( DeviceTable deviceTable ) {

    BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    if ( btAdapter != null ) {
      Set<BluetoothDevice> bondedDevices = btAdapter.getBondedDevices();

      for (BluetoothDevice btDevice : bondedDevices) {

        String deviceName = btDevice.getName();
        String deviceAlias = null;
        try {
          Method method = btDevice.getClass().getMethod( "getAliasName" );
          if (method != null) {
            deviceAlias = (String) method.invoke( btDevice );
          }
        }
        catch (Exception e) {
          Logger.error( "Error reading BT device alias", e );
        }

        deviceTable.addBluetoothDevice( new de.pidata.models.config.BluetoothDevice( ConfigFactory.NAMESPACE.getQName( btDevice.getAddress() ), deviceName, deviceAlias ) );
      }
    }
  }

  @Override
  public SPPConnection createBluetoothSPPConnection( String serverAddress, String uuidString, MessageSplitter messageSplitter ) {
    return new SPPConnectionAndroid( serverAddress, uuidString, messageSplitter );
  }

  @Override
  public MediaInterface getMediaInterface() {
    if (mediaInterface == null) {
      mediaInterface = new AndroidMedia();
    }
    return mediaInterface;
  }

  @Override
  public void runOnUiThread( Runnable runnable ) {
    Activity currentActivity = AndroidApplication.getInstance().getCurrentActivity();
    if (currentActivity != null) {
      // CurrentActivity may be null for a moment while switching form one to another
      currentActivity.runOnUiThread( runnable );
    }
  }

  @Override
  public boolean openBrowser( String url ) {
    try {
      Intent browserIntent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
      AndroidApplication.getInstance().getCurrentActivity().startActivity( browserIntent );
      return true;
    }
    catch (Exception ex) {
      Logger.error( "Error opening URL in Browser, url="+url );
      return false;
    }
  }

  @Override
  public NfcTool getNfcTool() {
    return new AndroidNfcTool();
  }

  /**
   * An event occurred.
   *
   * @param eventSender the sender of this event
   * @param eventID     the id of the event
   * @param source      the instance where the event occurred on, may be a child of eventSender
   * @param modelID     the id of the affected model
   * @param oldValue    the old value of the model
   * @param newValue    the new value of the model
   */
  @Override
  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue ) {
    if (eventID == EventListener.ID_MODEL_DATA_CHANGED) {
      BackgroundSynchronizer synchronizer = BackgroundSynchronizer.getInstance();
      if (synchronizer != null) {
        if ((modelID == synchronizer.getStateAttrID()) && (source == synchronizer.getStatusModel())) {
          if (synchronizer.isStateActive( (QName) newValue )) {
            if (syncWakeLock == null) {
              AndroidDialog dialog = (AndroidDialog) screen.getFocusDialog();
              if (dialog != null) {
                Activity activity = dialog.getActivity();
                PowerManager pm = (PowerManager) activity.getSystemService( android.content.Context.POWER_SERVICE );
                syncWakeLock = pm.newWakeLock( PowerManager.PARTIAL_WAKE_LOCK, "PI-Mobile Sync" );
                syncWakeLock.acquire();
                Logger.info( "WakeLock for background sync. acquired" );
              }
            }
          }
          else {
            if (syncWakeLock != null) {
              syncWakeLock.release();
              Logger.info( "WakeLock for background sync. released" );
              syncWakeLock = null;
            }
          }
        }
      }
    }
  }
}
