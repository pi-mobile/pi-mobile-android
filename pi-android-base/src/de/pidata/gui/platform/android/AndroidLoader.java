/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.platform.android;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Looper;
import de.pidata.gui.android.activity.PiMobileActivity;
import de.pidata.gui.component.base.Platform;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.system.base.BackgroundSynchronizer;

public class AndroidLoader extends AsyncTask<Void, Integer, Void> {

  private AndroidApplication androidApplication;
  private AssetManager assetMgr;
  private Model data;
  private Context context;

  public AndroidLoader( Context context, AndroidApplication androidApplication ) {
    this.context = context;
    this.androidApplication = androidApplication;
    android.content.Context androidContext = AndroidApplication.getInstance();
    this.assetMgr = androidContext.getAssets();
  }

  public Model getData() {
    return data;
  }

  /**
   * Runs on the UI thread after {@link #publishProgress} is invoked.
   * The specified values are the values passed to {@link #publishProgress}.
   *
   * @param values The values indicating progress.
   * @see #publishProgress
   * @see #doInBackground
   */
  @Override
  protected void onProgressUpdate( Integer... values ) {
    //set the current progress of the progress dialog
  }

  private void progress( int percent ) {
    Platform platform = Platform.getInstance();
    if (platform == null) {
      Logger.info( "Startup progress: "+percent+"%" );
    }
    else {
      publishProgress( percent );
    }
  }

  /**
   * Runs on the UI thread before {@link #doInBackground}.
   *
   * @see #onPostExecute
   * @see #doInBackground
   */
  @Override
  protected void onPreExecute() {
  }

  /**
   * Override this method to perform a computation on a background thread. The
   * specified parameters are the parameters passed to {@link #execute}
   * by the caller of this task.
   * <p>
   * This method can call {@link #publishProgress} to publish updates
   * on the UI thread.
   *
   * @param voids The parameters of the task.
   * @return A result, defined by the subclass of this task.
   * @see #onPreExecute()
   * @see #onPostExecute
   * @see #publishProgress
   */
  @Override
  protected Void doInBackground( Void... voids ) {
    try {
      Looper.prepare(); // Necessary to allow creating instances of android.os.Handler
      progress( 20 );
      Platform.getInstance().loadServices( context );

      progress( 90 );

      //----- Start system synchronizer
      BackgroundSynchronizer sync = BackgroundSynchronizer.getInstance();
      if (sync == null) {
        Logger.debug("synchronizer is disabled");
      }
      else {
        Logger.debug("starting synchronizer...");
        sync.setUncaughtExceptionHandler( AndroidApplication.getInstance() );
        sync.start();  //TODO create SyncListener instance
        Logger.debug("synchronizer started...");
      }
      progress( 100 );
      androidApplication.finishedLoading( true );
    }
    catch (Exception e) {
      Logger.error( "Exception loading pimobile", e );
      androidApplication.finishedLoading( false );
    }
    return null;
  }

  /**
   * <p>Runs on the UI thread after {@link #doInBackground}. The
   * specified result is the value returned by {@link #doInBackground}.</p>
   *
   * <p>This method won't be invoked if the task was cancelled.</p>
   *
   * @param aVoid The result of the operation computed by {@link #doInBackground}.
   * @see #onPreExecute
   * @see #doInBackground
   * @see #onCancelled(Object)
   */
  @Override
  protected void onPostExecute( Void aVoid ) {
  }
}
