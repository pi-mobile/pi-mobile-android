/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.view.*;
import android.widget.*;
import de.pidata.gui.android.activity.PiFragment;
import de.pidata.gui.android.adapter.*;
import de.pidata.gui.android.component.*;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.*;
import de.pidata.gui.view.base.*;
import de.pidata.gui.view.figure.*;
import de.pidata.gui.view.figure.path.PathElement;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.binding.SimpleModelSource;
import de.pidata.models.tree.Model;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.rect.*;
import de.pidata.string.Helper;
import de.pidata.system.android.tree.AndroidTreeView;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Properties;

import static android.view.View.NO_ID;

/**
 * Factory used to create UIAdapter components and load views by view definition IDs
 */
public class UIFactoryAndroid implements UIFactory {

  public static final Namespace NAMESPACE_GUI     = ControllerBuilder.NAMESPACE;
  public static final Namespace NAMESPACE_ANDROID = Namespace.getInstance( "http://schemas.android.com/apk/res/android" );
  public static final Namespace NAMESPACE_APP = Namespace.getInstance( "http://schemas.android.com/apk/res-auto" );

  public static final String IDCLASS_LAYOUT   = "layout";
  public static final String IDCLASS_ID       = "id";
  public static final String IDCLASS_DRAWABLE = "drawable";
  public static final String IDCLASS_STRING   = "string";
  public static final String IDCLASS_MENU     = "menu";

  protected static UIFactoryAndroid instance;

  public UIFactoryAndroid() {
    instance = this;
  }

  public static UIFactoryAndroid getInstance() {
    return instance;
  }

  public static int getRessourceID( String idSubClass, QName id ) {
    Namespace ns = id.getNamespace();
    String fieldName = id.getName();

    String prefix;
    if (ns == NAMESPACE_ANDROID) {
      prefix = "android";
    }
    else {
      // Get the prefix from the main activity, which always should match the default namespace.
      // If we use flavors the applicationID which is returned from AndroidApplication.getPackageName()
      // might have a suffix.
      String mainClassName = AndroidApplication.getInstance().getApplicationInfo().className;
      prefix = mainClassName.substring( 0, mainClassName.lastIndexOf( '.' )  );
    }
    //TODO weitere Namepaces auflösen

    try {
      Class resClass = Class.forName( prefix + ".R$" + idSubClass );
      Field field = resClass.getField( fieldName );
      return field.getInt( null );
    }
    catch (Exception ex) {
      String msg = "Could not get value for ID constant, name="+fieldName;
      Logger.error( msg + ": "  + ex.getMessage() );
      // NO_ID is the default value for not found ressource ids and used in View.findViewById()
      return NO_ID;
    }
  }

  public View createView( QName viewDefID, ViewGroup parent, boolean attachToParent ) {
    Context context = AndroidApplication.getInstance().getApplicationContext();
    LayoutInflater inflater = (LayoutInflater)context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
    View view = inflater.inflate( getRessourceID( IDCLASS_LAYOUT, viewDefID ), parent, attachToParent );
    return view;
  }

  public PiFragment createFragment( ModuleGroup moduleGroup, ModuleViewPI moduleViewPI ) {
    PiFragment piFragment = new PiFragment();
    piFragment.init( moduleGroup, moduleViewPI, moduleGroup.getModel() );
    return piFragment;
  }

  public void updateView( View view, Model model, AndroidListAdapter androidListAdapter ) {
    if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      for (int i = 0; i < group.getChildCount(); i++) {
        updateView( group.getChildAt( i ), model, androidListAdapter );
      }
    }
    else {
      Object value = null;
      ColumnInfo columnInfo = null;
      boolean hasBinding = true;
      AndroidCellAdapter cellAdapter = androidListAdapter.findCellAdapter( view.getId() );
      ListViewPI viewPI = androidListAdapter.getListViewPI();
      if (cellAdapter == null) {
        if (viewPI instanceof TableViewPI) {
          // For a table it shall be possible to have static views (e.g. labels) in the row. So if we don't have a
          // cell adapter we do not modify the view, especially do not try to fetch a value.
          return;
        }
        else {
          String tagStr = (String) view.getTag();
          QName valueID = tag2QName( tagStr, androidListAdapter.getViewPI().getController().getControllerGroup().getDialogController().getNamespaceTable() );

          if (viewPI == null) {
            value = null;
          }
          else {
            value = viewPI.getCellValue( model, valueID );
          }
          value = StringRenderer.getDefault().render( value );
        }
      }
      else {
        columnInfo = cellAdapter.getColumnInfo();
        QName rowColorID = viewPI.getRowColorID();
        if (rowColorID != null) {
          QName colorColumnID = viewPI.getColorColumnID();
          // if color column is defined, set color only for that column
          // else set color for whole row
          if (colorColumnID == null || columnInfo.getColName().equals( colorColumnID )) {
            Object rowColor = model.get( rowColorID );
            if (!Helper.isNullOrEmpty( rowColor )) {
              AndroidColor androidColor;
              if (rowColor instanceof QName) {
                androidColor = (AndroidColor) Platform.getInstance().getColor( (QName) rowColor );
              }
              else {
                androidColor = (AndroidColor) Platform.getInstance().getColor( rowColor.toString() );
              }
              view.setBackgroundColor( androidColor.getColorInt() );
            }
          }
        }

        Controller renderCtrl = columnInfo.getRenderCtrl();
        if ((renderCtrl != null) && (renderCtrl instanceof ValueController)) {
          ValueController valueController = (ValueController) renderCtrl;
          Binding renderBinding = valueController.getValueBinding();
          if (renderBinding != null) {
            ModelSource modelSource = new SimpleModelSource( null, model );
            renderBinding.setModelSource( modelSource, renderBinding.getModelPath() );
            value = valueController.getValueBinding().fetchModelValue();
            if (view instanceof TextView) {
              value = valueController.render( value );
            }
          }
          else {
            value = valueController.render( columnInfo.getCellValue( model ) );
          }
        }
        else {
          value = null;
        }
      }

      if (view instanceof ImageView) {
        if (columnInfo != null) {
          if (columnInfo.getValueID() == null) {
            QName iconValueID = columnInfo.getIconValueID();
            if(iconValueID != null){
              value = columnInfo.getIconValue( model );
            }
          }
          else {
            value = columnInfo.getCellValue( model );
          }
        }
        updateImageView( (ImageView) view, value );
      }
      else if (view instanceof CheckBox) {
        updateCheckBox( (CheckBox) view, (Boolean) value );
      }
      else if (view instanceof TextView) {
        if (hasBinding) {
          //a button is also a text view, but if the button has no binding, its lable will be overriden by ""
          updateTextView( (TextView) view, value );
        }
      }
      else {
        throw new IllegalArgumentException( "Unsupported view class="+view.getClass() );
      }
      if (view instanceof ImageButton || view instanceof Button){
        int visibility = View.VISIBLE;
        if(columnInfo != null &&
            ( columnInfo.getValueID() != null
                || columnInfo.getIconValueID() != null
                || columnInfo.getLabelValueID() != null)){
          if (value == null) {
            visibility = View.INVISIBLE;
          }
        }
        view.setVisibility( visibility );
      }
    }
  }

  public static QName tag2QName( String tagStr, NamespaceTable nsTable ) {
    QName valueID;
    if (tagStr == null) {
      valueID = null;
    }
    else {
      if (tagStr.indexOf( ':' ) < 0) {
        valueID = NAMESPACE_GUI.getQName( tagStr );
      }
      else {
        valueID = QName.getInstance( tagStr, nsTable );
      }
    }
    return valueID;
  }

  private void updateCheckBox( CheckBox view, Boolean value ) {
    if (value != null) {
      view.setChecked( value.booleanValue() );
    }
    else {
      view.setChecked( false );
    }
  }

  public static void updateImageView( ImageView view, Object value ) {
    if (value == null) {
      view.setImageResource( 0 );
    }
    else {
      if ((value instanceof QName) && ((QName) value).getName().startsWith( "@" )) {
        int resId = getRessourceID((QName)value);
        view.setImageResource( resId );
      }
      else if (value instanceof ComponentBitmap) {
        ComponentBitmap componentBitmap = (ComponentBitmap) value;
        Bitmap bitmap = (Bitmap) componentBitmap.getImage();
        view.setImageBitmap( bitmap );
      }
      else if (value instanceof Binary) {
        byte[] bytes = ((Binary) value).getBytes();
        Bitmap bitmap = BitmapFactory.decodeByteArray( bytes, 0, bytes.length );
        view.setImageBitmap( bitmap );
      }
      else {
        String path = value.toString();
        if (path.length() == 0) {
          view.setImageResource( 0 );
        }
        else {
          QName bitmapID;
          if(value instanceof QName){
            bitmapID = (QName) value;
          }
          else {
            bitmapID = GuiBuilder.NAMESPACE.getQName( value.toString() );
          }
          ComponentBitmap componentBitmap = Platform.getInstance().getBitmap( bitmapID );
          if (componentBitmap == null) {
            Logger.info( "Bitmap not found: " + value.toString() );
          }
          else {
            Bitmap bitmap = (Bitmap) componentBitmap.getImage();
            if (bitmap == null) {
              Logger.info( "Cannot get Bitmap from ComponentBitmap: " + value.toString() );
            }
            else {
              view.setImageBitmap( bitmap );
            }
          }
        }
      }
    }
  }

  public static int getRessourceID( QName value ) {
    String fieldName = value.getName();
    if (fieldName.startsWith( "@" )) {
      String idClass = fieldName.substring( fieldName.indexOf("@")+1, fieldName.indexOf("/") );
      QName qName = ControllerBuilder.NAMESPACE.getQName( fieldName.substring( fieldName.indexOf("/")+1 ));
      return getRessourceID( idClass, qName );
    }
    // Compatibility to deprecated method using a QName(String) without @class/ e.g.: @drawable/
    else {
      return getRessourceID( IDCLASS_DRAWABLE, value );
    }
  }

  private void updateTextView( TextView view, Object value ) {
    if (value == null) {
      view.setText( "" );
    }
    else {
      view.setText( value.toString() );
    }
  }

  @Override
  public UIButtonAdapter createButtonAdapter( UIContainer uiContainer, ButtonViewPI buttonViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( buttonViewPI.getComponentID() );
    if (androidView == null) {
      MenuItem menuItem = ((AndroidUIContainer) uiContainer).findMenuItem( buttonViewPI.getComponentID() );
      if (menuItem == null) {
        throw new IllegalArgumentException( "Could not find view or menu item ID=" + buttonViewPI.getComponentID() );
      }
      else {
        return new AndroidMenuItemAdapter( buttonViewPI, menuItem, (AndroidDialog) uiContainer, uiContainer );
      }
    }
    else {
      if (androidView instanceof Button) {
        return new AndroidButtonAdapter( buttonViewPI, (Button) androidView, uiContainer );
      }
      else if (androidView instanceof ImageButton) {
        return new AndroidImageButtonAdapter( buttonViewPI, (ImageButton) androidView, uiContainer );
      }
      else if (androidView instanceof SearchView) {
        return new AndroidSearchViewAdapter( buttonViewPI, (SearchView) androidView, uiContainer );
      }
      else {
        throw new IllegalArgumentException( "Type not supported for item with ID=" + buttonViewPI.getComponentID() );
      }
    }
  }

  @Override
  public UITextAdapter createTextAdapter( UIContainer uiContainer, TextViewPI textViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( textViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ textViewPI.getComponentID() );
    }
    else {
      return new AndroidTextAdapter( textViewPI, (TextView) androidView, uiContainer );
    }
  }

  @Override
  public UIDateAdapter createDateAdapter( UIContainer uiContainer, DateViewPI dateViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( dateViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ dateViewPI.getComponentID() );
    }
    else {
      return new AndroidDateAdapter( dateViewPI, (TextView) androidView, uiContainer );
    }
  }

  @Override
  public UITextEditorAdapter createTextEditorAdapter( UIContainer uiContainer, TextEditorViewPI textEditorViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( textEditorViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ textEditorViewPI.getComponentID() );
    }
    else {
      return new AndroidTextEditorAdapter( textEditorViewPI, (TextView) androidView, uiContainer );
    }
  }

   /**
   * Searches uiContainer for UI text matching textProps (i.e. having textProp key as ID)
    * and replaces that text by text from textProps. If text contains "{GLOSSARYNAME}" that
    * text is replaced by entry form entry of glossaryProps
    *
    * @param uiContainer   the UI container to process
    * @param textProps     properties with text to use
    * @param glossaryProps properties with glossary entries
    */
   @Override
   public void updateLanguage( UIContainer uiContainer, Properties textProps, Properties glossaryProps ) {

     Platform.getInstance().runOnUiThread( new Runnable() {
       @Override
       public void run() {
         for (String textID : textProps.stringPropertyNames()) {
           View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( GuiBuilder.NAMESPACE.getQName( textID ) );
           if (androidView != null) {
             if (androidView instanceof ListView) {
               String[] colTextList = textProps.getProperty( textID ).split( "\\|" );

               for (int i = 0; i < colTextList.length; i++) {
                 String colText = colTextList[i];
                 String text = Helper.replaceParams( colText, "{", "}", glossaryProps );
                 View columnHeaderView = ((AndroidUIContainer) uiContainer).findAndroidView( GuiBuilder.NAMESPACE.getQName( textID + "_h" + i ) );
                 if (columnHeaderView instanceof TextView) {
                   ((TextView) columnHeaderView).setText( text );
                 }
               }

             }
             else if (androidView instanceof TextView) {
               String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
               ((TextView) androidView).setText( text );
             }
             else if (androidView instanceof AndroidTabPane) {
               String[] colTextList = textProps.getProperty( textID ).split( "\\|" );

               AndroidTabPane tabPane = (AndroidTabPane) androidView;
               // First an only child must be a RadioGroup
               View radioGroup = tabPane.getChildAt( 0 );
               if (radioGroup instanceof RadioGroup) {

                 for (int i = 0; i < colTextList.length; i++) {
                   String colText = colTextList[i];
                   String text = Helper.replaceParams( colText, "{", "}", glossaryProps );

                   try {
                     RadioButton radioButton = (RadioButton) ((RadioGroup) radioGroup).getChildAt( i );
                     radioButton.setText( text );
                   }
                   catch (Exception e) {
                     Logger.error( "Child count of RadioGroup in AndroidTabPane, does not match the language properties text count: " + Arrays.toString( colTextList ) );
                   }
                 }
               }
               else {
                 Logger.error( "Unsupported element at position 0 of AndroidTabPane (should eb a RadioGroup): " + androidView.getClass().getName() );
               }
             }
             else {
               Logger.warn( "Unsupported element for updateLanguage: " + androidView.getClass().getName() );
             }
           }
           else {
             MenuItem menuItem = ((AndroidUIContainer) uiContainer).findMenuItem( GuiBuilder.NAMESPACE.getQName( textID ) );
             if (menuItem != null) {
               String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
               menuItem.setTitle( text );
             }
           }
         }
       }
     } );

   }


  @Override
  public UIListAdapter createListAdapter( UIContainer uiContainer, ListViewPI listViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( listViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ listViewPI.getComponentID() );
    }
    else {
      if (androidView instanceof ListView) {
        GestureDetector.OnGestureListener gestureListener = null;
        AndroidListAdapter.SwipeListenerType gestureListenerType = null;
        if (uiContainer instanceof AndroidFragmentAdapter) {
          PiFragment activeFragment = ((AndroidFragmentAdapter) uiContainer).getActiveFragment();
          if (activeFragment instanceof GestureDetector.OnGestureListener) {
            gestureListener = (GestureDetector.OnGestureListener) activeFragment;
            gestureListenerType = AndroidListAdapter.SwipeListenerType.HORIZONTAL;
          }
        }
        return new AndroidListAdapter( listViewPI, (ListView) androidView, uiContainer, gestureListener, gestureListenerType );
      }
      else if (androidView instanceof Spinner) {
        return new AndroidListAdapter( listViewPI, (Spinner) androidView );
      }
      else {
        throw new IllegalArgumentException( "Unsupported view class="+androidView.getClass()+", viewID="+ listViewPI.getComponentID() );
      }
    }
  }

  @Override
  public UITableAdapter createTableAdapter( UIContainer uiContainer, TableViewPI tableViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( tableViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ tableViewPI.getComponentID() );
    }
    else {
      GestureDetector.OnGestureListener gestureListener = null;
      AndroidListAdapter.SwipeListenerType gestureListenerType = null;
      if (uiContainer instanceof AndroidFragmentAdapter) {
        PiFragment activeFragment = ((AndroidFragmentAdapter) uiContainer).getActiveFragment();
        if (activeFragment instanceof GestureDetector.OnGestureListener) {
          gestureListener = (GestureDetector.OnGestureListener) activeFragment;
          gestureListenerType = AndroidListAdapter.SwipeListenerType.HORIZONTAL;
        }
      }
      return new AndroidTableAdapter( tableViewPI, (ListView) androidView, uiContainer, gestureListener, gestureListenerType );
    }
  }

  @Override
  public UITreeAdapter createTreeAdapter( UIContainer uiContainer, TreeViewPI treeViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( treeViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ treeViewPI.getComponentID() );
    }
    else {
      return new AndroidTreeAdapter( treeViewPI, (ViewGroup) androidView, uiContainer );
    }
  }

  @Override
  public UIImageAdapter createImageAdapter( UIContainer uiContainer, ImageViewPI imageViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( imageViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ imageViewPI.getComponentID() );
    }
    else {
      return new AndroidImageAdapter( imageViewPI, (ImageView) androidView, uiContainer );
    }
  }

  @Override
  public UIFlagAdapter createFlagAdapter( UIContainer uiContainer, FlagViewPI flagViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( flagViewPI.getComponentID() );

    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ flagViewPI.getComponentID() );
    }
    else {
        if (androidView instanceof ToggleButton) {
          return new AndroidToggleButtonAdapter( flagViewPI, (ToggleButton) androidView, uiContainer );
        }
        else if (androidView instanceof CompoundButton){
          return new AndroidFlagAdapter( flagViewPI, (CompoundButton) androidView, uiContainer );
        }
        else {
          throw new IllegalArgumentException( "Not supported FlagAdapter element: " + androidView.getClass().toString() );
        }
    }
  }

  @Override
  public UIPaintAdapter createPaintAdapter( UIContainer uiContainer, PaintViewPI paintViewPI  ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( paintViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ paintViewPI.getComponentID() );
    }
    else {
      return new AndroidPaintAdapter( paintViewPI, (AndroidPaintView) androidView, uiContainer );
    }
  }


  @Override
  public UIProgressBarAdapter createProgressAdapter( UIContainer uiContainer, ProgressBarViewPI progressBarViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( progressBarViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID=" + progressBarViewPI.getComponentID() );
    }
    else if (androidView instanceof ProgressBar) {
      return new AndroidProgressBarAdapter( progressBarViewPI, (ProgressBar) androidView, uiContainer );
    }
    else if (androidView instanceof PiNumberPicker) {
      return new AndroidPiNumberPickerAdapter( progressBarViewPI, (PiNumberPicker) androidView, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Unsupported view for ProgressAdapter: " + androidView.getClass().getName() );
    }
  }

  @Override
  public UITabGroupAdapter createTabGroupAdapter( UIContainer uiContainer, TabGroupViewPI tabGroupViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( tabGroupViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ tabGroupViewPI.getComponentID() );
    }
    else {
      return new AndroidTabGroupAdapter( tabGroupViewPI, androidView, uiContainer );
    }
  }

  @Override
  public UIShapeAdapter createShapeAdapter( UIPaintAdapter uiPaintAdapter, ShapePI shapePI ) {
    ShapeDrawable shapeDrawable;
    ShapeType shapeType = shapePI.getShapeType();
    ShapeStyle shapeStyle = shapePI.getShapeStyle();
    Rect bounds = shapePI.getBounds();
    switch (shapeType) {
      case rect: {
        RectShape rectangle = new RectShape();
        shapeDrawable = new ShapeDrawableBorder( rectangle, shapeStyle );
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        if (bounds instanceof RectDir) {
          Rotation rotation = ((RectDir) bounds).getRotation();
          if (rotation != null) {
            if (rotation.getAngle() != 0.0) {
              throw new RuntimeException( "TODO shape rotation" );
            }
          }
        }
        break;
      }
      case line: {
        Pos startPos = shapePI.getPos( 0 );
        Pos endPos = shapePI.getPos( 1 );
        LineShape lineShape = new LineShape( startPos, endPos );
        shapeDrawable = new LineShapeDrawable( lineShape, shapeStyle );
        break;
      }
      case path: {
        PathElement[] pathElements = ((PathShapePI) shapePI).getPathElementDefinition();
        shapeDrawable = new PathShapeDrawable( pathElements, shapeStyle );
        break;
      }
      case ellipse: {
        OvalShape ellipse = new OvalShape();
        shapeDrawable = new ShapeDrawableBorder( ellipse, shapeStyle );
        break;
      }
      case arc: {
        double startAngle = ((ArcShapePI) shapePI).getStartAngle();
        double sweepAngle = ((ArcShapePI) shapePI).getSweepAngle();
        boolean useCenter = ((ArcShapePI) shapePI).getUseCenter();
        shapeDrawable = new ArcShapeDrawable( startAngle, sweepAngle, useCenter, shapeStyle );
        break;
      }
      case text: {
        String text = shapePI.getText();
        shapeDrawable = new TextShapeDrawable( text, shapeStyle );
        break;
      }
      case bitmap: {
        QName bitmapID = ((BitmapPI) shapePI).getBitmapID();
        ComponentBitmap componentBitmap = ((BitmapPI) shapePI).getBitmap();
        BitmapAdjust bitmapAdjust = ((BitmapPI) shapePI).getBitmapAdjust();
        if (shapePI instanceof AnimatedBitmapPI) {
          AnimationType animationType = ((AnimatedBitmapPI) shapePI).getAnimationType();
          shapeDrawable = new AnimatedBitmapDrawable( bitmapID, componentBitmap, bitmapAdjust, animationType, shapeStyle );
        }
        else {
          shapeDrawable = new BitmapDrawable( bitmapID, componentBitmap, bitmapAdjust, shapeStyle );
        }
        break;
      }
      default: {
        throw new IllegalArgumentException( "Unsupported shape type="+shapeType );
      }
    }
    AndroidShapeAdapter shapeAdapter = new AndroidShapeAdapter( shapeDrawable, shapePI );
    if (bounds instanceof RectDir) {
      Rotation rotation = ((RectDir) bounds).getRotation();
      if (rotation != null) {
        if (rotation.getAngle() != 0.0) {
          shapeAdapter.addTransformation( rotation );
        }
      }
    }
    AndroidPaintAdapter.applyShapeStyle( shapeDrawable, shapeStyle );
    return shapeAdapter;
  }

  @Override
  public UIWebViewAdapter createWebViewAdapter( UIContainer uiContainer, WebViewPI webViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIHTMLEditorAdapter createHTMLEditorAdapter( UIContainer uiContainer, HTMLEditorViewPI htmlEditorViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UINodeAdapter createNodeAdapter( UIContainer uiContainer, NodeViewPI nodeViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( nodeViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID=" + nodeViewPI.getComponentID() );
    }
    else {
      return new AndroidNodeAdapter( androidView, nodeViewPI, uiContainer );
    }
  }

  @Override
  public UICodeEditorAdapter createCodeEditorAdapter( UIContainer uiContainer, CodeEditorViewPI codeEditorViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIFragmentAdapter createFragmentAdapter( UIContainer uiContainer, ModuleViewPI moduleViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( moduleViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ moduleViewPI.getComponentID() );
    }
    else {
      FragmentManager fragmentManager = ((AndroidUIContainer) uiContainer).getChildFragmentManager();
      return new AndroidFragmentAdapter( moduleViewPI, androidView, fragmentManager, uiContainer );
    }
  }

  @Override
  public UITabPaneAdapter createTabPaneAdapter( UIContainer uiContainer, TabPaneViewPI tabPaneViewPI ) {
    View androidView = ((AndroidUIContainer) uiContainer).findAndroidView( tabPaneViewPI.getComponentID() );
    if (androidView == null) {
      throw new IllegalArgumentException( "Could not find view ID=" + tabPaneViewPI.getComponentID() );
    }
    else if (androidView instanceof AndroidTabPane) {
      return new AndroidTabPaneAdapter( (AndroidTabPane) androidView, tabPaneViewPI, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Type not supported for item with ID=" + tabPaneViewPI.getComponentID() );
    }
  }

  @Override
  public UITreeTableAdapter createTreeTableAdapter( UIContainer uiContainer, TreeTableViewPI tableViewPI ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIAdapter createCustomAdapter( UIContainer uiContainer, ViewPI viewPI ) {
    throw new IllegalArgumentException( "Unsupported custom view class="+viewPI.getClass() );
  }
}
