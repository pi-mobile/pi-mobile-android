/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android;

import de.pidata.gui.android.adapter.AndroidUIAdapter;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.Rect;
import de.pidata.gui.component.base.Screen;
import de.pidata.gui.event.Dialog;

public class AndroidScreen implements Screen {

  private AndroidDialog focusDialog;

  public Rect getScreenBounds() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns true if this screen works with a single window. If true
   * PaintManager has to simulate Windows and Popups
   *
   * @return true if this screen works with a single window
   */
  public boolean isSingleWindow() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public void setScreenSize( int width, int height ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public Dialog getFocusDialog() {
    //TODO den FocusDialog müssten wir doch auch über die current Activity finden und dann nicht manuell verwalten
    return this.focusDialog;
  }

  public void setFocusDialog( AndroidDialog focusDialog ) {
    this.focusDialog = focusDialog;
  }

  /**
   * Switch focus to the given view if possible
   *
   * @param toView the destination view for focus switch
   */
  public void switchFocus( AndroidUIAdapter toView ) {
    toView.requestFocus();
  }
}
