/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;

public class ExitActivity extends Activity {

  @Override
  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );
    Logger.info( "finishAndRemoveTask() in ExitActivity" );
    finishAndRemoveTask();
    android.os.Process.killProcess( android.os.Process.myPid() );
  }

  public static void exitApplication( Context context ) {
    PiMobileActivity currentActivity = AndroidApplication.getInstance().getCurrentActivity();
    if(currentActivity == null) {
      Intent intent = new Intent( context, ExitActivity.class );
      intent.addFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS );
      context.startActivity( intent );
    }
    else{
      Logger.error( "Can not kill Application, an Activity is still active. Use Platform.getInstance.exit() instead!" );
    }
  }
}