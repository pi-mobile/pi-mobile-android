package de.pidata.gui.android.activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.widget.TextView;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.log.Logger;

public class ReadTagActivity extends PiMobileActivity {

  public static final String TAG_ID = "tag_id";
  private TextView nfcText;
  private NfcAdapter mNfcAdapter;

  @Override
  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );
    int layoutResID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_LAYOUT, GuiBuilder.NAMESPACE.getQName( "read_tag" ) );
    setContentView( layoutResID );

    int viewID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, GuiBuilder.NAMESPACE.getQName( "text_explanation" ) );
    nfcText = (TextView) findViewById( viewID );

    mNfcAdapter = NfcAdapter.getDefaultAdapter( this );

    if (mNfcAdapter == null) {
      int strID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_STRING, GuiBuilder.NAMESPACE.getQName( "nfc_not_supported" ) );
      nfcText.setText( strID );
    }

    if (!mNfcAdapter.isEnabled()) {
      int strID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_STRING, GuiBuilder.NAMESPACE.getQName( "nfc_disabled" ) );
      nfcText.setText( strID );
    }
    else {
      int strID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_STRING, GuiBuilder.NAMESPACE.getQName( "nfc_enabled" ) );
      nfcText.setText( strID );
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    setupForegroundDispatch( this, mNfcAdapter );
  }

  @Override
  protected void onNewIntent( Intent intent ) {
    Tag myTag = (Tag) intent.getParcelableExtra( NfcAdapter.EXTRA_TAG );
    if(myTag != null) {
      byte[] id = myTag.getId();
      String tagId = byteArrayToHex( id, true );

      String msg = "Scanned Tag: " + tagId;
      Logger.info( msg );

      Intent result = new Intent();
      result.putExtra( TAG_ID, tagId );
      setResult( Activity.RESULT_OK, result );
      finish();
    }
  }

  private String byteArrayToHex( byte[] bytes, boolean upperCase ) {
    StringBuilder hexString = new StringBuilder();
    //little endian
    for (byte b : bytes) {
      String hex = byteToHex( b );
      hexString.append( hex );
      hexString.append( ":" );
    }
    String tagId = hexString.substring( 0, hexString.length() - 1 );
    if (upperCase) {
      tagId = tagId.toUpperCase();
    }
    return tagId;
  }

  private String byteToHex( byte num ) {
    char[] hexDigits = new char[2];
    hexDigits[0] = Character.forDigit( (num >> 4) & 0xF, 16 );
    hexDigits[1] = Character.forDigit( (num & 0xF), 16 );
    return new String( hexDigits );
  }

  public static void setupForegroundDispatch( final Activity activity, NfcAdapter adapter ) {
    final Intent intent = new Intent( activity.getApplicationContext(), activity.getClass() );
    intent.setFlags( Intent.FLAG_ACTIVITY_SINGLE_TOP );

    final PendingIntent pendingIntent = PendingIntent.getActivity( activity.getApplicationContext(), 0, intent, 0 );

    adapter.enableForegroundDispatch( activity, pendingIntent, null, null );
  }

  @Override
  public void requireRights() {
    // nothing to do
  }
}
