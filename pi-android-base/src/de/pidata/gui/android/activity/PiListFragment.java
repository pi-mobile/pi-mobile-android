/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.activity;

import android.app.*;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import de.pidata.gui.android.adapter.AndroidFragmentAdapter;
import de.pidata.gui.android.adapter.AndroidListAdapter;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.controller.base.ModelParameterList;
import de.pidata.gui.controller.base.ModuleController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.Module;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.service.base.ParameterList;

public class PiListFragment extends ListFragment  {

  private static final boolean DEBUG_STATES = false;

  /**
   * Called when a fragment is first attached to its activity.
   * {@link #onCreate(android.os.Bundle)} will be called after this.
   */
  public void onAttach( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onAttach; activity is: " + activity );
    super.onAttach( activity );
  }

  /**
   * Called when the fragment's activity has been created and this
   * fragment's view hierarchy instantiated.  It can be used to do final
   * initialization once these pieces are in place, such as retrieving
   * views or restoring state.  It is also useful for fragments that use
   * {@link #setRetainInstance(boolean)} to retain their instance,
   * as this callback tells the fragment when it is fully associated with
   * the new activity instance.  This is called after {@link #onCreateView}
   * and before {@link #onStart()}.
   *
   * @param savedInstanceState If the fragment is being re-created from
   *                           a previous saved state, this is the state.
   */
  public void onActivityCreated( Bundle savedInstanceState ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onActivityCreated" );
    super.onActivityCreated( savedInstanceState );
  }

  public void onListItemClick( ListView l, View v, int pos, long id ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onListItemClick. pos = "  + pos );
    showDetails( pos );
  }

  public boolean isMultiPane() {
    return (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
  }

  /**
   * Helper function to show the details of a selected item, either by
   * displaying a fragment in-place in the current UI, or starting a
   * whole new activity in which it is displayed.
   */
  public void showDetails( int index ) {
    ListAdapter listAdapter = getListAdapter();
    Model selectedValue = (Model) listAdapter.getItem( index );
    SelectionController listCtrl = ((AndroidListAdapter) listAdapter).getListViewPI().getListCtrl();
    ModuleController detailModule = (ModuleController) listCtrl.getDetailController();
    if (isMultiPane()) {
      // Check what fragment is shown, replace if needed.
      int detailID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, detailModule.getName() );
      Fragment details = getFragmentManager().findFragmentById( detailID );
      if (details == null) {
        ControllerBuilder builder = listCtrl.getControllerGroup().getDialogController().getControllerBuilder();
        try {
          builder.loadModule( detailModule, detailModule.getModuleID(), detailModule, "." );
        }
        catch (Exception ex) {
          Logger.error( "Exception loading module id="+detailModule.getModuleID(), ex );
        }
        // Make new fragment to show this selection.
        details = new PiFragment();
        ModuleGroup moduleGroup = detailModule.getModuleGroup();
        ((PiFragment) details).init( moduleGroup, (ModuleViewPI) moduleGroup.getParentController().getView(), selectedValue );

        // Execute a transaction, replacing any existing
        // fragment inside the frame with the new one.
        if (DEBUG_STATES) Logger.info( "about to run FragmentTransaction..." );
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.replace( detailID, details );
        //ft.addToBackStack(TAG);
        ft.commit();
      }
      else {
        details.getView().invalidate();
      }
    }
    else {
      PiMobileActivity activity = (PiMobileActivity) getActivity();
      ParameterList parameterList = new ModelParameterList( selectedValue );
      activity.getDialogController().openChildDialog( detailModule.getModuleID(), null, parameterList );
    }
  }

  //----------------- Debugging only -------------------------

  public void onInflate( Activity activity, AttributeSet attrs, Bundle bundle ) {
    if (DEBUG_STATES) {
      Logger.info( "in "+getClass().getSimpleName()+" onInflate. AttributeSet contains:" );
      for (int i = 0; i < attrs.getAttributeCount(); i++) {
        Logger.info( "    " + attrs.getAttributeName( i ) +  " = " + attrs.getAttributeValue( i ) );
      }
    }
    super.onInflate( activity, attrs, bundle );
  }

  public View onCreateView( LayoutInflater myInflater, ViewGroup container, Bundle icicle ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onCreateView. container is " + container );
    return super.onCreateView( myInflater, container, icicle );
  }

  public void onCreate( Bundle bundle ) {
    if (DEBUG_STATES) {
      Logger.info(  "in "+getClass().getSimpleName()+" onCreate. Bundle contains:" );
      if (bundle != null) {
        for (String key : bundle.keySet()) {
          Logger.info( "    " + key );
        }
      }
      else {
        Logger.info( "    myBundle is null" );
      }
    }
    super.onCreate( bundle );
  }

  public void onStart() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onStart" );
    super.onStart();
    ListView listView = getListView();
    listView.setChoiceMode( ListView.CHOICE_MODE_SINGLE );
    setListAdapter( listView.getAdapter() );
  }

  public void onResume() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onResume" );
    super.onResume();
  }

  public void onPause() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onPause" );
    super.onPause();
  }

  public void onSaveInstanceState( Bundle icicle ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onSaveInstanceState" );
    super.onSaveInstanceState( icicle );
  }

  public void onStop() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onStop" );
    super.onStop();
  }

  public void onDestroyView() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDestroyView" );
    super.onDestroyView();
  }

  public void onDestroy() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDestroy" );
    super.onDestroy();
  }

  public void onDetach() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDetach" );
    super.onDetach();
  }
}
