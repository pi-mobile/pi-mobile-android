/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.adapter.AndroidFragmentAdapter;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;

public class PiFragment extends DialogFragment implements  DialogInterface.OnClickListener {

  private static final boolean DEBUG_STATES = false;

  private ModuleViewPI moduleViewPI;
  private ModuleGroup moduleGroup;
  private Model model;

  @Override
  public void onDismiss( DialogInterface dialog ) {
    getActivity().getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
    InputMethodManager imm =  (InputMethodManager) getActivity().getApplicationContext().getSystemService( Activity.INPUT_METHOD_SERVICE );
    if (imm != null) {
      imm.hideSoftInputFromWindow( getActivity().getWindow().getDecorView().getRootView().getWindowToken(),0 );
    }
    super.onDismiss( dialog );
  }

  public void init( ModuleGroup moduleGroup, ModuleViewPI moduleViewPI, Model model ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ".init" );
    this.moduleGroup = moduleGroup;
    this.moduleViewPI = moduleViewPI;
    this.model = model;
  }

  /**
   * Called when a fragment is first attached to its activity.
   * {@link #onCreate(android.os.Bundle)} will be called after this.
   */
  @Override
  public void onAttach( Activity activity ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ".onAttach, activity is " + activity );
    super.onAttach( activity );
  }

  public void onCreate( Bundle bundle ) {
    if (DEBUG_STATES) {
      Logger.info(  "in "+getClass().getSimpleName()+" onCreate. Bundle contains:" );
      if (bundle != null) {
        for (String key : bundle.keySet()) {
          Logger.info( "    " + key );
        }
      }
      else {
        Logger.info( "    myBundle is null" );
      }
    }
    super.onCreate( bundle );
    getActivity().getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
    InputMethodManager imm =  (InputMethodManager) getActivity().getApplicationContext().getSystemService( Activity.INPUT_METHOD_SERVICE );
    if (imm != null) {
      imm.hideSoftInputFromWindow( getActivity().getWindow().getDecorView().getRootView().getWindowToken(),0 );
    }
  }

  /**
   * Called to have the fragment instantiate its user interface view.
   * This is optional, and non-graphical fragments can return null (which
   * is the default implementation).  This will be called between
   * {@link #onCreate(android.os.Bundle)} and {@link #onActivityCreated(android.os.Bundle)}.
   * <p/>
   * <p>If you return a View from here, you will later be called in
   * {@link #onDestroyView} when the view is being released.
   *
   * @param inflater           The LayoutInflater object that can be used to inflate
   *                           any views in the fragment,
   * @param container          If non-null, this is the parent view that the fragment's
   *                           UI should be attached to.  The fragment should not add the view itself,
   *                           but this can be used to generate the LayoutParams of the view.
   * @param savedInstanceState If non-null, this fragment is being re-constructed
   *                           from a previous saved state as given here.
   * @return Return the View for the fragment's UI, or null.
   */
  public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ".onCreateView, container is " + container );
    if (moduleGroup == null) {
      return null;
    }
    else {
      int moduleResID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_LAYOUT, moduleGroup.getName() );
      View view = inflater.inflate( moduleResID, container, false );
      return view;
    }
  }

  /**
   * Called when the fragment's activity has been created and this
   * fragment's view hierarchy instantiated.  It can be used to do final
   * initialization once these pieces are in place, such as retrieving
   * views or restoring state.  It is also useful for fragments that use
   * {@link #setRetainInstance(boolean)} to retain their instance,
   * as this callback tells the fragment when it is fully associated with
   * the new activity instance.  This is called after {@link #onCreateView}
   * and before {@link #onStart()}.
   *
   * @param savedInstanceState If the fragment is being re-created from
   *                           a previous saved state, this is the state.
   */
  public void onActivityCreated( Bundle savedInstanceState ) {
    if (DEBUG_STATES) Logger.info( "in " + getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + ".onActivityCreated, moduleViewPI="+moduleViewPI );
    super.onActivityCreated( savedInstanceState );
    if (this.moduleViewPI != null) {
      try {
        if (moduleViewPI.getUIAdapter() == null) {
          // If we are running as Dialog now is the first moment we can activate our ModuleController
          AndroidFragmentAdapter androidFragmentAdapter = new AndroidFragmentAdapter( moduleViewPI, getView(), getFragmentManager(), moduleViewPI.getUIContainer() );
          androidFragmentAdapter.setActiveFragment( this );
          moduleViewPI.setFragmentAdapter( androidFragmentAdapter );
        }
        moduleViewPI.onFragmentLoaded( moduleGroup );
      }
      catch (Exception e) {
        Logger.error( "Error calling onFragmentLoaded", e );
      }
    }
  }

  /**
   * Override to build your own custom Dialog container.  This is typically
   * used to show an AlertDialog instead of a generic Dialog; when doing so,
   * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} does not need
   * to be implemented since the AlertDialog takes care of its own content.
   *
   * <p>This method will be called after {@link #onCreate(Bundle)} and
   * before {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.  The
   * default implementation simply instantiates and returns a {@link Dialog}
   * class.
   *
   * <p><em>Note: DialogFragment own the {@link Dialog#setOnCancelListener
   * Dialog.setOnCancelListener} and {@link Dialog#setOnDismissListener
   * Dialog.setOnDismissListener} callbacks.  You must not set them yourself.</em>
   * To find out about these events, override {@link #onCancel(DialogInterface)}
   * and {@link #onDismiss(DialogInterface)}.</p>
   *
   * @param savedInstanceState The last saved instance state of the Fragment,
   *                           or null if this is a freshly created Fragment.
   * @return Return a new Dialog instance to be displayed by the Fragment.
   */
/*  @Override
  public Dialog onCreateDialog( Bundle savedInstanceState ) {
    AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
    //builder.setIcon(R.drawable.alert_dialog_icon);
    //builder.setTitle( moduleViewPI.getModuleID().getName() );
    //builder.setMessage( message );
    builder.setPositiveButton( "OK", this );
    builder.setNegativeButton( "Cancel", this );
    builder.setCancelable( true );
    View content = getView();
    builder.setView( content );
    return builder.create();
  } */

  /**
   * This method will be invoked when a button in the dialog is clicked.
   *
   * @param dialog the dialog that received the click
   * @param which  the button that was clicked (ex.
   *               {@link DialogInterface#BUTTON_POSITIVE}) or the position
   */
  @Override
  public void onClick( DialogInterface dialog, int which ) {
    //TODO
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "@" + Integer.toHexString(hashCode()) + "(compID="+moduleViewPI.getComponentID()+",ctrlID="+moduleGroup.getName()+")";
  }

  //----------------- Debugging only -------------------------

  public void onInflate( Activity activity, AttributeSet attrs, Bundle bundle ) {
    if (DEBUG_STATES) {
      Logger.info( "in "+getClass().getSimpleName()+" onInflate. AttributeSet contains:" );
      for (int i = 0; i < attrs.getAttributeCount(); i++) {
        Logger.info( "    " + attrs.getAttributeName( i ) +  " = " + attrs.getAttributeValue( i ) );
      }
    }
    super.onInflate( activity, attrs, bundle );
  }

  public void onStart() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onStart" );
    super.onStart();
  }

  public void onResume() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onResume" );
    super.onResume();
  }

  public void onPause() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onPause" );
    super.onPause();
  }

  public void onSaveInstanceState( Bundle icicle ) {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onSaveInstanceState" );
    super.onSaveInstanceState( icicle );
  }

  public void onStop() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onStop" );
    super.onStop();
  }

  public void onDestroyView() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDestroyView" );
    if (this.moduleViewPI != null) {
      try {
        moduleViewPI.onFragmentDestroyed( moduleGroup );
      }
      catch (Exception e) {
        Logger.error( "Error calling onFragmentLoaded", e );
      }
    }
    super.onDestroyView();
  }

  public void onDestroy() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDestroy" );
    super.onDestroy();
  }

  public void onDetach() {
    if (DEBUG_STATES) Logger.info( "in "+getClass().getSimpleName()+" onDetach" );
    super.onDetach();
  }
}
