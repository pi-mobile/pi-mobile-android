/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.activity;

import android.content.Intent;
import android.os.Bundle;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;

public class DefaultActivity extends PiMobileActivity {

  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );
    QName dialogName;
    String action = getIntent().getAction();
    if (action.equals( Intent.ACTION_MAIN )) {
      dialogName = Platform.getInstance().getControllerBuilder().getApplication().getOperation();
      if (dialogName == null) {
        dialogName = NAMESPACE.getQName( "main_view" );
      }
    }
    else {
      dialogName = NAMESPACE.getQName( getIntent().getAction() );
    }
    try {
      int layoutResID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_LAYOUT, dialogName );
      createView( layoutResID, dialogName );
    }
    catch (Exception ex) {
      String msg = "Error creating dialog name="+dialogName;
      Logger.error( msg, ex );
      throw new IllegalArgumentException( msg );
    }
  }

  @Override
  public void requireRights() {
    // nothing to do
  }
}
