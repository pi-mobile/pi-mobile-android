/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.activity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.android.controller.AndroidDialogControllerBuilder;
import de.pidata.gui.component.base.ComponentFactory;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.platform.android.AndroidPlatform;
import de.pidata.log.Logger;
import de.pidata.models.binding.Selection;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.*;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ParameterType;
import de.pidata.string.Helper;
import de.pidata.system.base.SystemManager;

import java.lang.reflect.Field;
import java.util.*;

public abstract class PiMobileActivity extends Activity implements DialogActivity {

  private static final boolean DEBUG_STATES = false;

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  public static final String TYPE_QNAME = "QName";
  public static final String TYPE_STRING = "String";
  public static final String TYPE_DECIMAL_OBJECT = "DecimalObject";
  public static final String TYPE_INTEGER = "Integer";
  public static final String TYPE_BOOLEAN = "Boolean";
  public static final String TYPE_BINARY = "Binary";
  public static final String TYPE_DURATION_OBJECT = "DurationObject";
  
  public static final String NAMESPACE_OF = "_namespace_of_";

  protected AndroidDialog dialog;
  protected DialogController dialogController;
  protected Menu optionsMenu;
  protected QName dialogID;
  private boolean active = false;
  private List<RightsRequireListener> rightsRequireListeners = new ArrayList<RightsRequireListener>();
  private Bundle savedInstanceState = null;
  private ActivityResult activityResult;

  public String getName() {
    if (dialogID == null) {
      return "";
    }
    else {
      return dialogID.getName();
    }
  }

  protected void createView( int layoutResourceID, QName dialogID ) {
    View contentView = getLayoutInflater().inflate( layoutResourceID, null );
    setContentView( contentView );
    Logger.info( "Create View, dialogID="+dialogID );
    this.dialogID = dialogID;

    //--- Create android dialog
    dialog = new AndroidDialog();
    dialog.init( this, contentView );

    createController();
  }

  /**
   * Called by AndroidDialogControllerBuilder
   */
  public synchronized void createController() {
    if (dialogController == null) {
      Logger.info( "Creating dialogController for dialogID="+dialogID );
      Context context = AndroidApplication.getInstance().getContext();
      AndroidDialogControllerBuilder builder = (AndroidDialogControllerBuilder) Platform.getInstance().getControllerBuilder();

      DialogType dialogType = builder.getDialogType( dialogID );
      if (dialogType == null) {
        DialogDef dialogDef = builder.getDialogDef( dialogID );
        if (dialogDef == null) {
          throw new IllegalArgumentException( "No dialog definition found for dialogID=" + dialogID );
        }
      }

      //--- Create controller
      DialogController parentDlgCtrl = null;
      Activity parentActivity = getParent();
      if ((parentActivity != null) && (parentActivity instanceof DialogActivity)) {
        parentDlgCtrl = ((DialogActivity) parentActivity).getDialogController();
      }
      dialogController = builder.createDialogController( dialogID, context, dialog, parentDlgCtrl );
      runOnUiThread( new Runnable() {
        @Override
        public void run() {
          setTitle( dialogController.getTitle() );
        }
      } );

      DialogControllerDelegate delegate = dialogController.getDelegate();
      if (delegate == null) {
        throw new IllegalArgumentException( "Cannot create dialog id=" + dialogID + ", delegate is missing" );
      }
      else {
        ParameterList parameterList = null;
        try {
          parameterList = intentToParamList( getIntent() );
          dialogController.setParameterList( parameterList );
        }
        catch (Exception ex) {
          String msg = "Error setting ParameterList for dialog ID=" + dialogID;
          Logger.error( msg, ex );
          throw new IllegalArgumentException( msg );
        }
        Logger.info( "DialogController created for dialogID="+dialogID+", optionsMenu="+optionsMenu );
      }
    }
  }

  @Override
  public DialogController getDialogController() {
    return dialogController;
  }

  private String getDlgCtrlName() {
    if (dialogController == null) return "<unknown>";
    else return dialogController.getName().toString();
  }

  @Override
  protected void onCreate( Bundle savedInstanceState ) {

    Locale.setDefault( SystemManager.getInstance().getLocale() );

    if (DEBUG_STATES) Logger.info( "in "+getClass().getName()+".onCreate" );
    super.onCreate( savedInstanceState );
    try {
      Class res = Class.forName( getPackageName() + ".R$string" );
      Field field = res.getField( "orientation_fixed" );
      int resID = field.getInt( null );
      String orientationFixed = getResources().getString( resID );
      String constName = "SCREEN_ORIENTATION_" + orientationFixed.toUpperCase();
      Field orientationConst = ActivityInfo.class.getField( constName );
      setRequestedOrientation( orientationConst.getInt( null ) );
      Logger.info( "Requested orientation set to "+constName );
    }
    catch (Exception ex) {
      Logger.info( "Value 'orientation_fixed' not found, ex="+ex.getMessage() );
    }
  }

  protected void onStart() {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onStart" );
    super.onStart();
  }

  protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onActivityResult" );
    super.onActivityResult( requestCode, resultCode, data );
    this.activityResult = new ActivityResult( requestCode, resultCode, data );
    if (active) {
      processActivityResult();
    }
    if (((AndroidPlatform) Platform.getInstance()).isExiting()) {
      this.finish();
    }
  }

  private void processActivityResult() {
    AndroidDialog androidDialog = (AndroidDialog) getDialogController().getDialogComp();
    androidDialog.childClosed( activityResult.getResultCode(), activityResult.getData() );
    this.activityResult = null;
  }

  protected void onSaveInstanceState( Bundle outState ) {
    if (DEBUG_STATES) Logger.info( "in " + getName() + " onSaveInstanceState" );
    super.onSaveInstanceState( outState );
    NamespaceTable namespaces = dialogController.getNamespaceTable();
    try {
      //TODO logik prüfen
      if (dialogController != null) {
        QName lastGuiOpID = dialogController.getLastGuiOpID();
        if (lastGuiOpID == null) {
          outState.putString( "lastGuiOpID", null );
        }
        else {
          outState.putString( "lastGuiOpID", lastGuiOpID.toString() );
        }
        int selectionCtrlIndex = 0;
        int valueCtrlIndex = 0;
        for (Controller childController : dialogController.getControllerGroup().controllerIterator()) {
          if (childController instanceof SelectionController) {
            selectionCtrlIndex++;
            SelectionController selectionController = (SelectionController) childController;
            Selection selection = selectionController.getSelection();
            int selectedIndex = 0;
            if (selection != null) {
              selectedIndex = selection.indexOf( selection.getSelectedValue( 0 ) );
            }
            outState.putString( "selectionCtrl_" + selectionCtrlIndex, selectionController.getName().toString() );
            outState.putString( "selectedIndexCtrl_" + selectionCtrlIndex, String.valueOf( selectedIndex ) );
          }
          else if (childController instanceof ValueController) {
            valueCtrlIndex++;
            ValueController valueCtrl = (ValueController) childController;
            if (!valueCtrl.isReadOnly()) {
              Object value = valueCtrl.getValue();
              if (value != null) {
                String controllerName = valueCtrl.getName().toString();
                String stringValue;
                String dataType;
                if (value instanceof QName) {
                  dataType = TYPE_QNAME;
                  String nsPrefix = namespaces.getOrCreatePrefix( (QName) value, false );
                  //save prefix instead of NamespaceUri otherwise it can not be interpreted correctly later if uri contains ":" - Characters
                  stringValue = nsPrefix + ":" + ((QName) value).getName();
                }
                else if (value instanceof String) {
                  dataType = TYPE_STRING;
                  stringValue = (String) value;
                }
                else if (value instanceof DecimalObject) {
                  dataType = TYPE_DECIMAL_OBJECT;
                  stringValue = value.toString();
                }
                else if (value instanceof Integer) {
                  dataType = TYPE_INTEGER;
                  stringValue = String.valueOf( (int) value );
                }
                else if (value instanceof Boolean) {
                  dataType = TYPE_BOOLEAN;
                  stringValue = String.valueOf( (boolean) value );
                }
                else if (value instanceof DateObject) {
                  QName type = ((DateObject) value).getType();
                  dataType = type.getName();
                  if (type == DateTimeType.TYPE_DATE) {
                    stringValue = DateTimeType.toDateString( (DateObject) value );
                  }
                  else if (type == DateTimeType.TYPE_DATETIME) {
                    stringValue = DateTimeType.toDateTimeString( (DateObject) value, true );
                  }
                  else {
                    //                type == DateTimeType.TYPE_TIME
                    stringValue = DateTimeType.toTimeString( (DateObject) value, true );
                  }
                }
                else if (value instanceof Binary) {
                  dataType = TYPE_BINARY;
                  stringValue = BinaryType.toBase64String( ((Binary) value).getBytes(), ((Binary) value).size() );
                }
                else if (value instanceof DurationObject) {
                  dataType = TYPE_DURATION_OBJECT;
                  stringValue = value.toString();
                }
                else {
                  dataType = TYPE_STRING;
                  stringValue = value.toString();
                }

                outState.putString( "valueCtrl_" + valueCtrlIndex, controllerName );
                outState.putString( "value_" + valueCtrlIndex, stringValue );
                outState.putString( "valueType_" + valueCtrlIndex, dataType );
              }
            }
          }
        }
        Iterator<Namespace> namespaceIterator = namespaces.getNamespaceIter();
        while (namespaceIterator.hasNext()) {
          Namespace namespace = namespaceIterator.next();
          String prefix = namespaces.getPrefix( namespace );
          outState.putString( prefix, namespace.getUri() );
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Exception in onSaveInstanceState", ex );
    }
  }

  protected void onRestoreInstanceState( Bundle savedInstanceState ) {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onRestoreInstanceState" );
    super.onRestoreInstanceState( savedInstanceState );
    if (dialogController != null) {
      String lastGuiOpID = savedInstanceState.getString( "lastGuiOpID" );
      if (lastGuiOpID != null) {
        dialogController.setLastGuiOpID( QName.getInstance( lastGuiOpID, dialogController.getNamespaceTable() ) );
      }
    }
    this.savedInstanceState = savedInstanceState;
  }

  private void restoreDialogController( Bundle savedInstanceState ) {
    try {
      int selectionCtrlIndex = 0;
      int valueCtrlIndex = 0;
      NamespaceTable namespaceTable = dialogController.getNamespaceTable();
      for (Controller childController : dialogController.getControllerGroup().controllerIterator()) {
        try {
          if (childController instanceof SelectionController) {
            selectionCtrlIndex++;
            String selectionCtrlName = savedInstanceState.getString( "selectionCtrl_" + selectionCtrlIndex );
            if (selectionCtrlName != null) {
              QName name = QName.getInstance( selectionCtrlName, namespaceTable );
              SelectionController selectionController = (SelectionController) dialogController.getController( name );
              if (selectionController != null) {
                String indexString = savedInstanceState.getString( "selectedIndexCtrl_" + selectionCtrlIndex );
                if (indexString != null) {
                  Model selectedModel = selectionController.getSelection().getValue( Integer.parseInt( indexString ) );
                  selectionController.selectRow( selectedModel );
                }

              }
            }
          }
          else if (childController instanceof ValueController) {
            valueCtrlIndex++;
            String valueCtrlName = savedInstanceState.getString( "valueCtrl_" + valueCtrlIndex );
            if (valueCtrlName != null) {
              QName ctrlName = QName.getInstance( valueCtrlName, namespaceTable );
              ValueController valueCtrl = (ValueController) dialogController.getController( ctrlName );
              if (valueCtrl != null) {
                String stringValue = savedInstanceState.getString( "value_" + valueCtrlIndex );
                if (stringValue != null) {
                  Object value = null;
                  String datatype = savedInstanceState.getString( "valueType_" + valueCtrlIndex );
                  if (Helper.isNullOrEmpty( datatype )) {
                    datatype = TYPE_STRING;
                  }
                  if (TYPE_QNAME.equals( datatype )) {
                    int nsEnd = stringValue.indexOf( ":" );
                    String nsPrefix = stringValue.substring( 0, nsEnd );
                    String namespaceUri = savedInstanceState.getString( nsPrefix, "" );
                    String name = stringValue.substring( nsEnd + 1 );
                    Namespace namespace = Namespace.getInstance( namespaceUri );
                    value = namespace.getQName( name );
                  }
                  else if (TYPE_STRING.equals( datatype )) {
                    value = stringValue;
                  }
                  else if (TYPE_INTEGER.equals( datatype )) {
                    value = Integer.valueOf( stringValue );
                  }
                  else if (TYPE_BOOLEAN.equals( datatype )) {
                    value = Boolean.valueOf( stringValue );
                  }
                  else if (DateTimeType.TYPE_DATE.getName().equals( datatype )) {
                    value = DateTimeType.getDefDate().createValue( stringValue, null );
                  }
                  else if (DateTimeType.TYPE_DATETIME.getName().equals( datatype )) {
                    value = DateTimeType.getDefDateTime().createValue( stringValue, null );
                  }
                  else if (DateTimeType.TYPE_TIME.getName().equals( datatype )) {
                    value = DateTimeType.getDefTime().createValue( stringValue, null );
                  }
                  else if (TYPE_DECIMAL_OBJECT.equals( datatype )) {
                    value = new DecimalObject( stringValue );
                  }
                  else if (TYPE_BINARY.equals( datatype )) {
                    value = BinaryType.base64Binary.createValue( stringValue, null );
                  }
                  else if (TYPE_DURATION_OBJECT.equals( datatype )) {
                    value = DurationType.getDefault().createValue( stringValue, null );
                  }

                  if (value != null) {
                    valueCtrl.setValue( value );
                  }
                }
              }
            }
          }
        }
        catch (Exception e) {
          Logger.error( "Could not restore selection of controller " + childController.getName().toString(), e );
        }
      }
    }
    catch (Exception ex) {
      savedInstanceState.clear();
      Logger.error( "Exception in onRestoreInstanceState", ex );
    }
  }

  /**
   * Called when the activity has detected the user's press of the back
   * key.  The default implementation simply finishes the current activity,
   * but you can override this to do whatever you want.
   */
  @Override
  public void onBackPressed() {
    try {
      dialogController.onBackPressed();
    }
    catch (Exception ex) {
      Logger.error( "Exception while onBackPressed(), dialog controller ID="+getDlgCtrlName(), ex );
      super.onBackPressed(); // Make sure a crashed activity (dialog) can be left via back button
    }
  }

  protected void onResume() {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onResume" );
    super.onResume();
    // Check whether the App/Activity we have an ActionBar. If not call activate() right here, because
    // in that case onCreateOptionsMenu will never get called.
    if (getActionBar() == null) {
      if (dialogController != null) {
        activate();
      }
    }
  }

  @Override
  public void onWindowFocusChanged( boolean hasFocus ) {
    super.onWindowFocusChanged( hasFocus );
    if (hasFocus) {
      InputMethodManager imm =  (InputMethodManager) getApplicationContext().getSystemService( Activity.INPUT_METHOD_SERVICE );
      imm.hideSoftInputFromWindow( getWindow().getDecorView().getRootView().getWindowToken(),0 );
    }
  }

  public synchronized void activate() {
    if (!active) {
      try {
        /**
         * If we got an activityResult from on onActivityResult() forward it to the appropriate controller.
         * Moved code to call childClosed() from the onStart() method to onResume() due to the fact that some
         * Android Intents e.g. Photo-Chooser on tablet do not stop the activity and onStart() will not get called.
         */
        dialogController.activate( dialog );

        if (savedInstanceState != null) {
          restoreDialogController( savedInstanceState );
        }

        // It is important that following happens after dialogController.activate() because onResume() calls
        // delegate.dialogShowing() which expects UI to be connected to the views
        dialog.onResume();

        if (activityResult != null) {
          processActivityResult();
        }
      }
      catch (Exception ex) {
        Logger.error( "Exception while activating dialog controller ID=" + getDlgCtrlName(), ex );
      }
      this.active = true;
    }
  }

  public int getActionMenuLayoutID() {
    return -1;
  }

  public Menu getOptionsMenu() {
    return optionsMenu;
  }

  /**
   * Hooks into Menubar creation and adds our actionmenu buttons
   *
   * @param menu
   * @return
   */
  @Override
  public boolean onCreateOptionsMenu( Menu menu ) {
    try {
      int menuLayoutID = getActionMenuLayoutID();
      if (menuLayoutID > 0) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate( getActionMenuLayoutID(), menu );
      }
      optionsMenu = menu;

      if (dialogController == null) {  // may be null while system startup
        Logger.info( "in " + getName() + " onCreateOptionsMenu: dialogController is null" );
        if (Platform.getInstance().getStartupProgress() == 100) {
          activate();
        }
      }
      else {
        activate();
      }
      return true;
    }
    catch (Exception ex) {
      Logger.error( "Error in onCreateOptionsMenu", ex );
      return false;
    }
  }

  protected void onPause() {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onPause" );
    super.onPause();
  }

  protected void onStop() {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onStop" );
    super.onStop();
    // Wenn hier "deactivate" passiert muss in resume() "activate" passieren, da beim Aufruf eines
    // Kind-Dialogs auch onStop() gerufen wird --> sonst tun hier die Controls nicht mehr
  }

  protected void onDestroy() {
    if (DEBUG_STATES) Logger.info( "in "+getName()+" onDestroy" );
    super.onDestroy();
    try {
      if (dialogController != null) {
        dialogController.deactivate( false ); // must be false to avoid saveValue()
      }
    }
    catch (Exception ex) {
      Logger.error( "Exception while deactivating dialog controller ID="+getDlgCtrlName(), ex );
    }
    this.active = false;
  }

  protected void logDeviceConfiguration() {
    Logger.info( "----- Device Info: " );
    Logger.info( "Build.DEVICE: " + Build.DEVICE );
    Logger.info( "Build.VERSION.RELEASE: " + Build.VERSION.RELEASE );
    Logger.info( "Build.VERSION.SDK_INT: " + Build.VERSION.SDK_INT );
    Configuration config = getResources().getConfiguration();
    Logger.info( "----- Device Configuration: " + config.toString() );
    Logger.info( "screen height (dp): " + Integer.toString( config.screenHeightDp) );
    Logger.info( "screen width (dp): " + Integer.toString( config.screenWidthDp) );
    Logger.info( "screen smallest width (dp): " + Integer.toString( config.smallestScreenWidthDp) );
    int screenLayoutSize = config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
    if (screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_SMALL) {
      Logger.info( "screen size: small" );
    }
    else if (screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
      Logger.info( "screen size: normal" );
    }
    else if (screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_LARGE) {
      Logger.info( "screen size: large" );
    }
    else if (screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
      Logger.info( "screen size: xlarge" );
    }
    else if (screenLayoutSize == Configuration.SCREENLAYOUT_SIZE_UNDEFINED) {
      Logger.info( "screen size: undefined" );
    }
    int screenLayoutLong = config.screenLayout & Configuration.SCREENLAYOUT_LONG_MASK;
    if (screenLayoutLong == Configuration.SCREENLAYOUT_LONG_YES) {
      Logger.info( "...longer as normal");
    }
    Logger.info( "font scale: " + Float.toString( config.fontScale ));
    // info( "density: " + config.densityDpi ); // TODO: API level 17 (4.2)

    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    Logger.info( "----- Display Metrics: " + metrics.toString());
    Logger.info( "density: " + Float.toString( metrics.density ));
    Logger.info( "density dpi: " + Integer.toString( metrics.densityDpi ));
    Logger.info( "scaled density: " + Float.toString( metrics.scaledDensity ));
    Logger.info( "height pixels: " + Integer.toString( metrics.heightPixels ));
    Logger.info( "width pixels: " + Integer.toString( metrics.widthPixels ));
    Logger.info( "xdpi: " + Float.toString( metrics.xdpi ));
    Logger.info( "ydpi: " + Float.toString( metrics.ydpi ));

    Logger.info( "----- end of configuration");
  }

  public static ParameterList intentToParamList( Intent intent ) throws Exception {
    ParameterList resultList;
    Bundle extras = intent.getExtras();
    Uri dataUri = intent.getData();
    if ((dataUri != null) && dataUri.getScheme().equals( ParameterList.DATAURI_SCHEME_CLASS )) {
      resultList = (ParameterList) Class.forName( dataUri.getHost() ).newInstance();
      if (extras != null) {
        for (int i = 0; i < resultList.size(); i++) {
          QName paramKey = resultList.getName( i );
          ParameterType parameterType = resultList.getType( i );
          switch (parameterType) {
            case BooleanType: {
              resultList.setBoolean( paramKey, extras.getBoolean( paramKey.toString() ) );
              break;
            }
            case DoubleType: {
              resultList.setDouble( paramKey, extras.getDouble( paramKey.toString() ) );
              break;
            }
            case IntegerType: {
              resultList.setInteger( paramKey, extras.getInt( paramKey.toString() ) );
              break;
            }
            case DateType: {
              resultList.setDate( paramKey, new Date( extras.getLong( paramKey.toString() ) ) );
              break;
            }
            case StringType: {
              resultList.setString( paramKey, extras.getString( paramKey.toString() ) );
              break;
            }
            case QNameType: {
              String value = extras.getString( paramKey.toString() );
              if (value == null) {
                resultList.setQName( paramKey, null );
              }
              else {
                Namespace ns = Namespace.getInstance( extras.getString( NAMESPACE_OF + paramKey.toString() ) );
                resultList.setQName( paramKey, ns.getQName( value ) );
              }
              break;
            }
            default:
              throw new IllegalArgumentException( "Unknown parameter type: " + parameterType );
          }
        }
      }
    }
    else if ((dataUri != null) && dataUri.getScheme().equals( ContentResolver.SCHEME_CONTENT )) {
      ParameterType[] typeList = new ParameterType[1];
      QName[] nameList = new QName[1];
      Object[] valueList = new Object[1];
      resultList = new AbstractParameterList( dataUri.toString(), typeList, nameList, valueList );
    }
    else {
      if (extras == null) {
        resultList = AbstractParameterList.EMPTY;
      }
      else {
        int size = extras.size();
        ParameterType[] typeList = new ParameterType[size];
        QName[] nameList = new QName[size];
        Object[] valueList = new Object[size];
        Namespace ns = ComponentFactory.NAMESPACE;

        int i = 0;
        for (String keyStr : extras.keySet()) {
          nameList[i] = ns.getQName( keyStr );
          Object value = extras.get( keyStr );
          if (value == null) {
            typeList[i] = ParameterType.StringType;
            valueList[i] = value;
          }
          else if (value instanceof Boolean) {
            typeList[i] = ParameterType.BooleanType;
            valueList[i] = value;
          }
          else if (value instanceof Date) {
            typeList[i] = ParameterType.DateType;
            valueList[i] = value;
          }
          else if (value instanceof Double) {
            typeList[i] =  ParameterType.DoubleType;
            valueList[i] = (Double) value;
          }
          else if (value instanceof Float) {
            typeList[i] =  ParameterType.DoubleType;
            valueList[i] = new Double( ((Float) value).doubleValue() );
          }
          else if (value instanceof Integer) {
            typeList[i] = ParameterType.IntegerType;
            valueList[i] = (Integer) value;
          }
          else if (value instanceof Byte) {
            typeList[i] = ParameterType.IntegerType;
            valueList[i] = new Integer( ((Byte) value).intValue() );
          }
          else if (value instanceof Short) {
            typeList[i] = ParameterType.IntegerType;
            valueList[i] = new Integer( ((Short) value).intValue() );
          }
          else if (value instanceof QName) {
            typeList[i] = ParameterType.QNameType;
            valueList[i] = (QName) value;
          }
          else {
            typeList[i] = ParameterType.StringType;
            valueList[i] = value.toString();
          }
        }
        if (dataUri == null) {
          resultList = new AbstractParameterList( null, typeList, nameList, valueList );
        }
        else {
          resultList = new AbstractParameterList( dataUri.toString(), typeList, nameList, valueList );
        }
      }
    }
    return resultList;
  }

  public static void paramListToIntent( ParameterList parameterList, Intent intent ) {
    String dataUriStr = parameterList.getDataUri();
    if (dataUriStr != null) {
      intent.setData( Uri.parse( dataUriStr ) );
    }
    for (int i = 0; i < parameterList.size(); i++) {
      QName paramKey = parameterList.getName( i );
      ParameterType parameterType = parameterList.getType( i );
      switch (parameterType) {
        case BooleanType: {
          intent.putExtra( paramKey.toString(), parameterList.getBoolean( i ) );
          break;
        }
        case DoubleType: {
          intent.putExtra( paramKey.toString(), parameterList.getDouble( i ) );
          break;
        }
        case IntegerType: {
          intent.putExtra( paramKey.toString(), parameterList.getInteger( i ) );
          break;
        }
        case DateType: {
          intent.putExtra( paramKey.toString(), parameterList.getDate( i ).getTime() );
          break;
        }
        case StringType: {
          intent.putExtra( paramKey.toString(), parameterList.getString( i ) );
          break;
        }
        case QNameType: {
          QName value = parameterList.getQName( i );
          if (value == null) {
            intent.putExtra( paramKey.toString(), (String) null );
          }
          else {
            intent.putExtra( NAMESPACE_OF + paramKey.toString(), value.getNamespace().getUri() );
            intent.putExtra( paramKey.toString(), value.getName() );
          }
          break;
        }
        default: throw new IllegalArgumentException( "Unknown parameter type: "+parameterType );
      }
    }
  }

  public void addRightsRequireListener( RightsRequireListener rightsRequireListener ) {
    rightsRequireListeners.add( rightsRequireListener );
  }

  public void callbackRightsRequireListeners( boolean rightsGranted ) {
    for (RightsRequireListener listener : rightsRequireListeners) {
      listener.rightsRequired(rightsGranted);
    }
  }

  public abstract void requireRights();
}
