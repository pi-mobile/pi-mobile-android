/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.view.figure.AnimationStyle;
import de.pidata.gui.view.figure.AnimationType;
import de.pidata.gui.view.figure.BitmapAdjust;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.qnames.QName;

import java.util.Objects;

/**
 * Provides a drawable for a bitmap which can be animated.
 *
 * @author dsc
 */
public class AnimatedBitmapDrawable extends BitmapDrawable implements Runnable {

  private static final boolean DEBUG = false;

  private AnimationStyle animationStyle;
  private AnimationType animationType;
  private AndroidPaintView androidPaintView;

  protected Bitmap scaledBitmap;
  protected Paint bitmapPaint;
  private int bitmapX;
  private int bitmapY;

  private boolean restartAnimation = true;

  private Handler animationHandler;

  public AnimatedBitmapDrawable( QName bitmapID, ComponentBitmap bitmap, BitmapAdjust bitmapAdjust, AnimationType animationType, ShapeStyle shapeStyle ) {
    super( bitmapID, bitmap, bitmapAdjust, shapeStyle );
    if (shapeStyle instanceof AnimationStyle) {
      this.animationStyle = (AnimationStyle) shapeStyle;
    }
    else {
      this.animationStyle = AnimationStyle.DEFAULT_ANIMATION_STYLE;
    }
    this.animationType = animationType;

    this.animationHandler = new Handler();

    this.bitmapPaint = new Paint();

    // use actual bounds later
    calculateAnimation( bitmapID, bitmap, bitmapAdjust, animationType,0, 0, androidPaintView );
  }

  /**
   * Loads the bitmap and scales it if necessary. Calculates the coordinates necessary
   * to start animation.
   *
   * @param bitmapID
   * @param bitmapAdjust
   * @param width         border of the animation
   * @param height        border of the animation
   * @param androidPaintView
   */
  public void calculateAnimation( QName bitmapID, ComponentBitmap componentBitmap, BitmapAdjust bitmapAdjust, AnimationType animationType, int width, int height, AndroidPaintView androidPaintView ) {

    this.animationType = animationType;
    this.androidPaintView = androidPaintView;

    if (bitmapID != this.bitmapID) {
      this.bitmapID = bitmapID;
      if (bitmapID == null) {
        componentBitmap = null;
      }
      else {
        componentBitmap = Platform.getInstance().getBitmap( bitmapID );
      }
    }
    if (componentBitmap == null) {
      this.bitmap = null;
      this.scaledBitmap = null;
    }
    else {
      this.bitmap = (Bitmap) componentBitmap.getImage();
    }
    if (bitmap != null) {
      switch (bitmapAdjust) {
        case SCALE: {
          switch (animationType) {
            case RUNNING_TO_RIGHT:
            case RUNNING_TO_LEFT: {
              // horizontal movement: scale vertical
              double bitmapHeight = bitmap.getHeight();
              double bitmapWidth = bitmap.getWidth();
              double scaleFactor = height / bitmapHeight;
              int newBitmapHeight = (int) Math.round( bitmapHeight * scaleFactor );
              int newBitmapWidth = (int) Math.round( bitmapWidth * scaleFactor );
              if( newBitmapHeight > 0 && newBitmapWidth > 0) {
                scaledBitmap = Bitmap.createScaledBitmap( bitmap, newBitmapWidth, newBitmapHeight, true );
              }
              bitmapY = (height - newBitmapHeight) / 2;
              break;
            }
            default: {
              throw new RuntimeException( "unsupported combination: " + bitmapAdjust + "/" + animationType );
            }
          }
          break;
        }
        case CENTER: {
          switch (animationType) {
            case RUNNING_TO_RIGHT:
            case RUNNING_TO_LEFT: {
              // horizontal movement: adjust vertical
              int bitmapHeight = bitmap.getHeight();
              bitmapY = (height - bitmapHeight) / 2;
              scaledBitmap = bitmap;
              break;
            }
            default: {
              throw new RuntimeException( "unsupported combination: " + bitmapAdjust + "/" + animationType );
            }
          }
          break;
        }
        case NONE: {
          switch (animationType) {
            case RUNNING_TO_RIGHT:
            case RUNNING_TO_LEFT: {
              // horizontal movement: adjust vertical
              bitmapY = 0;
              scaledBitmap = bitmap;
              break;
            }
            default: {
              throw new RuntimeException( "unsupported combination: " + bitmapAdjust + "/" + animationType );
            }
          }
          break;
        }
        default: {
          throw new RuntimeException( "unsupported bitmap adjustment: " + bitmapAdjust );
        }
      }
    }
    else {
      scaledBitmap = null;
    }
  }


  protected void doDrawBitmap( Canvas canvas ) {
    Bitmap bitmap = this.scaledBitmap;
    if (bitmap != null) {
      switch (animationType) {
        case RUNNING_TO_LEFT:
          animateToLeft( canvas, bitmap );
          break;

        case RUNNING_TO_RIGHT:
          animateToRight( canvas, bitmap );
          break;

        default:
          throw new RuntimeException( "unsupported animation type: " + animationType );
      }
      //--Redraw animation if active
      animationHandler.postDelayed( this, animationStyle.getAnimationDelay() );
    }
  }

  /**
   * Draws the bitmap moving to the right.
   *
   * @param canvas where the animation should be drawn
   */
  private void animateToRight( Canvas canvas, Bitmap bitmap ) {

    Rect bounds = getBounds();
    int startLeft = 0;
    int stopRight = bounds.width();

    int offset = (int) (bitmap.getWidth() * animationStyle.getOffsetRatio());
    int startX = startLeft - (bitmap.getWidth());
    int secondX = startX + offset;

    if (restartAnimation) {
      bitmapX = startX;
      restartAnimation = false;
    }
    else {
      bitmapX++;
    }
    canvas.drawBitmap( bitmap, bitmapX, bitmapY, bitmapPaint );

    if (offset > 0) {
      int nextX = bitmapX + offset;
      while (nextX <= (stopRight - bitmap.getWidth())) {
        canvas.drawBitmap( bitmap, nextX, bitmapY, bitmapPaint );
        nextX += (offset);
      }
    }

    if (bitmapX >= secondX) {
      restartAnimation = true;
    }
  }

  /**
   * Draws the bitmap moving to the left.
   *
   * @param canvas where the animation should be drawn
   */
  private void animateToLeft( Canvas canvas, Bitmap bitmap ) {

    Rect bounds = getBounds();
    int stopLeft = 0;
    int startRight = bounds.width();

    int offset = (int) (bitmap.getWidth() * animationStyle.getOffsetRatio());
    int startX = startRight - bitmap.getWidth();
    int secondX = startX - offset;

    if (restartAnimation) {
      bitmapX = startX;
      restartAnimation = false;
    }
    else {
      bitmapX--;
    }
    canvas.drawBitmap( bitmap, bitmapX, bitmapY, bitmapPaint );
    if (offset > 0) {
      int nextX = bitmapX - (offset);
      while (nextX >= stopLeft - (bitmap.getWidth() * 2)) {
        canvas.drawBitmap( bitmap, nextX, bitmapY, bitmapPaint );
        nextX -= (offset);
      }
    }

    if (bitmapX <= secondX) {
      restartAnimation = true;
    }
  }

  /**
   * Starts executing the active part of the class' code. This method is
   * called when a thread is started that has been created with a class which
   * implements {@code Runnable}.
   */
  @Override
  public void run() {
    androidPaintView.invalidate( getDirtyBounds() );
  }

}