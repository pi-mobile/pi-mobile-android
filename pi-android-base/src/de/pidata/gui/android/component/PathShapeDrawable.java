/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Path;
import android.graphics.drawable.shapes.PathShape;
import de.pidata.gui.view.figure.path.ArcPathElement;
import de.pidata.gui.view.figure.path.PathElement;
import de.pidata.gui.view.figure.path.PathElementType;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.rect.Pos;
import de.pidata.rect.Rect;

/**
 * Provides a drawable for a compound (multiple contour) geometric path.
 *
 * @author dsc
 */
public class PathShapeDrawable extends ShapeDrawableBorder {

  private static final boolean DEBUG = true;

  private PathElement[] pathElementDefinition;

  public PathShapeDrawable( PathElement[] pathElementDefinition, ShapeStyle shapeStyle ) {
    super( shapeStyle );
    this.pathElementDefinition = pathElementDefinition;

    // use actual bounds later
    calculatePath( 0, 0 );
  }

  /**
   * Recalculates the path shape.
   * Hint: the drawable's bounds are set to the surrounding view's bounds;
   * otherwise the path's expansion will not be shown.
   *
   * @param width
   * @param height
   */
  public void calculatePath( int width, int height ) {
    Path path = new Path();
    for (int i = 0; i < pathElementDefinition.length; i++) {
      PathElement pathElement = pathElementDefinition[i];
      PathElementType pathType = pathElement.getPathType();
      switch (pathType) {
        case none: {
          Pos pos = pathElement.getPos();
          path.moveTo( (float) pos.getX(), (float) pos.getY() );
          break;
        }
        case arc: {
          // arc is fitted inside the bounds
          ArcPathElement arcPathElement = (ArcPathElement) pathElement;
          Rect arcBounds = arcPathElement.getBounds();
          // adjust start angle: Android starts with 0 = right!
          float nativeStartAngle = (float) arcPathElement.getStartAngle() - 90;
          path.arcTo( (float) arcBounds.getX(), (float) arcBounds.getY(), (float) arcBounds.getRight(), (float) arcBounds.getBottom(), nativeStartAngle, (float) arcPathElement.getSweepAngle(), false );
          break;
        }
        case line:
        default: {
          Pos pos = pathElement.getPos();
          path.lineTo( (float) pos.getX(), (float) pos.getY() );
        }
      }
    }
    PathShape pathShape = new PathShape( path, width, height );
    setShape( pathShape );
  }
}
