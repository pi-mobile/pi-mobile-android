/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;

public class TextShape extends Shape {

  private String text;
  private double textX;
  private double textY;

  /**
   * The text reference point 0/0 specifies the point at which the text placement
   * starts to calculate. For default left alignment this is the leftmost point
   * on the baseline.
   *
   * To draw the text within given bounds the x/y must be adjusted by the requester
   * according to the surrounding bounds.
   *
   * @param text
   * @param textX the adjusted left position within the surrounding bounds
   * @param textY the adjusted top position within the surrounding bounds
   */
  public TextShape( String text, double textX, double textY ) {
    this.text = text;
    this.textX = textX;
    this.textY = textY;
  }

  @Override
  public void draw( Canvas canvas, Paint paint ) {
    canvas.drawText( text, (float) textX, (float) textY, paint );
  }
}
