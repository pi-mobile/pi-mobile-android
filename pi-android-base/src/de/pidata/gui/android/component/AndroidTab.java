package de.pidata.gui.android.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;
import de.pidata.gui.android.UIFactoryAndroid;

public class AndroidTab extends RadioButton {

  private int containerId = -1;

  public AndroidTab( Context context ) {
    super( context );
  }

  public AndroidTab( Context context, AttributeSet attrs ) throws InstantiationException {
    super( context, attrs );
    // Get the custom attribute tabContainer if available.
    if (attrs != null) {
      this.containerId = attrs.getAttributeResourceValue( UIFactoryAndroid.NAMESPACE_APP.getUri(), "tabContainer" , -1 );
    }
  }

  public AndroidTab( Context context, AttributeSet attrs, int defStyleAttr ) {
    super( context, attrs, defStyleAttr );
  }

  public AndroidTab( Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
    super( context, attrs, defStyleAttr, defStyleRes );
  }

  public int getContainerId(){
    return containerId;
  }
}
