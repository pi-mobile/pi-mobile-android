/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.view.figure.ShapeStyle;

/**
 * Provides a drawable for drawing a text as a {@link TextShape}.
 *
 * @author dsc
 */
public class TextShapeDrawable extends ShapeDrawable implements StyledDrawable {

  private ShapeStyle shapeStyle;
  private String text;

  public TextShapeDrawable( String text, ShapeStyle shapeStyle ) {
    super();
    this.text = text;
    this.shapeStyle = shapeStyle;

    // use actual bounds later
    calculateText( text, 0, 0 );
  }

  @Override
  public void shapeStyleChanged() {
    switch (shapeStyle.getTextAlign()) {
      case RIGHT: {
        getPaint().setTextAlign( Paint.Align.RIGHT );
        break;
      }
      case CENTER: {
        getPaint().setTextAlign( Paint.Align.CENTER );
        break;
      }
      case LEFT:
      default: {
        getPaint().setTextAlign( Paint.Align.LEFT );
      }
    }
    getPaint().setTypeface( Typeface.create( shapeStyle.getFontName(), Typeface.NORMAL ) );

    ComponentColor fillColor = shapeStyle.getBorderColor();
    if (fillColor != null) {
      if (shapeStyle.isFillColorTransparent()) {
        getPaint().setColor( ((AndroidColor) ShapeStyle.COLOR_TEMP_TRANSPARENT).getColorInt() );
      }
      else {
        getPaint().setColor( ((AndroidColor) fillColor).getColorInt() );
      }
    }
  }

  /**
   * Recalculate the text shape.
   * Text size is adjusted to bounds's height.
   *
   *  @param text
   * @param width
   * @param height
   */
  public void calculateText( String text, int width, int height ) {
    double textX;
    switch (shapeStyle.getTextAlign()) {
      case RIGHT: {
        textX = width;
        break;
      }
      case CENTER: {
        textX = width / 2;
        break;
      }
      case LEFT:
      default: {
        textX = 0;
      }
    }
    double textY = height - getPaint().getFontMetrics().bottom;
    TextShape textShape = new TextShape( text, textX, textY );
    setShape( textShape );
    getPaint().setTextSize( (float) height );
  }

  @Override
  public ShapeStyle getShapeStyle() {
    return shapeStyle;
  }

  /**
   * Needed for shadow support. The shadow is rendered on the main paint.
   *
   * @param androidPaintView
   */
  @Override
  public void enableShadow( AndroidPaintView androidPaintView ) {
    int layerType = androidPaintView.getLayerType();
    if (layerType != View.LAYER_TYPE_SOFTWARE) {
      androidPaintView.setLayerType( View.LAYER_TYPE_SOFTWARE, getPaint() );
    }
  }
}
