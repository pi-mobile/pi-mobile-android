/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import de.pidata.gui.view.figure.ShapeStyle;

/**
 * Provides a drawable for a simple arc.
 *
 * @author dsc
 */
public class ArcShapeDrawable extends ShapeDrawableBorder {

  private static final boolean DEBUG = true;

  private boolean useCenter;

  /**
   * Creates a drawable for a simple arc.
   *
   * @param startAngle the angle (in degrees) where the arc begins; 0 = top
   * @param sweepAngle the sweep angle (in degrees). Anything equal to or
   *                   greater than 360 results in a complete circle/oval.
   * @param useCenter if true: draw lines to the center
   * @param shapeStyle
   */
  public ArcShapeDrawable( double startAngle, double sweepAngle, boolean useCenter, ShapeStyle shapeStyle ) {
    super( shapeStyle );
    this.useCenter = useCenter;

    calculateArc( startAngle, sweepAngle );
  }

  /**
   * Recalculates the arc shape. Apex and radius are set by the surrounding bounds.
   *
   * @param startAngle the angle (in degrees) where the arc begins; 0 = top
   * @param sweepAngle the sweep angle (in degrees). Anything equal to or
   *                   greater than 360 results in a complete circle/oval.
   */
  public void calculateArc( double startAngle, double sweepAngle ) {
    ArcShape arcShape = new ArcShape( (float) startAngle, (float) sweepAngle, useCenter );
    setShape( arcShape );
  }

}
