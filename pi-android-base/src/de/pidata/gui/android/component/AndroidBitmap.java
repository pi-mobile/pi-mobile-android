/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentGraphics;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class AndroidBitmap implements ComponentBitmap {

  private Bitmap bitmap;

  public AndroidBitmap( Bitmap bitmap ) {
    this.bitmap = bitmap;
  }

  public ComponentGraphics getGraphics() {
    //TODO
    throw new RuntimeException( "TODO ");
  }

  public int getWidth() {
    return bitmap.getWidth();
  }

  public int getHeight() {
    return bitmap.getHeight();
  }

  public Object getImage() {
    return bitmap;
  }

  /**
   * Saves this bitmap to the given OutputStream
   *
   * @param outStream the strem to which image will be written
   * @param imageType the image type, see constants (TIFF, JPEG, ...)
   */
  public void save( OutputStream outStream, QName imageType ) throws IOException {
    if (imageType == PNG) {
      bitmap.compress( Bitmap.CompressFormat.PNG, 90, outStream );
    }
    else if (imageType == GIF) {
      //TODO
      throw new RuntimeException( "TODO ");
    }
    else if (imageType == JPEG) {
      bitmap.compress( Bitmap.CompressFormat.JPEG, 90, outStream );
    }
    else if (imageType == TIFF) {
      //TODO
      throw new RuntimeException( "TODO ");
    }
  }

  @Override
  public int size() {
    return bitmap.getByteCount();
  }

  @Override
  public byte[] getBytes() {
    ByteBuffer byteBuffer = ByteBuffer.allocate( bitmap.getByteCount() );
    bitmap.copyPixelsToBuffer( byteBuffer );
    byteBuffer.rewind();
    return byteBuffer.array();
  }

  @Override
  public void readBytes( InputStream byteStream ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void writeBytes( OutputStream out ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
