/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.annotation.NonNull;
import de.pidata.gui.android.adapter.AndroidShapeAdapter;
import de.pidata.gui.view.figure.Figure;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.log.Logger;

import java.util.ArrayList;
import java.util.List;

public class AndroidPaintView extends View {

  private List<AndroidShapeAdapter> shapeAdapterList = new ArrayList<AndroidShapeAdapter>();

  public AndroidPaintView( Context context ) {
    super( context );
    setLongClickable( true );
  }

  public AndroidPaintView( Context context, AttributeSet attrs ) {
    super( context, attrs );
    setLongClickable( true );
  }

  public AndroidPaintView( Context context, AttributeSet attrs, int defStyleAttr ) {
    super( context, attrs, defStyleAttr );
    setLongClickable( true );
  }

  public AndroidPaintView( Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
    super( context, attrs, defStyleAttr, defStyleRes );
    setLongClickable( true );
  }

  @Override
  protected void onDraw( Canvas canvas ) {
    super.onDraw( canvas );
    try {
      synchronized (shapeAdapterList) {
        for (AndroidShapeAdapter shapeAdapter : shapeAdapterList) {
          Drawable drawable = shapeAdapter.getDrawable();
          drawable.draw( canvas );
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in onDraw", ex );
    }
  }

  public void add( AndroidShapeAdapter shapeAdapter ) {
    synchronized (shapeAdapterList) {
      this.shapeAdapterList.add( shapeAdapter );
    }
  }

  public void remove( AndroidShapeAdapter shapeAdapter ) {
    synchronized (shapeAdapterList) {
      this.shapeAdapterList.remove( shapeAdapter );
    }
  }

  public void removeFigureShapes( Figure figure ) {
    synchronized (shapeAdapterList) {
      for (int k = shapeAdapterList.size() - 1; k >= 0; k--) {
        AndroidShapeAdapter shapeAdapter = shapeAdapterList.get( k );
        ShapePI shapePI = shapeAdapter.getShapePI();
        if (shapePI.getFigure() == figure) {
          remove( shapeAdapter );
          shapePI.detachUI();
        }
      }
    }
  }

  final GestureDetector gestureDetector = new GestureDetector( new GestureDetector.SimpleOnGestureListener() {

    @Override
    public void onLongPress(MotionEvent event) {
      try {
        float x = event.getX();
        float y = event.getY();
        synchronized (shapeAdapterList) {
          for (int i = shapeAdapterList.size() - 1; i >= 0; i--) {
            AndroidShapeAdapter shapeAdapter = shapeAdapterList.get( i );
            Rect bounds = shapeAdapter.getDrawable().getBounds();
            if ((x > bounds.left) && (x < bounds.right) && (y > bounds.top) && (y < bounds.bottom)) {
              shapeAdapter.onClick( 2, x, y );
            }
          }
        }
      }
      catch (Exception ex) {
        Logger.error( "Error in onTouchEvent", ex );
      }
    }

    @Override
    public boolean onSingleTapUp( @NonNull MotionEvent event ) {
      try {
        float x = event.getX();
        float y = event.getY();
        synchronized (shapeAdapterList) {
          for (int i = shapeAdapterList.size() - 1; i >= 0; i--) {
            AndroidShapeAdapter shapeAdapter = shapeAdapterList.get( i );
            Rect bounds = shapeAdapter.getDrawable().getBounds();
            if ((x > bounds.left) && (x < bounds.right) && (y > bounds.top) && (y < bounds.bottom)) {
              shapeAdapter.onClick( 1, x, y );
              return true;
            }
          }
        }
      }
      catch (Exception ex) {
        Logger.error( "Error in onTouchEvent", ex );
      }
      return super.onSingleTapUp( event );
    }

  } );

  /**
   * Implement this method to handle touch screen motion events.
   * <p>
   * If this method is used to detect click actions, it is recommended that
   * the actions be performed by implementing and calling
   * {@link #performClick()}. This will ensure consistent system behavior,
   * including:
   * <ul>
   * <li>obeying click sound preferences
   * <li>dispatching OnClickListener calls
   * <li>handling  when
   * accessibility features are enabled
   * </ul>
   *
   * @param event The motion event.
   * @return True if the event was handled, false otherwise.
   */
  @Override
  public boolean onTouchEvent( MotionEvent event ) {
    if (gestureDetector.onTouchEvent(event)) {
        return true;
    }
    return super.onTouchEvent(event);
  }

}
