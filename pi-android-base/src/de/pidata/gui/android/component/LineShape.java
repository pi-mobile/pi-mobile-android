/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.shapes.Shape;
import de.pidata.rect.Pos;

/**
 * Shape representing a line with dynamic start ans end.
 *
 * @author dsc
 */
public class LineShape extends Shape {

  private Pos startPos;
  private Pos endPos;

  public LineShape( Pos startPos, Pos endPos ) {
    this.startPos = startPos;
    this.endPos = endPos;
  }

  /**
   * Draw this shape into the provided Canvas, with the provided Paint.
   * Before calling this, you must call {@link #resize(float, float)}.
   *
   * @param canvas the Canvas within which this shape should be drawn
   * @param paint  the Paint object that defines this shape's characteristics
   */
  @Override
  public void draw( Canvas canvas, Paint paint ) {
    canvas.drawLine( (float) startPos.getX(), (float) startPos.getY(), (float) endPos.getX(), (float) endPos.getY(), paint );
  }
}
