/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import de.pidata.log.Logger;

import java.io.OutputStream;

public class AndroidSignatureView extends View {
  private static final float STROKE_WIDTH = 5f;
  private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

  private Paint paint = new Paint();
  private Path path = new Path();

  private float lastTouchX;
  private float lastTouchY;
  private final RectF dirtyRect = new RectF();

  private LinearLayout mContent;
  private Button mClear, mGetSign, mCancel;
  public static String tempDir;
  public int count = 1;
  public String current = null;
  private Bitmap mBitmap;

  public AndroidSignatureView( Context context, AttributeSet attrs ) {
    super( context, attrs );
    paint.setAntiAlias( true );
    paint.setColor( Color.BLACK );
    paint.setStyle( Paint.Style.STROKE );
    paint.setStrokeJoin( Paint.Join.ROUND );
    paint.setStrokeWidth( STROKE_WIDTH );
  }

  public void save( OutputStream outStream ) {
    if (mBitmap == null) {
      mBitmap = Bitmap.createBitmap( mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565 );
    }
    Canvas canvas = new Canvas( mBitmap );
    try {
      draw( canvas );
      mBitmap.compress( Bitmap.CompressFormat.PNG, 90, outStream );
      outStream.flush();
      outStream.close();
    }
    catch (Exception e) {
      Logger.debug( e.toString() );
    }
  }

  public void clear() {
    path.reset();
    invalidate();
  }

  protected void onDraw( Canvas canvas ) {
    canvas.drawPath( path, paint );
  }

  public boolean onTouchEvent( MotionEvent event ) {
    float eventX = event.getX();
    float eventY = event.getY();
    mGetSign.setEnabled( true );

    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        path.moveTo( eventX, eventY );
        lastTouchX = eventX;
        lastTouchY = eventY;
        return true;

      case MotionEvent.ACTION_MOVE:

      case MotionEvent.ACTION_UP:

        resetDirtyRect( eventX, eventY );
        int historySize = event.getHistorySize();
        for (int i = 0; i < historySize; i++) {
          float historicalX = event.getHistoricalX( i );
          float historicalY = event.getHistoricalY( i );
          expandDirtyRect( historicalX, historicalY );
          path.lineTo( historicalX, historicalY );
        }
        path.lineTo( eventX, eventY );
        break;

      default:
        return false;
    }

    invalidate( (int) (dirtyRect.left - HALF_STROKE_WIDTH),
        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH) );

    lastTouchX = eventX;
    lastTouchY = eventY;

    return true;
  }

  private void expandDirtyRect( float historicalX, float historicalY ) {
    if (historicalX < dirtyRect.left) {
      dirtyRect.left = historicalX;
    } else if (historicalX > dirtyRect.right) {
      dirtyRect.right = historicalX;
    }

    if (historicalY < dirtyRect.top) {
      dirtyRect.top = historicalY;
    } else if (historicalY > dirtyRect.bottom) {
      dirtyRect.bottom = historicalY;
    }
  }

  private void resetDirtyRect( float eventX, float eventY ) {
    dirtyRect.left = Math.min( lastTouchX, eventX );
    dirtyRect.right = Math.max( lastTouchX, eventX );
    dirtyRect.top = Math.min( lastTouchY, eventY );
    dirtyRect.bottom = Math.max( lastTouchY, eventY );
  }
}
