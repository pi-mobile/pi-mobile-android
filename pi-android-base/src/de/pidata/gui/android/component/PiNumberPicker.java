package de.pidata.gui.android.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.string.Helper;

public class PiNumberPicker extends LinearLayout {

  public static final int DEFAULT_STEP_SIZE = 1;
  private Integer minValue;
  private Integer maxValue;
  private Integer stepSize = DEFAULT_STEP_SIZE;

  public PiNumberPicker( Context context ) {
    super( context );
    initLayout();
  }

  public PiNumberPicker( Context context, @Nullable AttributeSet attrs ) {
    super( context, attrs );
    initLayout();
    if (attrs != null) {
      initAttributes( attrs );
    }
  }

  public PiNumberPicker( Context context, @Nullable AttributeSet attrs, int defStyleAttr ) {
    super( context, attrs, defStyleAttr );
    initLayout();
    if (attrs != null) {
      initAttributes( attrs );
    }
  }

  public PiNumberPicker( Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
    super( context, attrs, defStyleAttr, defStyleRes );
    initLayout();
    if (attrs != null) {
      initAttributes( attrs );
    }
  }

  public void initLayout() {
    int layoutID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_LAYOUT, ControllerBuilder.NAMESPACE.getQName( "pi_number_picker" ) );
    View view = AndroidApplication.getInstance().getCurrentActivity().getLayoutInflater().inflate( layoutID, this, true );
  }

  private void initAttributes( AttributeSet attrs ) {
    String attributeName = "minValue";
    if(isAttributeSet( attributeName, attrs)){
    this.minValue = attrs.getAttributeIntValue( UIFactoryAndroid.NAMESPACE_APP.getUri(), attributeName, -1 );
    }
    attributeName = "maxValue";
    if(isAttributeSet( attributeName, attrs )){
      this.maxValue = attrs.getAttributeIntValue( UIFactoryAndroid.NAMESPACE_APP.getUri(), attributeName, -1 );
    }
    attributeName = "stepSize";
    stepSize = attrs.getAttributeIntValue( UIFactoryAndroid.NAMESPACE_APP.getUri(), attributeName, DEFAULT_STEP_SIZE );
  }

  private boolean isAttributeSet( String attributeName, AttributeSet attrs ) {
    String value = attrs.getAttributeValue( UIFactoryAndroid.NAMESPACE_APP.getUri(), attributeName );
    return !Helper.isNullOrEmpty( value );
  }

  public Integer getMinValue() {
    return minValue;
  }

  public Integer getMaxValue() {
    return maxValue;
  }

  public int getStepSize() {
    return stepSize;
  }
}
