/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.shapes.RectShape;

/**
 * Provides an alternative for the native {@link android.graphics.drawable.shapes.ArcShape}.
 *
 * @author dsc
 */
public class ArcShape extends RectShape {

  private float startAngle;
  private float sweepAngle;
  private boolean useCenter;

  /**
   * ArcShape constructor.
   *
   * @param startAngle the angle (in degrees) where the arc begins; 90 = bottom
   * @param sweepAngle the sweep angle (in degrees). Anything equal to or
   *                   greater than 360 results in a complete circle/oval.
   */
  public ArcShape( float startAngle, float sweepAngle ) {
    this( startAngle, sweepAngle, true );
  }

  /**
   * ArcShape constructor.
   *
   * @param startAngle the angle (in degrees) where the arc begins; ; 0 = top
   * @param sweepAngle the sweep angle (in degrees). Anything equal to or
   *                   greater than 360 results in a complete circle/oval.
   * @param useCenter
   */
  public ArcShape( float startAngle, float sweepAngle, boolean useCenter ) {
    this.startAngle = startAngle;
    this.sweepAngle = sweepAngle;
    this.useCenter = useCenter;
  }

  @Override
  public void draw( Canvas canvas, Paint paint ) {
    // adjust start angle: Android starts with 0 = right!
    float nativeStartAngle = startAngle - 90;
    canvas.drawArc( rect(), nativeStartAngle, sweepAngle, useCenter, paint );
  }

}
