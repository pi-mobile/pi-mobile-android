/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.*;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.Shape;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.view.figure.BitmapAdjust;
import de.pidata.gui.view.figure.ShapeStyle;
import de.pidata.qnames.QName;
import de.pidata.rect.Pos;
import de.pidata.rect.Rotation;

import java.util.Objects;

/**
 * Provides a drawable for a bitmap.
 *
 * @author dsc
 */
public class BitmapDrawable extends ShapeDrawableBorder implements StyledDrawable {

  protected QName bitmapID;
  protected Bitmap bitmap;

  protected Rect originalRect;
  protected Matrix matrix = new Matrix();
  protected Rotation rotation= null;

  public BitmapDrawable( QName bitmapID, ComponentBitmap bitmap, BitmapAdjust bitmapAdjust, ShapeStyle shapeStyle ) {
    super( new RectShape(), shapeStyle );

    // use actual bounds later
    calculateBitmap( bitmapID, bitmap, bitmapAdjust, 0, 0 );
  }

  /**
   * Loads the bitmap and scales it if necessary.
   *
   * @param bitmapID
   * @param bitmapAdjust
   * @param width         border of the bitmap
   * @param height        border of the bitmap
   */
  public void calculateBitmap( QName bitmapID, ComponentBitmap componentBitmap, BitmapAdjust bitmapAdjust, int width, int height ) {
    matrix.reset();
    if (bitmapID != this.bitmapID) {
      this.bitmapID = bitmapID;
      if (bitmapID == null) {
        componentBitmap = null;
      }
      else {
        componentBitmap = Platform.getInstance().getBitmap( bitmapID );
      }
    }
    if (componentBitmap == null) {
      this.bitmap = null;
    }
    else {
      this.bitmap = (Bitmap) componentBitmap.getImage();
      originalRect = new Rect( 0, 0, bitmap.getWidth() - 1, bitmap.getHeight() - 1 );
      /*if ((width != 0) && (height != 0)) {
        float sx = ((float) width - 1) / (bitmap.getWidth() - 1);
        float sy = ((float) height - 1) / (bitmap.getHeight() - 1);
        matrix.postScale( sx, sy );
      }
      else {
        matrix.postScale( 1.0f, 1.0f );
      } */
    }
    if (bitmap != null) {
      switch (bitmapAdjust) {
        case SCALE: {
          if (height == 0) {
            matrix.postScale( 1.0f, 1.0f, 0, 0 );
          }
          else {
            float bitmapHeight = bitmap.getHeight();
            float scaleFactor = 1.0f;
            if (height <= bitmapHeight) {
              scaleFactor = height / bitmapHeight;
            }

            matrix.postScale( scaleFactor, scaleFactor, 0, 0 );
          }
          break;
        }

        case CENTER: {
          if (height == 0) {
            matrix.postScale( 1.0f, 1.0f, (float) width / 2, (float) height / 2 );
          }
          else {
            float bitmapHeight = bitmap.getHeight();
            float scaleFactor = height / bitmapHeight;
            matrix.postScale( scaleFactor, scaleFactor, (float) width / 2, (float) height / 2 );
          }
          break;
        }

        case FITCENTER: {
          if (height == 0) {
            matrix.postScale( 1.0f, 1.0f, (float) width / 2, (float) height / 2 );
          }
          else {
            float bitmapHeight = bitmap.getHeight();
            float scaleFactor = height / bitmapHeight;
            matrix.postScale( scaleFactor, scaleFactor, (float) width / 2, (float) height / 2 );
          }
          break;
        }

        case NONE: {
          // do nothing
          break;
        }
        default: {
          throw new RuntimeException( "unsupported bitmap adjustment: " + bitmapAdjust );
        }
      }
      if (rotation != null) {
        matrix.preRotate( (float) rotation.getAngle(), originalRect.width() / 2, originalRect.height() / 2 );
      }
    }
  }

  public void applyRotation( Rotation rotation ) {
    this.rotation = rotation; // will be applied to matrix on next call to calculateBitmap()
  }

  /**
   * Called from the drawable's draw() method after the canvas has been set to
   * draw the shape at (0,0). Subclasses can override for special effects such
   * as multiple layers, stroking, etc.
   *
   * @param shape
   * @param canvas
   * @param paint
   */
  @Override
  protected void onDraw( Shape shape, Canvas canvas, Paint paint ) {
    super.onDraw( shape, canvas, paint );

    doDrawBitmap( canvas );
  }

  /**
   * Override for Specific bitmap drawing, e.g. animation.
   *
   * @param canvas
   */
  protected void doDrawBitmap( Canvas canvas ) {
    if (bitmap != null) {
      canvas.drawBitmap( bitmap, matrix, null );
    }
  }

}
