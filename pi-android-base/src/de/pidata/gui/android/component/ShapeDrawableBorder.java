/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.*;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.view.View;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.view.figure.*;
import de.pidata.log.Logger;
import de.pidata.rect.Pos;

import java.util.List;

/**
 * Adds Border support to the default {@link ShapeDrawable}.
 * The border is added as CENTER, that means half of the border width is inside and
 * half of it is outside the bounds of the shape.
 *
 * Implementation hint: This is the only implemented border type in Android; other types (INSIDE / OUTSIDE)
 * have to be implemented separately.
 *
 * @author dsc
 */
public class ShapeDrawableBorder extends ShapeDrawable implements StyledDrawable {

  private Paint borderPaint;
  private ShapeStyle shapeStyle;


  public ShapeDrawableBorder( ShapeStyle shapeStyle ) {
    super();
    init( shapeStyle );
  }

  /**
   * Creates a ShapeDrawableBorder for the given shape and the style to apply.
   * Adds a second {@link Paint} for drawing the border.
   *
   * @param shape
   * @param shapeStyle
   */
  public ShapeDrawableBorder( Shape shape, ShapeStyle shapeStyle ) {
    super( shape );
    init( shapeStyle );
  }

  private void init( ShapeStyle shapeStyle ) {
    this.shapeStyle = shapeStyle;
    StrokeType strokeType = shapeStyle.getStrokeType();
    if (strokeType != StrokeType.CENTERED) {
      Logger.info( "unsupported StrokeType '" + strokeType + "'; use default CENTERED instead" );
    }

    this.borderPaint = new Paint();
    borderPaint.setStyle( Paint.Style.STROKE );
    borderPaint.setAntiAlias( true );

    applyStyle();
  }

  /**
   * Needed for shadow support. The shadow is rendered on the main paint.
   *
   * @param androidPaintView
   */
  @Override
  public void enableShadow( AndroidPaintView androidPaintView ) {
    int layerType = androidPaintView.getLayerType();
    if (layerType != View.LAYER_TYPE_SOFTWARE) {
      androidPaintView.setLayerType( View.LAYER_TYPE_SOFTWARE, getPaint() );
    }
  }

  @Override
  public void shapeStyleChanged() {
    applyStyle();
  }

  private void applyStyle() {
    if (shapeStyle instanceof LinearGradientStyle) {
      LinearGradientStyle gradientStyle = (LinearGradientStyle) shapeStyle;
      Pos start = gradientStyle.getStartPos();
      Pos end = gradientStyle.getEndPos();
      Shader.TileMode tileMode = getCycleMethod( gradientStyle.getGradientMode() );
      List<ComponentColor> colorList = gradientStyle.getColorList();
      List<Double> offsetList = gradientStyle.getOffsetList();
      int[] colors = new int[colorList.size()];
      float[] positions = new float[colorList.size()];
      for (int i = 0; i < colorList.size(); i++) {
        positions[i] = offsetList.get( i ).floatValue();
        colors[i] = ((AndroidColor) colorList.get( i )).getColorInt();
      }
      float gradientStartX = (float) start.getX() - getBounds().left;
      float gradientStartY = (float) start.getY() - getBounds().top;
      float gradientEndX = (float) end.getX() - getBounds().left;
      float gradientEndY = (float) end.getY() - getBounds().top;
      LinearGradient gradient = new LinearGradient( gradientStartX, gradientStartY, gradientEndX, gradientEndY, colors, positions, tileMode );
      getPaint().setShader( gradient );
    }
    else if (shapeStyle instanceof CircleGradientStyle) {
      CircleGradientStyle circleGradientStyle = (CircleGradientStyle) shapeStyle;
      Pos center = circleGradientStyle.getCenterPos();
      Shader.TileMode tileMode = getCycleMethod( circleGradientStyle.getGradientMode() );
      List<ComponentColor> colorList = circleGradientStyle.getColorList();
      List<Double> offsetList = circleGradientStyle.getOffsetList();
      int[] colors = new int[colorList.size()];
      float[] positions = new float[colorList.size()];
      for (int i = 0; i < colorList.size(); i++) {
        positions[i] = offsetList.get( i ).floatValue();
        colors[i] = ((AndroidColor) colorList.get( i )).getColorInt();
      }
      float gradientCenterX = (float) (center.getX() - getBounds().left);
      float gradientCenterY = (float) (center.getY() - getBounds().top);
      RadialGradient gradient = new RadialGradient( gradientCenterX, gradientCenterY, (float) circleGradientStyle.getRadius(), colors, positions, tileMode );
      getPaint().setShader( gradient );
    }
    else {
      ComponentColor fillColor = shapeStyle.getFillColor();
      if (fillColor != null) {
        // workaround: Android does not start drawing if fill color is transparent
        if (shapeStyle.isFillColorTransparent()) {
          getPaint().setColor( ((AndroidColor) ShapeStyle.COLOR_TEMP_TRANSPARENT).getColorInt() );
        }
        else {
          getPaint().setColor( ((AndroidColor) fillColor).getColorInt() );
        }
      }
    }

    // set border
    ComponentColor borderColor = shapeStyle.getBorderColor();
    double strokeWidth = shapeStyle.getStrokeWidth();
    if (borderColor != null && strokeWidth > 0) {
      borderPaint.setStrokeWidth( (float) strokeWidth );
      borderPaint.setColor( ((AndroidColor) borderColor).getColorInt() );
    }

    // set shadow
    ComponentColor shadowColor = shapeStyle.getShadowColor();
    double shadowRadius = shapeStyle.getShadowRadius();
    if (shadowColor != null && shadowRadius > 0) {
      int shadowColorInt = ((AndroidColor) shadowColor).getColorInt();
      getPaint().setShadowLayer( (float) shadowRadius, 0, 0, shadowColorInt );
      borderPaint.setShadowLayer( (float) shadowRadius, 0, 0, shadowColorInt );
    }
  }

  public static Shader.TileMode getCycleMethod( GradientMode gradientMode ) {
    switch (gradientMode) {
      case CLAMP: return Shader.TileMode.CLAMP;
      case MIRROR: return Shader.TileMode.MIRROR;
      case REPEAT: return Shader.TileMode.REPEAT;
    }
    return null;
  }

  /**
   * Called from the drawable's draw() method after the canvas has been set to
   * draw the shape at (0,0). Subclasses can override for special effects such
   * as multiple layers, stroking, etc.
   *
   * @param shape
   * @param canvas
   * @param paint
   */
  @Override
  protected void onDraw( Shape shape, Canvas canvas, Paint paint ) {
    // workaround: Android originally does not start drawing if fill color is not set
    if (!shapeStyle.isFillColorTransparent()) {
      super.onDraw( shape, canvas, paint );
    }
    if (shapeStyle.getBorderColor() != null && shapeStyle.getStrokeWidth() > 0) {
      shape.draw( canvas, borderPaint );
    }
  }

  @Override
  public ShapeStyle getShapeStyle() {
    return shapeStyle;
  }
}
