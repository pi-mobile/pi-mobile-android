/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.component;

import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.Shape;
import android.view.View;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.view.figure.ShapeStyle;

/**
 * Provides a drawable for a line.
 *
 * @author dsc
 */
public class LineShapeDrawable extends ShapeDrawable implements StyledDrawable {

  private ShapeStyle shapeStyle;

  /**
   * Creates a LineShapeDrawable with shape representing a line and a style to apply.
   *
   * @param shape
   * @param shapeStyle
   */
  public LineShapeDrawable( Shape shape, ShapeStyle shapeStyle ) {
    super( shape );
    init(shapeStyle);
  }

  private void init( ShapeStyle shapeStyle ) {
    this.shapeStyle = shapeStyle;

    getPaint().setStyle( Paint.Style.STROKE );
    getPaint().setAntiAlias( true );

    applyStyle();
  }

  /**
   * Needed for shadow support. The shadow is rendered on the main paint.
   *
   * @param androidPaintView
   */
  @Override
  public void enableShadow( AndroidPaintView androidPaintView ) {
    int layerType = androidPaintView.getLayerType();
    if (layerType != View.LAYER_TYPE_SOFTWARE) {
      androidPaintView.setLayerType( View.LAYER_TYPE_SOFTWARE, getPaint() );
    }
  }


  @Override
  public void shapeStyleChanged() {
    applyStyle();
  }

  private void applyStyle() {
    ComponentColor lineColor = shapeStyle.getBorderColor();
    double strokeWidth = shapeStyle.getStrokeWidth();
    if (lineColor != null) {
      getPaint().setColor( ((AndroidColor) lineColor).getColorInt() );
      getPaint().setStrokeWidth( (float) strokeWidth );
    }
  }

  @Override
  public ShapeStyle getShapeStyle() {
    return shapeStyle;
  }

}
