package de.pidata.gui.android.component;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class AndroidTabPane extends LinearLayout {

  public AndroidTabPane( Context context ) {
    super( context );
  }

  public AndroidTabPane( Context context, AttributeSet attrs ) {
    super( context, attrs );
  }

  public AndroidTabPane( Context context, AttributeSet attrs, int defStyleAttr ) {
    super( context, attrs, defStyleAttr );
  }

  public AndroidTabPane( Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes ) {
    super( context, attrs, defStyleAttr, defStyleRes );
  }

}
