/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.ProgressController;
import de.pidata.log.Logger;

/**
 * Manages an dialog which shows the progress of the currently running task
 *
 * @author Doris Schwarzmaier
 */
public class AndroidProgressController implements ProgressController  {

  private ProgressDialog progressDialog;
  private Activity currentActivity;
  private TaskHandler progressTaskHandler;
  private Runnable cancelAction;

  /**
   * Builds and manages an Android ProgressDialog to show the progress of the currently running Activity.
   *
   * @param parentDialog
   * @param message
   * @param minValue ignored by Android
   * @param maxValue
   * @param cancelable
   */
  public AndroidProgressController( DialogController parentDialog, String message, int minValue, int maxValue, final boolean cancelable ) {
    currentActivity = ((AndroidDialog) parentDialog.getDialogComp()).getActivity();

    progressDialog = new ProgressDialog( currentActivity );
    if (maxValue == minValue) {
      progressDialog.setProgressStyle( ProgressDialog.STYLE_SPINNER );
      progressDialog.setIndeterminate( true );
    }
    else {
      progressDialog.setProgressStyle( ProgressDialog.STYLE_HORIZONTAL );
    }
    progressDialog.setCancelable( cancelable );

    if(cancelable) {
      progressDialog.setButton( DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
        @Override
        public void onClick( DialogInterface dialog, int which ) {
          if(cancelAction!=null){
            cancelAction.run();
          }
          progressTaskHandler.abortTask();
        }
      } );
    }
    progressDialog.setMax( maxValue );
    progressDialog.setMessage( message );

  }

  public ProgressDialog getProgressDialog() {
    return progressDialog;
  }

  @Override
  public void show() {
    progressDialog.show();
  }

  @Override
  public void finish() {
    if (progressDialog != null) {
      if (progressDialog.isShowing()) {
        progressDialog.dismiss();
      }
    }
  }

  @Override
  public void setCancelAction( Runnable cancelAction ) {
    this.cancelAction = cancelAction;
  }

  @Override
  public void startAndObserveTask( ProgressTask progressTask ) {
    if (this.progressTaskHandler != null) {
      // another task is already running
      Logger.warn( "another background task is already running [" + progressTaskHandler.getTaskName() + "]" );
      return;
    }
    progressTaskHandler = progressTask.getTaskHandler();
    progressTaskHandler.startTask( this );
  }

  public void setProgress( final double progress, final String message ) {
    if (progressDialog != null) {
      Platform.getInstance().runOnUiThread( new Runnable() {
        @Override
        public void run() {
          progressDialog.setProgress( (int) Math.floor( progress ) );
          progressDialog.setMessage( message );
        }
      } );
    }
  }

  @Override
  public void setProgress( double progress ) {
    setProgress( progress, "" );
  }

  /**
   * Called to set an optional max value. this allows prograss messages
   * like 8 of 25
   *
   * @param maxValue the max value, following calls to setProgress mus not
   *                 exceed this value
   */
  @Override
  public void setMaxValue( double maxValue ) {
    progressDialog.setMax( (int) maxValue );
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param newMessage the message to display or null
   * @param progress   progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( String newMessage, double progress ) {
    setProgress( progress, newMessage );
  }

  /**
   * Updates progress indicator with message and per cent value.
   *
   * @param progress progress in per cent or -1 for aborted with error
   */
  @Override
  public void updateProgress( double progress ) {
    setProgress( progress );
  }

  /**
   * Closes the progress dialog attached to this controller
   */
  public void abortTask() {
    if (progressTaskHandler != null) {
      progressTaskHandler.abortTask();
    }
  }

  @Override
  public void showError( String errorMessage ) {
    updateProgress( errorMessage, progressDialog.getMax() );
  }

  @Override
  public void hideError() {
    updateProgress( "", progressDialog.getProgress() );
  }

  @Override
  public void showInfo( String infoMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void showWarning( String warningMessage ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetColor() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
