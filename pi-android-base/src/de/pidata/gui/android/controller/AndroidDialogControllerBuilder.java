/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.controller;

import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.component.base.ProgressTask;
import de.pidata.gui.component.base.TaskHandler;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.guidef.*;
import de.pidata.gui.platform.android.AndroidProgressTask;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;

public class AndroidDialogControllerBuilder extends ControllerBuilder {

  public AndroidDialogControllerBuilder() {
  }

  @Override
  public synchronized void init( Application app, GuiBuilder guiBuilder ) {
    Logger.info( "Init AndroidDialogControllerBuilder app="+app );
    super.init( app, guiBuilder );
  }

  /**
   * Creates a dialog containing a message and a progress bar. Content of message and progress bar
   * can be changed via ProgressController.
   *
   * @param title      the dialog title
   * @param message    the messsage, use CR (\n) for line breaks
   * @param fromValue  the value representing 0%
   * @param toValue    the value representing 100%
   * @param cancelable true if progress dialog can be canceled by user
   * @return ProgressController for this dialog
   */
  public ProgressController createProgressDialog( String title, String message, int fromValue, int toValue,
                                                  boolean cancelable, DialogController parentDialogCtrl ) {
    ProgressController progressController = new AndroidProgressController( parentDialogCtrl, message, fromValue, toValue, cancelable );
    return progressController;
  }

  public ControllerGroup addTab( DialogController dialogCtrl, QName tabName ) {
    // TODO: used only in unused old PIMobileTab???
    throw new RuntimeException( "TODO: unused???" );
//    if (this.app == null) {
//      throw new IllegalArgumentException("Application not initialized - use loadGui()");
//    }
//    TabControllerGroup tabController = (TabControllerGroup) dialogCtrl.getController( tabName );
//    try {
//      ControllerType ctrlDef = tabController.getCtrlDef();
//      boolean readOnly =  ((ctrlDef.getReadOnly() != null) && (ctrlDef.getReadOnly().booleanValue() == true));
//      dialogCtrl.getControllerBuilder().addChildControllers( tabController, ctrlDef, readOnly );
//      tabController.activate( dialogCtrl.getDialogComp(), null );
//    }
//    catch (Exception ex) {
//      Logger.error( "Could not create Tab's children", ex );
//    }
//    return tabController;
  }

  @Override
  public TaskHandler createTaskHandler( ProgressTask progressTask ) {
    return new AndroidProgressTask( progressTask );
  }
}
