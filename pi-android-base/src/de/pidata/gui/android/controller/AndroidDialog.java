/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.controller;

import android.app.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.activity.DefaultActivity;
import de.pidata.gui.android.activity.PiFragment;
import de.pidata.gui.android.activity.PiMobileActivity;
import de.pidata.gui.android.activity.ReadTagActivity;
import de.pidata.gui.android.adapter.AndroidUIContainer;
import de.pidata.gui.android.AndroidScreen;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.AbstractParameterList;
import de.pidata.service.base.ParameterList;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.util.Vector;

public class AndroidDialog implements Dialog, AndroidUIContainer {

  public static final String SCHEME_PIMOBILE = "pi-mobile";

  private Activity activity;
  private Vector contentViewList = new Vector();
  private AndroidDialog parent;
  private DialogController dialogController;
  private EditText input;

  public AndroidDialog() {
  }

  public void init( Activity activity, View contentView ) {
    this.activity = activity;
    addContentView( contentView );
    this.parent = (AndroidDialog) Platform.getInstance().getScreen().getFocusDialog();
    ((AndroidScreen) Platform.getInstance().getScreen()).setFocusDialog( this );
  }

  public Activity getActivity() {
    return activity;
  }

  @Override
  public FragmentManager getChildFragmentManager() {
    return getActivity().getFragmentManager();
  }

  public View getContentView( int index ) {
    return (View) contentViewList.elementAt( index );
  }

  public int contentViewCount() {
    return contentViewList.size();
  }

  public void addContentView( View contentView ) {
    this.contentViewList.addElement( contentView );
  }

  public AndroidDialog getParent() {
    return parent;
  }

  public View findAndroidView( QName viewID ) {
    int id = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, viewID );
    for (int i = 0; i < contentViewList.size(); i++) {
      View contentView = (View) contentViewList.elementAt( i );
      View androidView;
      try {
        androidView = contentView.findViewById( id );
      }
      catch (Exception ex) {
        androidView = null;
      }
      if (androidView != null) {
        return androidView;
      }
    }
    return null;
  }

  @Override
  public MenuItem findMenuItem( QName viewID ) {
    int id = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, viewID );
    if (activity instanceof PiMobileActivity) {
      Menu optionsMenu = ((PiMobileActivity) activity).getOptionsMenu();
      if (optionsMenu != null) {
        MenuItem item = getMenuItem( optionsMenu, id );
        if (item != null) {
          return item;
        }
      }
    }
    return null;
  }

  private MenuItem getMenuItem( Menu optionsMenu, int id ) {
    for (int i = 0; i < optionsMenu.size(); i++) {
      MenuItem item = optionsMenu.getItem( i );
      if (item.getItemId() == id) {
        return item;
      }
      MenuItem subItem = findSubMenuItem( item, id );
      if (subItem != null) {
        return subItem;
      }
    }
    return null;
  }

  private MenuItem findSubMenuItem( MenuItem item, int viewID ) {
    if (item.hasSubMenu()) {
      Menu subMenu = item.getSubMenu();
      return getMenuItem( subMenu, viewID );
    }
    else {
      return null;
    }
  }

  /**
   * Called by DialogController after title has been changed.
   *
   * @param title the new dialog title
   */
  @Override
  public void onTitleChanged( final String title ) {
    activity.runOnUiThread( new Runnable() {
      @Override
      public void run() {
        activity.setTitle( title );
      }
    } );
  }

  @Override
  public String getTitle() {
    return activity.getTitle().toString();
  }

  public void show( Dialog parent, boolean fullScreen ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public void updateLanguage() {
    Platform.getInstance().updateLanguage( this, getID(), null );
    ((AbstractDialogController)dialogController).updateChildrenLanguage();
  }

  /**
   * Called by DialogController.close() before anything other is
   * done. By returning false this Dialog can interrupt closing if
   * the Platform supports to abort close requests (e.g. Android does
   * not support that). Java FX will try to close children and
   * abort closing if any child dialog need user feedback.
   *
   * @param ok true if closing is OK operation
   * @return false to abort closing
   */
  @Override
  public boolean closing( boolean ok ) {
    return true; // Android does not allow to interrupt closing
  }

  public void close( boolean ok, ParameterList resultList ) {
    ((AndroidScreen) Platform.getInstance().getScreen()).setFocusDialog( this.getParent() );
    Activity currentActivity = getActivity();
    Intent intent = new Intent();
    if (resultList != null) {
      PiMobileActivity.paramListToIntent( resultList, intent );
    }
    if (ok) {
      currentActivity.setResult( Activity.RESULT_OK, intent );
    }
    else {
      currentActivity.setResult( Activity.RESULT_CANCELED, intent );
    }
    currentActivity.finish();
  }

  public void speak( String text ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  public QName getID() {
    return dialogController.getName();
  }

  /**
   * Sets a component dialogController for this component. SelectionKey may be used as identifier
   * if dialogController wants to listen on multiple components. Listener is called whenever
   * a input event occurs on this component.
   *
   * @param dialogController the component dialogController
   */
  public void setController( DialogController dialogController ) {
    this.dialogController = dialogController;
  }

  /**
   * Returns the ComponentListener
   *
   * @return the ComponentListener
   */
  public DialogController getController() {
    return this.dialogController;
  }

  /**
   * Returns the UIFactory to use when creating UIAdapter to this dialog
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return getController().getModel();
  }

  public String getActivityClassName( DialogType dialogType ) {
    String className = dialogType.getDialogClass();
    if ((className == null) || (className.length() == 0)) {
      className = DefaultActivity.class.getName();
    }
    return className;
  }

  /**
   * Set UI Style
   *
   * @param style
   */
  @Override
  public void setStyle( String style ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogType    user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   */
  @Override
  public void openChildDialog( DialogType dialogType, String title, ParameterList parameterList ) {
    Activity currentActivity = getActivity();
    Intent intent = new Intent();
    String activityClassName = getActivityClassName( dialogType );
    QName dialogID = dialogType.getID();
    intent.setClassName( currentActivity, activityClassName );
    intent.setAction( dialogID.getName() ); // this allows to start different dialogs using the same Activity

    if (parameterList != null) {
      PiMobileActivity.paramListToIntent( parameterList, intent );
    }

    // Important: If requestCode < 0 then onActivityResult() is not called after closing child Activity
    currentActivity.startActivityForResult( intent, 0 );
  }

  public String getActivityClassName( DialogDef dialogDef ) {
    String className = dialogDef.getDialogClass();
    if ((className == null) || (className.length() == 0)) {
      className = DefaultActivity.class.getName();
    }
    return className;
  }

  public void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList ) {
    Activity currentActivity = getActivity();
    Intent intent = new Intent();
    String activityClassName = getActivityClassName( dialogDef );
    QName dialogID = dialogDef.getID();
    intent.setClassName( currentActivity, activityClassName );
    intent.setAction( dialogID.getName() ); // this allows to start different dialogs using the same Activity

    if (parameterList != null) {
      PiMobileActivity.paramListToIntent( parameterList, intent );
    }
    // Important: If requestCode < 0 then onActivityResult() is not called after closing child Activity
    currentActivity.startActivityForResult( intent, 0 );
  }

  /**
   * Opens popup with given moduleDef and title
   *
   * @param popupCtrl
   * @param moduleGroup module group to be shown in a Popup
   * @param title       title for the dialog, if null title from dialogDef is used
   */
  @Override
  public void showPopup( PopupController popupCtrl, ModuleGroup moduleGroup, String title ) {
    try {
      ModuleViewPI moduleViewPI = (ModuleViewPI) popupCtrl.getView();

      PiFragment androidPopup = new PiFragment();
      androidPopup.init( moduleGroup, moduleViewPI, getController().getModel() );
      moduleGroup.activate( moduleViewPI.getUIContainer() );
      androidPopup.show( getActivity().getFragmentManager(), title );
      // Note: moduleViewPI is attached to its UIFragmentAdapter by PiFragement.onActivityCreated()
    }
    catch (Exception ex) {
      Logger.error( "Error showing popup", ex );
    }
  }

  /**
   * Hides current popup. If popup is not visible nothing happens.
   */
  @Override
  public void closePopup( PopupController popupController ) {
    popupController.setModuleGroup( null, ViewAnimation.NONE );
    popupController.deactivate( true );
  }

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   */
  public void showMessage( final String title, final String message ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        builder.setTitle( title );
        builder.setMessage( message );
        builder.setPositiveButton( "OK", new DialogInterface.OnClickListener() {
          @Override
          public void onClick( DialogInterface dialogInterface, int buttonID ) {
            AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
          }
        } );
        AlertDialog dialog = builder.create();
        dialog.show();
      }
    } );
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  public void showQuestion( final String title, final String message, final String btnYesLabel, final String btnNoLabel, final String btnCancelLabel ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        builder.setTitle( title );
        builder.setMessage( message );
        builder.setPositiveButton( btnYesLabel, new DialogInterface.OnClickListener() {
              @Override
              public void onClick( DialogInterface dialogInterface, int buttonID ) {
                AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
              }
            }
        );
        builder.setNegativeButton( btnNoLabel, new DialogInterface.OnClickListener() {
              @Override
              public void onClick( DialogInterface dialogInterface, int buttonID ) {
                AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
              }
            }
        );
        if (btnCancelLabel != null) {
          builder.setCancelable( true );
          builder.setNeutralButton( btnCancelLabel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick( DialogInterface dialogInterface, int buttonID ) {
                  AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
                }
              }
          );
        }
        AlertDialog dialog = builder.create();
        dialog.show();
      }
    } );
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   */
  @Override
  public void showInput( final String title, final String message, final String defaultValue, final String btnOKLabel, final String btnCancelLabel ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );
        builder.setTitle( title );

        input = new EditText( getActivity() );
        input.setInputType( InputType.TYPE_CLASS_TEXT );
        input.setText( defaultValue );
        builder.setView( input );

        builder.setPositiveButton( btnOKLabel, new DialogInterface.OnClickListener() {
              @Override
              public void onClick( DialogInterface dialogInterface, int buttonID ) {
                AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
              }
            }
        );
        if (btnCancelLabel != null) {
          builder.setCancelable( true );
          builder.setNeutralButton( btnCancelLabel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick( DialogInterface dialogInterface, int buttonID ) {
                  AndroidDialog.this.doOnClick( dialogInterface, buttonID, title );
                }
              }
          );
        }
        AlertDialog dialog = builder.create();
        dialog.show();
      }
    } );
  }

  public void showDatePicker( DatePickerDialog.OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth ) {
    DatePickerDialog dialog = new DatePickerDialog( getActivity(), callBack, year, monthOfYear, dayOfMonth );
    dialog.show();
  }

  public void showTimePicker( TimePickerDialog.OnTimeSetListener callBack, int hourOfDay, int minute ) {
    TimePickerDialog dialog = new TimePickerDialog( getActivity(), callBack, hourOfDay, minute, true );
    dialog.show();
  }

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   */
  @Override
  public void showFileChooser( String title, FileChooserParameter fileChooserParams ) {
    SimpleFileDialog fileOpenDialog = new SimpleFileDialog( this, "FileOpen" );
    fileOpenDialog.show( fileChooserParams );
  }

  public void showNfcTool() {
    Activity currentActivity = getActivity();
    Intent intent = new Intent();
    intent.setClassName( currentActivity, ReadTagActivity.class.getName() );

    currentActivity.startActivityForResult( intent, 0 );
  }

  /**
   * Shows the platform specific image chooser dialog.
   * <ul>
   *   <li>assure client has rights to write on destination</li>
   *   <li>use {@link SystemManager#getStorage(String)} and {@link Storage#getPath(String)} to get absolute Path on different Platforms</li>
   * </ul>
   *
   * @param imageChooserType type of the image chooser
   * @param filePath         <b>absolute</b> path of file that has to be used, if image will be created (by using camera)
   * @return dialog Handle
   */
  @Override
  public void showImageChooser( ImageChooserType imageChooserType, String filePath ) {
    Intent intent = null;
    switch (imageChooserType) {
      case CAMERA_ONLY: {
        intent = getCameraIntent( filePath );
        break;
      }
      case GALLERY_ONLY: {
        intent = getGalleryIntent();
        break;
      }
      case CAMERA_AND_GALLERY: {
        intent = Intent.createChooser( getGalleryIntent(), "Bild auswählen" );
        intent.putExtra( Intent.EXTRA_INITIAL_INTENTS, new Intent[]{getCameraIntent( filePath )} );
        break;
      }
    }
    getActivity().startActivityForResult( intent, 0 );
  }

  private Intent getCameraIntent( String fotoFilePath ) {
    File fotoFile = new File( fotoFilePath );
    fotoFile.getParentFile().mkdirs();
    //Use FileProvider to work on Android Api >24
    Uri photoUri = FileProvider.getUriForFile( this.getActivity(), "de.pidata.fileProvider", fotoFile );
    Intent cameraIntent = new Intent( android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
    cameraIntent.putExtra( MediaStore.EXTRA_OUTPUT, photoUri ); // set the image file name
    return cameraIntent;
  }

  private Intent getGalleryIntent() {
    return new Intent( Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI );
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( final String message ) {
    getActivity().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        Toast.makeText( getActivity(), message, Toast.LENGTH_LONG ).show();
      }
    } );
  }

  @Override
  public void toFront() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void toBack() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public void doOnClick( DialogInterface dialogInterface, int buttonID, String title ) {
    QName selBtn;
    if (buttonID == DialogInterface.BUTTON_POSITIVE) {
      selBtn = QuestionBoxResult.YES_OK;
    }
    else if (buttonID == DialogInterface.BUTTON_NEGATIVE) {
      selBtn = QuestionBoxResult.NO;
    }
    else {
      selBtn = QuestionBoxResult.CANCEL;
    }
    String inputValue = null;
    if (input != null) {
      inputValue = input.getText().toString();
    }
    QuestionBoxResult result = new QuestionBoxResult( selBtn, inputValue, title );
    getController().childDialogClosed( (buttonID == DialogInterface.BUTTON_POSITIVE), result );
  }

  public void childClosed( int resultCode, Intent resultIntent ) {
    ParameterList resultList;
    if (resultIntent != null) {
      try {
        resultList = PiMobileActivity.intentToParamList( resultIntent );
      }
      catch (Exception ex) {
        String msg = "Error extracting result ParameterList from Intent";
        Logger.error( msg, ex );
        throw new IllegalArgumentException( msg );
      }
    }
    else {
      resultList = AbstractParameterList.EMPTY;
    }
    getController().childDialogClosed( (resultCode == Activity.RESULT_OK), resultList );
  }

  public void onResume() {
    DialogControllerDelegate delegate = getController().getDelegate();
    if (delegate != null) {
      delegate.dialogShowing( getController() );
    }
  }

  public void showBusy( boolean show ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    // throw new RuntimeException( "TODO" );
  }

  @Override
  public void setAlwaysOnTop( boolean onTop ) {
    // do nothing
  }

  @Override
  public boolean getAlwaysOnTop() {
    return false;
  }

  @Override
  public void setMinWidth( int width ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMaxWidth( int width ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinHeight( int height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMaxHeight( int height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void registerShortcut( QName command, ActionController controller ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Require rights at a specific platform.
   */
  @Override
  public void requireRights() {
    ((PiMobileActivity) activity).requireRights();
  }

  /**
   * Add a RightsRequireListener for callback.
   *
   * @param rightsRequireListener
   */
  @Override
  public void addRightsRequireListener( RightsRequireListener rightsRequireListener ) {
    ((PiMobileActivity) activity).addRightsRequireListener( rightsRequireListener );
  }
}