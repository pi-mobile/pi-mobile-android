/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class AndroidSearchViewAdapter extends AndroidUIAdapter implements UIButtonAdapter, SearchView.OnQueryTextListener {

  private SearchView searchView;
  private ButtonViewPI buttonViewPI;
  private String queryText = "";

  public AndroidSearchViewAdapter( ButtonViewPI buttonView, final SearchView searchView, final UIContainer uiContainer ) {
    super( searchView, uiContainer );
    this.buttonViewPI = buttonView;
    this.searchView = searchView;

    this.searchView.setOnQueryTextListener( this );
    this.searchView.setSubmitButtonEnabled( true );

    int searchCloseButtonId = searchView.getContext().getResources().getIdentifier( "android:id/search_close_btn", null, null);
    ImageView _closeBtn = (ImageView) searchView.findViewById( searchCloseButtonId );
    _closeBtn.setOnClickListener( new View.OnClickListener() {
      @Override
      public void onClick( View view ) {
        if(searchView.getQuery().length() == 0) {
          searchView.setIconified(true);
        } else {
          queryText = "";

          // Do your task here
          searchView.setQuery(queryText, false);
          Logger.info( "AndroidSearchView.onClose()" );
          buttonViewPI.onClick( AndroidSearchViewAdapter.this, uiContainer.getDataContext() );
        }
      }
    } );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (searchView != null) {
      searchView.setOnQueryTextListener( null );
      searchView = null;
    }
    buttonViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return buttonViewPI;
  }

  @Override
  protected View getAndroidView() {
    return searchView;
  }

  /**
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( final String label ) {
    // Nothing to set here
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return this.queryText;
  }


  @Override
  public void setVisible( final boolean visible ) {
    final SearchView searchView = this.searchView;
    if (searchView != null) {
      searchView.post( new Runnable() {
        @Override
        public void run() {
          if (visible) {
            searchView.setVisibility( View.VISIBLE );
          }
          else {
            searchView.setVisibility( View.GONE );
          }
        }
      } );
    }
  }

  @Override
  public void setSelected( final boolean selected) {
    searchView.post( new Runnable() {
      @Override
      public void run() {
        searchView.setPressed( selected );
      }
    } );
  }

  @Override
  public void setEnabled( final boolean enable) {
    final SearchView searchView  = this.searchView;
    if (searchView != null) {
      searchView.post( new Runnable() {
        @Override
        public void run() {
          searchView.setEnabled( enable );
        }
      } );
    }
  }

  /**
   * Called when the user submits the query. This could be due to a key press on the
   * keyboard or due to pressing a submit button.
   * The listener can override the standard behavior by returning true
   * to indicate that it has handled the submit request. Otherwise return false to
   * let the SearchView handle the submission by launching any associated intent.
   *
   * @param query the query text that is to be submitted
   * @return true if the query has been handled by the listener, false to let the
   * SearchView perform the default action.
   */
  @Override
  public boolean onQueryTextSubmit( String query ) {
    this.queryText = query;
    Logger.info( "AndroidSearchView.onQueryTextSubmit("+ query +")");
    buttonViewPI.onClick( this, uiContainer.getDataContext() );
    searchView.clearFocus();
    return true;
  }

  /**
   * Called when the query text is changed by the user.
   *
   * @param newText the new content of the query text field.
   * @return false if the SearchView should perform the default action of showing any
   * suggestions if available, true if the action was handled by the listener.
   */
  @Override
  public boolean onQueryTextChange( String newText ) {
    this.queryText = newText;
    return true;
  }

}
