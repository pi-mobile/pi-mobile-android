/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.List;

/**
 * Implements list and table
 *
 * NOTE: onGestureListener is necessary for getting swipe events correctly
 * previously a onclickListener has been used instead,
 * but this did not return events to other listeners so swiping on a listview was not possible
 */
public class AndroidListAdapter extends BaseAdapter implements UIListAdapter, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, GestureDetector.OnGestureListener, View.OnClickListener, View.OnTouchListener {

  protected static final boolean DEBUG_STATES = false;
  protected static String ME = AndroidListAdapter.class.getSimpleName();

  protected static final int VIEWTYPE_RENDERROW = 0;
  protected static final int VIEWTYPE_EDITROW = 1;
  private GestureDetector gestureDetector;

  protected AdapterView androidListView; // be careful: Modifications are only allowed on UI thread!
  protected View selectedRowView = null;
  protected ListViewPI listViewPI;
  protected UIContainer uiContainer;
  protected AdapterView.OnItemClickListener oldOnItemClickListener;
  protected boolean firstEvent = true;

  protected int textHeightList = 16;
  protected int textHeightCombo = 24;
  protected int selectedRowColor;
  protected int fontColor;
  private Integer initialBackgroundColor;

  private AndroidCellAdapter[] cellAdapter;
  private final List<Model> displayRows = new ArrayList<Model>();
  private final List<Model> adapterRows = new ArrayList<Model>();
  private GestureDetector.OnGestureListener gestureListener;
  private SwipeListenerType listenerType;

  public AndroidListAdapter( ListViewPI listViewPI, final ListView androidListView, UIContainer uiContainer, GestureDetector.OnGestureListener gestureListener, SwipeListenerType swipeListenerType ) {
    ME = listViewPI.getComponentID().getName();
    this.listViewPI = listViewPI;
    this.uiContainer = uiContainer;
    this.androidListView = androidListView;
    Drawable background = androidListView.getBackground();
    if (background instanceof ColorDrawable) {
      this.initialBackgroundColor = ((ColorDrawable) background).getColor();
    }
    try {
      int id = UIFactoryAndroid.getRessourceID( "color", ControllerBuilder.NAMESPACE.getQName( "spinner_selection" ) );
      this.selectedRowColor = androidListView.getResources().getColor( id );
    }
    catch (Exception ex) {
      this.selectedRowColor = Color.DKGRAY;
    }
    gestureDetector = new GestureDetector( null, this );
    this.gestureListener = gestureListener;
    this.listenerType = swipeListenerType;
    androidListView.post( new Runnable() {
      public void run() {
        androidListView.setChoiceMode( AbsListView.CHOICE_MODE_SINGLE );
        androidListView.setAdapter( AndroidListAdapter.this );
        AndroidListAdapter.this.oldOnItemClickListener = androidListView.getOnItemClickListener();
        androidListView.setOnItemClickListener( AndroidListAdapter.this );

        // Only listen to TouchEvents if there is a given gestureListener which is a special requirement by some fragments.
        if (AndroidListAdapter.this.gestureListener != null) {
          androidListView.setOnTouchListener( AndroidListAdapter.this );
        }

        attachColumns();
      }
    } );
    try {
      int id = UIFactoryAndroid.getRessourceID( "color", ControllerBuilder.NAMESPACE.getQName( "spinner_text" ) );
      fontColor = androidListView.getResources().getColor( id );
    }
    catch (Exception ex) {
      fontColor = Color.WHITE;
    }
  }


  public AndroidListAdapter( ListViewPI listViewPI, final Spinner androidSpinner ) {
    this.listViewPI = listViewPI;
    this.androidListView = androidSpinner;
    Drawable background = androidListView.getBackground();
    if(background instanceof ColorDrawable) {
      this.initialBackgroundColor = ((ColorDrawable) background).getColor();
    }
    selectedRowColor = Color.TRANSPARENT;
    androidListView.post( new Runnable() {
      public void run() {
        androidSpinner.setAdapter( AndroidListAdapter.this );
        androidSpinner.setOnItemSelectedListener( AndroidListAdapter.this );
        attachColumns();
      }
    } );
    try {
      int id = UIFactoryAndroid.getRessourceID( "color", ControllerBuilder.NAMESPACE.getQName( "spinner_text" ) );
      fontColor = androidListView.getResources().getColor( id );
    }
    catch (Exception ex) {
      fontColor = Color.WHITE;
    }
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if (androidListView != null) {
      // Note: Do not remove ListAdapter to avoid system crash, e.g. by androidListView.setAdapter( null );
      if (!(androidListView instanceof Spinner)) androidListView.setOnItemClickListener( oldOnItemClickListener );
      androidListView.setOnItemSelectedListener( null );
      androidListView.setOnTouchListener( null );
      androidListView = null;
    }
    this.listViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return listViewPI;
  }

  @Override
  public UIContainer getUIContainer() {
    return uiContainer;
  }

  private void attachColumns() {
    SelectionController selectionController = getListViewPI().getListCtrl();
    if (selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      if (cellAdapter == null) {
        cellAdapter = new AndroidCellAdapter[tableController.columnCount()];
      }
      for (short i = 0; i < cellAdapter.length; i++) {
        ColumnInfo columnInfo = tableController.getColumn( i );
        QName colCompName = columnInfo.getRenderCtrl().getView().getComponentID();
        int ressourceID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, colCompName );
        cellAdapter[i] = new AndroidCellAdapter( columnInfo, ressourceID );
      }
    }
  }

  public AndroidCellAdapter findCellAdapter( int ressourceID ) {
    if (cellAdapter != null) {
      for (short i = 0; i < cellAdapter.length; i++) {
        if (cellAdapter[i].getRessourceID() == ressourceID) {
          return cellAdapter[i];
        }
      }
    }
    return null;
  }

  public void setTextHeightList( int textHeightList ) {
    this.textHeightList = textHeightList;
  }

  public void setTextHeightCombo( int textHeightCombo ) {
    this.textHeightCombo = textHeightCombo;
  }

  public void setSelectedRowColor( int selectedRowColor ) {
    this.selectedRowColor = selectedRowColor;
  }

  public ListViewPI getListViewPI() {
    return listViewPI;
  }

  public AdapterView getAndroidListView() {
    return androidListView;
  }

  @Override
  public void setVisible( boolean visible ) {
    androidListView.post( new Runnable() {
      @Override
      public void run() {
        if (visible) {
          androidListView.setVisibility( View.VISIBLE );
        }
        else {
          androidListView.setVisibility( View.INVISIBLE );
        }
      }
    });
  }

  @Override
  public boolean isVisible() {
    if (androidListView == null) {
      return false;
    }
    else {
      return (androidListView.getVisibility() == View.VISIBLE);
    }
  }

  @Override
  public boolean isFocused() {
    if (androidListView == null) {
      return false;
    }
    else {
      return androidListView.isFocused();
    }
  }

  @Override
  public void setEnabled( boolean enabled ) {
    if (androidListView != null) {
      androidListView.setEnabled( enabled );
    }
  }

  @Override
  public boolean isEnabled() {
    if (androidListView == null) {
      return false;
    }
    else {
      return androidListView.isEnabled();
    }
  }

  /**
   * Set this UIAdapter's background color
   *
   * @param color new background color
   */
  @Override
  public void setBackground( ComponentColor color ) {
    if (androidListView != null) {
      androidListView.post( new UIUpdater( androidListView, UpdateJob.SET_BACKGROUND, color.getColor() ) );
    }
  }

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  @Override
  public void resetBackground() {
    if (androidListView != null) {
      if (this.initialBackgroundColor != null) {
        androidListView.post( new UIUpdater( androidListView, UpdateJob.SET_BACKGROUND, this.initialBackgroundColor ) );
      }
      else {
        setBackground( Platform.getInstance().getColor( ComponentColor.TRANSPARENT ) );
      }
    }
  }

  private void startUpdate() {
  }

  private void endUpdate() {
    if (androidListView != null) {
      for (int i = 0; i < displayRows.size(); i++) {
        if (i < adapterRows.size()) {
          adapterRows.set( i, displayRows.get( i ) );
        }
        else {
          adapterRows.add( displayRows.get( i ) );
        }
      }
      while (adapterRows.size() > displayRows.size()) {
        adapterRows.remove( adapterRows.size() - 1 );
      }
      fireDataSetInvalidated();
    }
  }

  @Override
  public void removeAllDisplayRows() {
    androidListView.post( new Runnable() {
      public  void run() {
        synchronized (displayRows) {
          startUpdate();
          displayRows.clear();
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void insertRow( final Model newRow, final Model beforeRow ) {
    androidListView.post( new Runnable() {
      public  void run() {
        synchronized (displayRows) {
          startUpdate();
          int index;
          if (beforeRow == null) {
            index = -1;
          }
          else {
            index = indexOf( beforeRow );
          }
          if (index < 0) {
            displayRows.add( newRow );
            index = displayRows.size() - 1;
          }
          else {
            displayRows.add( index, newRow );
          }
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void removeRow( final Model removedRow ) {
    androidListView.post( new Runnable() {
      public  void run() {
        synchronized (displayRows) {
          startUpdate();
          displayRows.remove( removedRow );
          endUpdate();
        }
      }
    } );
  }

  @Override
  public void updateRow( final Model changedRow ) {
    androidListView.post( new Runnable() {
      public  void run() {
        synchronized (displayRows) {
          startUpdate();
          int index = indexOf( changedRow );
          if (index > 0) {
            displayRows.set( index, changedRow );
          }
          endUpdate();
        }
      }
    } );
  }

  public int indexOf( Model row ) {
    // avoid using displayRows.indexOf(), because that will call row.equals
    for (int i = 0; i < displayRows.size(); i++) {
      if (displayRows.get( i ) == row) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Called by ListViewPI in order to select row at index
   *  @param displayRow    the row to be selected
   * @param selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    new ComponentSelectionEvent( this, displayRow, selected );
  }

  public int getSelectedIndex() {
    if (androidListView == null) {
      return -1;
    }
    else {
      if (androidListView instanceof ListView) {
        return ((ListView) androidListView).getCheckedItemPosition();
      }
      else {
        return androidListView.getSelectedItemPosition();
      }
    }
  }

  protected void fireDataSetInvalidated() {
    notifyDataSetChanged();
  }

  protected void fireDataSetChanged() {
    if (listViewPI != null) {
      notifyDataSetChanged();
    }
  }

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    if (displayRows == null) {
      return 0;
    }
    else {
      return displayRows.size();
    }
  }

  /**
   * Returns the data displayed in row at position
   *
   * @param position
   * @return
   */
  @Override
  public Model getDisplayRow( int position ) {
    synchronized (displayRows) {
      int maxY = getDisplayRowCount() - 1;
      if ((position > maxY) || (position < 0)) {
        return null; // this may happen while UI updating
      }
      else {
        return displayRows.get( position );
      }
    }
  }

//--------------------------------------------------------
  // Android Adapter
  //--------------------------------------------------------

  public int getCount() {
    return adapterRows.size();
  }

  public Object getItem( int position ) {
    if (listViewPI == null) {
      return null;
    }
    else {
      return adapterRows.get( position );
    }
  }

  public long getItemId( int position ) {
    return position;
  }

  public View getView( int position, View convertView, ViewGroup parent ) {
    return getView( position, convertView, parent, textHeightList );
  }

  private View getView( int position, View convertView, ViewGroup parent, int textHeightDIP ) {
    View view = convertView;
    try {
      Model row = (Model) getItem( position );
      if (convertView == null) {
        int viewType = getItemViewType( position );
        if (viewType == VIEWTYPE_RENDERROW) {
          view = createRowView( parent, textHeightDIP );
          view.setFocusable( false );
          view.setClickable( false );
        }
        else {
          if (selectedRowView == null) {
            selectedRowView = createRowView( parent, textHeightDIP );
          }
          view = selectedRowView;
        }
        view.setVisibility( View.VISIBLE );
      }
      else {
        view = convertView;
      }
      boolean selectable = true;
      if(this.listViewPI instanceof TableViewPI){
        selectable = ((TableViewPI)listViewPI).isSelectable();
      }
      //boolean selected = (position == androidListView.getSelectedItemPosition());
      if (selectable) {
        boolean selected = (position == getSelectedIndex());
        if (DEBUG_STATES) Logger.info( "getView pos="+position+", selected="+selected );
        if (selected) {
          ComponentColor selColor;
          ComponentColor fontColor;
          if (listViewPI == null) {
            selColor = null;
            fontColor = null;
          }
          else {
            selColor = listViewPI.getSelectedRowColor();
            fontColor = listViewPI.getSelectedRowFontColor();
          }
          if (selColor == null) {
            view.setBackgroundColor( this.selectedRowColor );
          }
          else {
            view.setBackgroundColor( ((Integer) selColor.getColor()).intValue() );
          }
          if (fontColor == null) {
            setFontColorRecursive( view, this.fontColor );
          }
          else {
            int color = ((Integer) fontColor.getColor()).intValue();
            setFontColorRecursive( view, color );
          }
        }
        else {
          view.setBackgroundColor( Color.TRANSPARENT );
          setFontColorRecursive( view, fontColor );
        }
      }
      ((UIFactoryAndroid) Platform.getInstance().getUiFactory()).updateView( view, row, this );
    }
    catch (Exception ex) {
      Logger.error( "Error creating row view, pos="+position, ex );
    }
    return view;
  }

  private void setFontColorRecursive( View view, int color ) {
    if (view instanceof TextView) {
      ((TextView) view).setTextColor( color );
    }
    else if (view instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) view;
      for (int i = 0; i < group.getChildCount(); i++) {
        setFontColorRecursive( group.getChildAt( i ), color );
      }
    }
  }

  protected View createRowView( ViewGroup parent, int textHeightDIP ) {
    View view;
    QName rowCompID;
    if (listViewPI == null) {
      rowCompID = null;
    }
    else {
      rowCompID = listViewPI.getRowCompID();
    }
    if (rowCompID == null) {
      TextView textView = new TextView( parent.getContext() );
      textView.setTextColor( fontColor );
      textView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, textHeightDIP );
      view = textView;
    }
    else {
      view = ((UIFactoryAndroid) Platform.getInstance().getUiFactory()).createView( rowCompID, parent, false );
      if (view instanceof ViewGroup) {
        ViewGroup group = (ViewGroup) view;
        for (int i = 0; i < group.getChildCount(); i++) {
          View childView = group.getChildAt( i );
          childView.setOnClickListener( AndroidListAdapter.this );

          // Only listen to TouchEvents if there is a given gestureListener which is a special requirement by some fragments.
          if (AndroidListAdapter.this.gestureListener != null) {
            childView.setOnTouchListener( AndroidListAdapter.this );
          }

        }
      }
    }
    return view;
  }

  //--------------------------------------------------------
  // Android SpinnerAdapter
  //--------------------------------------------------------

  public View getDropDownView( int position, View convertView, ViewGroup parent ) {
    return getView( position, convertView, parent, textHeightCombo );
  }

  //--------------------------------------------------------
  // Android Listeners
  //--------------------------------------------------------

  /**
   * Called when a touch event is dispatched to a view. This allows listeners to
   * get a chance to respond before the target view.
   *
   * @param v     The view the touch event has been dispatched to.
   * @param event The MotionEvent object containing full information about
   *              the event.
   * @return True if the listener has consumed the event, false otherwise.
   */
  @Override
  public boolean onTouch( View v, MotionEvent event ) {
    if (DEBUG_STATES) {
      Logger.info( "in " + ME + " onTouch." );
    }
    if (gestureDetector != null) {
      return gestureDetector.onTouchEvent( event );
    }

    return false;
  }

  /**
   * Callback method to be invoked when an item in this AdapterView has
   * been clicked.
   * <p/>
   * Implementers can call getItemAtPosition(position) if they need
   * to access the data associated with the selected item.
   *
   * @param parent   The AdapterView where the click happened.
   * @param view     The view within the AdapterView that was clicked (this
   *                 will be a view provided by the adapter)
   * @param position The position of the view in the adapter.
   * @param id       The row id of the item that was clicked.
   */
  @Override
  public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
    if (DEBUG_STATES) Logger.info( "in " + ME + " onItemClick. pos is " + position );
    if (listViewPI != null) {
      Model selectedRow = getDisplayRow( position );
      setSelected( selectedRow, true );
      listViewPI.onSelectionChanged( selectedRow, true );
      if (oldOnItemClickListener != null) {
        oldOnItemClickListener.onItemClick( parent, view, position, id );
      }
    }
  }

  /**
   * <p>Callback method to be invoked when an item in this view has been
   * selected. This callback is invoked only when the newly selected
   * position is different from the previously selected position or if
   * there was no selected item.</p>
   * <p/>
   * Impelmenters can call getItemAtPosition(position) if they need to access the
   * data associated with the selected item.
   *
   * @param parent   The AdapterView where the selection happened
   * @param view     The view within the AdapterView that was clicked
   * @param position The position of the view in the adapter
   * @param id       The row id of the item that is selected
   */
  @Override
  public void onItemSelected( AdapterView<?> parent, View view, int position, long id ) {
    if (DEBUG_STATES) Logger.info( "in " + ME + " onItemSelected. pos is " + position );
    if (firstEvent) {
      // Spinner always fires an event when initializing which is not a user click - we must ignore that event
      firstEvent = false;
    }
    else if (listViewPI != null) {
      Model selectedRow = getDisplayRow( position );
      this.listViewPI.onSelectionChanged( selectedRow, true );
    }
  }

  /**
   * Callback method to be invoked when the selection disappears from this
   * view. The selection can disappear for instance when touch is activated
   * or when the adapter becomes empty.
   *
   * @param parent The AdapterView that now contains no selected item.
   */
  @Override
  public void onNothingSelected( AdapterView<?> parent ) {
    if (DEBUG_STATES) Logger.info( "in " + ME + " onNothingSelected." );
    // do nothing
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    //TODO not implemented To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void performOnClick() {
    //TODO not implemented To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void repaint() {
    notifyDataSetInvalidated();
    getAndroidListView().invalidate();
  }

  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if ((posX < 0) && (posY < 0)) {
      if (enabled != null) {
        setEnabled( enabled.booleanValue() );
      }
      if (visible != null) {
        setVisible( visible.booleanValue() );
      }
      if (focus != null) {
        if (focus.booleanValue()) {
          if (androidListView != null) {
            androidListView.requestFocus();
          }
        }
      }
      if (selected != null) {
        if (androidListView != null) {
          androidListView.setSelected( selected.booleanValue() );
        }
      }
    }
    else {
      //TODO not implemented To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  /**
   *  Implement GestureDetector.OnGestureListener ************************************************
   */

  @Override
  public boolean onDown( MotionEvent motionEvent ) {
    if(DEBUG_STATES){
      Logger.info( "in " + ME + " onDown." );
    }
    return false;
  }

  @Override
  public void onShowPress( MotionEvent motionEvent ) {
    if(DEBUG_STATES){
      Logger.info( "in " + ME + " onShowPress." );
    }
  }

  @Override
  public boolean onSingleTapUp( MotionEvent motionEvent ) {
    if(DEBUG_STATES){
      Logger.info( "in " + ME + " onSingleTapUp." );
    }
    return false;
  }

  @Override
  public boolean onScroll( MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1 ) {
    if(DEBUG_STATES){
      Logger.info( "in " + ME + " onScroll." );
    }
    return false;
  }

  @Override
  public void onLongPress( MotionEvent motionEvent ) {
    if(DEBUG_STATES){
      Logger.info( "in " + ME + " onLong'Press." );
    }
  }

  public enum SwipeListenerType {
    HORIZONTAL,
    VERTICAL,
    BOTH
  }

  @Override
  public boolean onFling( MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1 ) {
    final float MINIMUM_FACTOR_HORIZONTAL = (float) 2.;
    final float MINIMUM_FACTOR_VERTICAL = (float) 0.2;
    final int VERTICAL_SWIPE_DISTANCE = 300;
    final int HORIZONTAL_SWIPE_DISTANCE = 300;

    if (DEBUG_STATES) {
      Logger.info( "in " + ME + " onFling." );
    }
    if (this.gestureListener != null) {

      boolean callOnFling = false;

      float verticalDistance = Math.abs( motionEvent.getY() - motionEvent1.getY() );
      float horizontalDistance = Math.abs( motionEvent.getX() - motionEvent1.getX() );

      boolean horizontalFling = horizontalDistance > verticalDistance;

      //don't give Scroll Gesture to otherGestureListener
      float factor = horizontalDistance / verticalDistance;
      if (horizontalFling && factor >= MINIMUM_FACTOR_HORIZONTAL) {
        if (this.listenerType == SwipeListenerType.HORIZONTAL || this.listenerType == SwipeListenerType.BOTH) {
          if (horizontalDistance >= HORIZONTAL_SWIPE_DISTANCE) {
            callOnFling = true;
          }
        }
      }
      else if (!horizontalFling && factor <= MINIMUM_FACTOR_VERTICAL) {
        if (this.listenerType == SwipeListenerType.VERTICAL || this.listenerType == SwipeListenerType.BOTH) {
          if (verticalDistance >= VERTICAL_SWIPE_DISTANCE) {
            callOnFling = true;
          }
        }
      }

      if (callOnFling) {
        gestureListener.onFling( motionEvent, motionEvent1, v, v1 );
        //if otherGestureListener is added as listener to the listview instead of the listadapter, it will get broken motion events
      }
    }
    return true;
  }


  /**
   *  Implement OnClickListener ************************************************
   */

  /**
   * Called when a view has been clicked.
   *
   * @param v The view that was clicked.
   */
  @Override
  public void onClick( View v ) {
    int pos = this.androidListView.getPositionForView( v );
    if (DEBUG_STATES) Logger.info( "in " + ME + " onClick. pos is " + pos );
    androidListView.performItemClick( v, pos, pos );
  }
}
