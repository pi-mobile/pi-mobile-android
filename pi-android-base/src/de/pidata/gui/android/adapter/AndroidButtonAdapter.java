/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class AndroidButtonAdapter extends AndroidUIAdapter implements UIButtonAdapter, View.OnClickListener {

  private Button androidButton;
  private ButtonViewPI buttonViewPI;

  public AndroidButtonAdapter( ButtonViewPI buttonView, Button androidButton, UIContainer uiContainer ) {
    super( androidButton, uiContainer );
    this.buttonViewPI = buttonView;
    this.androidButton = androidButton;
    androidButton.setOnClickListener(this);
    androidButton.setOnFocusChangeListener( this );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (androidButton != null) {
      androidButton.setOnClickListener( null );
      androidButton.setOnFocusChangeListener( null );
      androidButton = null;
    }
    buttonViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return buttonViewPI;
  }

  @Override
  protected View getAndroidView() {
    return androidButton;
  }

  @Override
  public void onClick(View view) {
    Logger.info( "AndroidButtonAdapter.onClick...");
    buttonViewPI.onClick( this, uiContainer.getDataContext() );
  }

  @Override
  public void performOnClick() {
    onClick( androidButton );
  }

  /**
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( final String label ) {
    final Button androidButton = this.androidButton;
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        int pos = label.indexOf( '|' );
        if (pos > 0) {
          String iconName = label.substring( 0, pos );
          String text = label.substring( pos+1 );
          if (iconName.length() > 0) {
            internalSetImage( iconName, androidButton );
          }
          else {
            internalSetImage( null, androidButton );
          }
          androidButton.setText( text );
        }
        else {
          androidButton.setText( label );
        }
      }
    } );
  }

  private void internalSetImage( String iconName, Button androidButton ) {
    try {
      if (iconName == null) {
        androidButton.setCompoundDrawables( null, null, null, null );
      }
      else {
        QName iconID = UIFactoryAndroid.NAMESPACE_GUI.getQName( iconName );
        int resID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_DRAWABLE, iconID );
        Drawable icon = AndroidApplication.getInstance().getResources().getDrawable( resID );
        icon.setBounds( 0, 0, icon.getMinimumWidth(), icon.getMinimumHeight() );
        androidButton.setCompoundDrawables( icon, null, null, null );
      }
    }
    catch (Exception ex) {
      Logger.error( "Error setting image name="+iconName, ex );
    }
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( final Object text ) {
    final Button androidButton = this.androidButton;
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        if (text == null) {
          androidButton.setText( "" );
        }
        else {
          androidButton.setText( text.toString() );
        }
      }
    });
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( final Object value ) {
    final Button androidButton = this.androidButton;
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        if (value == null) {
          internalSetImage( null, androidButton );
        }
        else {
          internalSetImage( value.toString(), androidButton );
        }
      }
    } );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return String.valueOf( androidButton.getText() );
  }


  @Override
  public void setVisible( final boolean visible ) {
    final Button androidButton = this.androidButton;
    if (androidButton != null) {
      androidButton.post( new Runnable() {
        @Override
        public void run() {
          if (visible) {
            androidButton.setVisibility( View.VISIBLE );
          }
          else {
            androidButton.setVisibility( View.GONE );
          }
        }
      } );
    }
  }

  @Override
  public void setSelected( final boolean selected) {
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        androidButton.setPressed( selected );
      }
    } );
  }

  @Override
  public void setEnabled( final boolean enable) {
    final Button androidButton = this.androidButton;
    if (androidButton != null) {
      androidButton.post( new Runnable() {
        @Override
        public void run() {
          androidButton.setEnabled( enable );
        }
      } );
    }
  }

}
