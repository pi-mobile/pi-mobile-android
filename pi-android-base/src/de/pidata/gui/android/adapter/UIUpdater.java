/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.MenuItem;
import android.view.View;

/**
 * Created by pru on 04.05.16.
 */
public class UIUpdater implements Runnable {

  private MenuItem androidMenuItem;
  private View androidView;
  private AndroidValueAdapter valueAdapter;
  private UpdateJob updateJob;
  private boolean boolValue;
  private Object value;

  public UIUpdater( View androidView, UpdateJob updateJob, boolean boolValue ) {
    this.androidView = androidView;
    this.updateJob = updateJob;
    this.boolValue = boolValue;
  }

  public UIUpdater( View androidView, UpdateJob updateJob, Object value ) {
    this.androidView = androidView;
    this.updateJob = updateJob;
    this.value = value;
  }

  public UIUpdater( MenuItem androidMenuItem, UpdateJob updateJob, boolean boolValue ) {
    this.androidMenuItem = androidMenuItem;
    this.updateJob = updateJob;
    this.boolValue = boolValue;
  }

  /**
   * Starts executing the active part of the class' code. This method is
   * called when a thread is started that has been created with a class which
   * implements {@code Runnable}.
   */
  @Override
  public void run() {
    if (androidView != null) {
      switch (updateJob) {
        case SET_VISIBLE: {
          if (boolValue) androidView.setVisibility( View.VISIBLE );
          else androidView.setVisibility( View.GONE );
          break;
        }
        case SET_ENABLED: {
          androidView.setEnabled( boolValue );
          break;
        }
        case SET_SELECTED: {
          androidView.setSelected( boolValue );
          break;
        }
        case SET_BACKGROUND: {
          if ((value != null) && (value instanceof Integer)) {
            androidView.setBackgroundColor( ((Integer) value).intValue() );
          }
          break;
        }
        case INVALIDATE: {
          androidView.invalidate();
          break;
        }
      }
    }
    else if (androidMenuItem != null) {
      switch (updateJob) {
        case SET_VISIBLE: {
          androidMenuItem.setVisible( boolValue );
          break;
        }
        case SET_ENABLED: {
          androidMenuItem.setEnabled( boolValue );
          break;
        }
        case SET_SELECTED: {
          androidMenuItem.setChecked( boolValue );
          break;
        }
      }
    }
  }
}
