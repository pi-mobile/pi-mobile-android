/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.drawable.ShapeDrawable;
import android.view.View;
import de.pidata.gui.android.component.*;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIPaintAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.gui.view.figure.*;
import de.pidata.rect.*;

import java.util.HashMap;
import java.util.Map;

public class AndroidPaintAdapter extends AndroidUIAdapter implements UIPaintAdapter, View.OnLayoutChangeListener {

  private static final boolean DEBUG = false;

  private AndroidPaintView androidPaintView;
  private PaintViewPI paintViewPI;
  private Map<AndroidShapeAdapter,FigureHandle> handleShapeMap = new HashMap<AndroidShapeAdapter,FigureHandle>();

  public AndroidPaintAdapter( PaintViewPI paintViewPI, AndroidPaintView androidPaintView, UIContainer uiContainer ) {
    super( androidPaintView, uiContainer );
    this.paintViewPI = paintViewPI;
    this.androidPaintView = androidPaintView;
    this.androidPaintView.addOnLayoutChangeListener( this );
    Rect bounds = paintViewPI.getBounds();
    bounds.setWidth( androidPaintView.getWidth() );
    bounds.setHeight( androidPaintView.getHeight() );
    for (int i = 0; i < paintViewPI.figureCount(); i++) {
      figureAdded( paintViewPI.getFigure( i ) );
    }
  }

  private void addShapes( Figure figure ) {
    UIFactory uiFactory = Platform.getInstance().getUiFactory();
    for (int k = 0; k < figure.shapeCount(); k++) {
      ShapePI shapePI = figure.getShape( k );
      AndroidShapeAdapter shapeAdapter = (AndroidShapeAdapter) shapePI.getUIAdapter();
      if (shapeAdapter == null) {
        shapeAdapter = (AndroidShapeAdapter) uiFactory.createShapeAdapter( this, shapePI );
      }
      androidPaintView.add( shapeAdapter );
      shapePI.attachUI( shapeAdapter );
      updateShape( shapeAdapter, shapePI );
    }
  }

  private void removeShape( ShapePI shapePI ) {
    AndroidShapeAdapter shapeAdapter = (AndroidShapeAdapter) shapePI.getUIAdapter();
    if (shapeAdapter != null) {
      androidPaintView.remove( shapeAdapter );
      shapePI.detachUI();
    }
  }

  @Override
  public void detach() {
    androidPaintView = null;
    paintViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return paintViewPI;
  }

  @Override
  protected View getAndroidView() {
    return androidPaintView;
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setSelectionHandles( FigureHandle[] selectionHandles ) {
    for (AndroidShapeAdapter shapeAdapter : handleShapeMap.keySet()) {
      androidPaintView.remove( shapeAdapter );
    }
    handleShapeMap.clear();
    if (selectionHandles != null) {
      for (int i = 0; i < selectionHandles.length; i++) {
        AndroidShapeAdapter androidShapeAdapter = (AndroidShapeAdapter) Platform.getInstance().getUiFactory().createShapeAdapter( this, selectionHandles[i] );
        androidPaintView.add( androidShapeAdapter );
        handleShapeMap.put( androidShapeAdapter, selectionHandles[i] );
      }
    }
  }

  private void updateShape( AndroidShapeAdapter shapeAdapter, ShapePI shapePI ) {
    ShapeDrawable shapeDrawable = shapeAdapter.getShapeDrawable();
    ShapeType shapeType = shapePI.getShapeType();
    ShapeStyle shapeStyle = shapePI.getShapeStyle();
    Rect bounds = shapePI.getBounds();
    // enable shadow support
    if (shapeStyle.getShadowColor() != null && shapeStyle.getShadowRadius() > 0) {
      if (shapeDrawable instanceof StyledDrawable) {
        ((StyledDrawable)shapeDrawable).enableShadow( androidPaintView );
      }
    }
    switch (shapeType) {
      case rect: {
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        if (bounds instanceof RectDir) {
          Rotation rotation = ((RectDir) bounds).getRotation();
          if (rotation.getAngle() != 0.0) {
            shapeAdapter.addTransformation( rotation );
          }
        }
        break;
      }
      case ellipse: {
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        break;
      }
      case arc: {
        if (shapeDrawable instanceof ArcShapeDrawable && shapePI instanceof ArcShapePI) {
          ArcShapePI arcShapePI = (ArcShapePI) shapePI;
          ((ArcShapeDrawable) shapeDrawable).calculateArc( arcShapePI.getStartAngle(), arcShapePI.getSweepAngle() );
        }
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        break;
      }
      case line: {
        // line does not need any bounds or recalculation here
        break;
      }
      case path: {
        if (shapeDrawable instanceof PathShapeDrawable) {
          ((PathShapeDrawable) shapeDrawable).calculatePath( androidPaintView.getWidth(), androidPaintView.getHeight());
        }
        shapeDrawable.setBounds( 0, 0, androidPaintView.getWidth(), androidPaintView.getHeight() );
        break;
      }
      case text: {
        if (shapeDrawable instanceof TextShapeDrawable) {
          ((TextShapeDrawable) shapeDrawable).calculateText( shapePI.getText(), (int) bounds.getWidth(), (int) bounds.getHeight() );
        }
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        break;
      }
      case bitmap: {
        if (shapeDrawable instanceof AnimatedBitmapDrawable) {
          AnimatedBitmapPI animatedBitmapPI = (AnimatedBitmapPI) shapePI;
          ((AnimatedBitmapDrawable) shapeDrawable).calculateAnimation( animatedBitmapPI.getBitmapID(), animatedBitmapPI.getBitmap(), animatedBitmapPI.getBitmapAdjust(), animatedBitmapPI.getAnimationType(), (int) bounds.getWidth(), (int) bounds.getHeight(), androidPaintView);
        }
        else if (shapeDrawable instanceof BitmapDrawable) {
          BitmapPI bitmapPI = (BitmapPI) shapePI;
          ((BitmapDrawable) shapeDrawable).calculateBitmap( bitmapPI.getBitmapID(), bitmapPI.getBitmap(), bitmapPI.getBitmapAdjust(), (int) bounds.getWidth(), (int) bounds.getHeight() );
        }
        shapeDrawable.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        break;
      }
      default: {
        throw new IllegalArgumentException( "Unsupported shape type="+shapeType );
      }
    }
    applyShapeStyle( shapeDrawable, shapeStyle );
    Platform.getInstance().runOnUiThread( new UIUpdater( androidPaintView, UpdateJob.INVALIDATE, null ) );
  }

  /**
   * Notifies that shape for given figure has been modified.
   * If shapePI is null all shapes of figure are modified.
   *
   * @param figure  the figure having modified shape(s)
   * @param shapePI the shape or null for all shapes
   */
  @Override
  public void figureModified( Figure figure, ShapePI shapePI ) {
    UIFactory uiFactory = Platform.getInstance().getUiFactory();
    if (shapePI == null) {
      // remove all shapes of figure - we do not know what changed (add, remove, reorder)
      androidPaintView.removeFigureShapes( figure );

      for (int i = 0; i < figure.shapeCount(); i++) {
        ShapePI childShapePI = figure.getShape( i );
        AndroidShapeAdapter shapeAdapter = ((AndroidShapeAdapter) childShapePI.getUIAdapter());
        if (shapeAdapter == null) {
          shapeAdapter = (AndroidShapeAdapter) uiFactory.createShapeAdapter( this, childShapePI );
          androidPaintView.add( shapeAdapter );
          childShapePI.attachUI( shapeAdapter );
        }
        updateShape( shapeAdapter, childShapePI );
      }
    }
    else {
      AndroidShapeAdapter shapeAdapter = (AndroidShapeAdapter) shapePI.getUIAdapter();
      if (shapeAdapter == null) {
        shapeAdapter = (AndroidShapeAdapter) uiFactory.createShapeAdapter( this, shapePI );
        androidPaintView.add( shapeAdapter );
        shapePI.attachUI( shapeAdapter );
      }
      updateShape( shapeAdapter, shapePI );
    }

    //--- Always update visible handles
    for (Map.Entry<AndroidShapeAdapter,FigureHandle> entry : handleShapeMap.entrySet()) {
      ShapePI handleShapePI = entry.getValue();
      AndroidShapeAdapter fxHandleShape = entry.getKey();
      updateShape( fxHandleShape, handleShapePI );
    }
  }

  @Override
  public void figureAdded( Figure figure ) {
    addShapes( figure );
  }

  @Override
  public void figureRemoved( Figure figure ) {
    androidPaintView.removeFigureShapes( figure );
  }

  @Override
  public void shapeRemoved( Figure figure, ShapePI shapePI ) {
    removeShape( shapePI );
  }

  @Override
  public void onLayoutChange( View view, int left, int top, int right, int bottom,
                              int oldLeft, int oldTop, int oldRight, int oldBottom) {
    Rect bounds = paintViewPI.getBounds();
    bounds.setHeight( androidPaintView.getHeight() );
    bounds.setWidth( androidPaintView.getWidth() );
  }

  public static void applyShapeStyle( ShapeDrawable shapeDrawable, ShapeStyle shapeStyle ) {
    if (shapeDrawable instanceof StyledDrawable) {
      ((StyledDrawable)shapeDrawable).shapeStyleChanged();
    }
    else {
      shapeDrawable.getPaint().setColor( ((AndroidColor) shapeStyle.getFillColor()).getColorInt() );
    }
  }
}
