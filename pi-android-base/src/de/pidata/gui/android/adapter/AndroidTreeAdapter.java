package de.pidata.gui.android.adapter;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITreeAdapter;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.system.android.tree.AndroidTreeView;
import de.pidata.system.android.tree.TreeNode;
import de.pidata.system.android.tree.TreeNodeClickListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AndroidTreeAdapter extends AndroidUIAdapter implements UITreeAdapter, TreeNodeClickListener {

  private int selectedRowColor;
  private AndroidTreeView androidTreeView;
  private TreeViewPI treeViewPI;

  private Map<TreeNodePI, TreeNode> itemMap = new HashMap<>();

  public AndroidTreeAdapter( TreeViewPI treeViewPI, ViewGroup view, UIContainer uiContainer ) {
    super( view, uiContainer );
    this.treeViewPI = treeViewPI;
    TreeNodePI rootNode = treeViewPI.getRootNode();
    this.androidTreeView = new AndroidTreeView( view );
    if (rootNode != null) {
      setRootNode( rootNode );
    }
    this.androidTreeView.setDefaultNodeClickListener( this );

    try {
      int id = UIFactoryAndroid.getRessourceID( "color", ControllerBuilder.NAMESPACE.getQName( "spinner_selection" ) );
      this.selectedRowColor = view.getResources().getColor( id );
    }
    catch (Exception ex) {
      this.selectedRowColor = Color.DKGRAY;
    }
  }

  @Override
  protected View getAndroidView() {
    return androidTreeView.getView();
  }

  @Override
  public ViewPI getViewPI() {
    return treeViewPI;
  }

  public TreeNode getTreeItem( TreeNodePI nodeViewPI) {
    return itemMap.get( nodeViewPI );
  }

  @Override
  public void setRootNode( TreeNodePI rootNode ) {
    TreeNode treeItem = getTreeItem( rootNode );
    if (treeItem == null) {
      treeItem = new TreeNode( rootNode );
      this.itemMap.put( rootNode, treeItem );
    }
    androidTreeView.setRootNode( treeItem );
  }

  @Override
  public void updateNode( TreeNodePI treeNode ) {
    TreeNode treeItem = getTreeItem( treeNode );
    if (treeItem != null) {
      treeItem.update();
    }
  }

  @Override
  public void updateChildList( TreeNodePI treeNodePI ) {
    TreeNode treeItem = getTreeItem( treeNodePI );
    if (treeItem != null) {
      List<TreeNode> childItems = treeItem.getChildren();
      int i = 0;
      int childItemCount = childItems.size();
      for (TreeNodePI childNode : treeNodePI.childNodeIter()) {
        if (i < childItemCount) {
          TreeNode childItem = childItems.get( i );
          while ((i < childItemCount) && (childItem.getValue() != childNode)) {
            this.androidTreeView.removeNode( childItem );
            this.itemMap.remove( childItem.getValue() );
            childItemCount--;
            childItem = childItems.get( i );
          }
          if (childItem.getValue() == childNode) {
            // got node at current position
            childItem.update();
            this.itemMap.put( childNode, childItem );
          }
          else {
            TreeNode newItem = new TreeNode( childNode );
            this.androidTreeView.addNode( treeItem, newItem );
            this.itemMap.put( childNode, newItem );
          }
        }
        else {
          // reached end of items: add remaining
          TreeNode newItem = new TreeNode( childNode );
          this.androidTreeView.addNode( treeItem, newItem );
          this.itemMap.put( childNode, newItem );
        }
        i++;
      }
      if (i < childItemCount) {
        TreeNode delItem = childItems.get( i );
        this.androidTreeView.removeNode( delItem );
        this.itemMap.remove( delItem.getValue() );
      }
    }
  }

  @Override
  public TreeNodePI getSelectedNode() {
    List<TreeNode> selectedNodes = androidTreeView.getSelected();
    if ((selectedNodes == null) || (selectedNodes.size() == 0)) {
      return null;
    }
    else {
      return selectedNodes.get( 0 ).getValue();
    }
  }

  @Override
  public void setSelectedNode( TreeNodePI selectedNode ) {
    TreeNode treeItem = getTreeItem( selectedNode );
    androidTreeView.selectNode( treeItem, true );
  }

  @Override
  public void editNode( TreeNodePI treeNode ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setExpanded( TreeNodePI treeNodePI, boolean expand ) {
    TreeNode treeItem = getTreeItem( treeNodePI );
    if (treeItem != null) {
      if (expand) {
        androidTreeView.expandNode( treeItem );
      }
      else {
        androidTreeView.collapseNode( treeItem );
      }
    }
  }

  @Override
  public void onClick( TreeNode node, Object value ) {
    //deselect currently selected nodes
    List<TreeNode> selectedNodes = androidTreeView.getSelected();
    for (TreeNode selectedNode : selectedNodes) {
      selectedNode.setSelected( false );
      selectedNode.getViewHolder().getView().getNodeContainer().setBackgroundColor( Color.TRANSPARENT );
    }
    //set new selected node
    TreeNodePI selectedNode = node.getValue();
    node.setSelected( true );
    node.getViewHolder().getView().getNodeContainer().setBackgroundColor( selectedRowColor );

    this.treeViewPI.onSelectionChanged( selectedNode );
  }
}
