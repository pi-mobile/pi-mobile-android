/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.FlagController;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFlagAdapter;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.string.Helper;

public class AndroidToggleButtonAdapter extends AndroidValueAdapter implements UIFlagAdapter, View.OnClickListener {

  private static final boolean DEBUG_STATES = false;
  private static String ME = AndroidToggleButtonAdapter.class.getSimpleName();

  private ToggleButton toggleButton;
  private FlagViewPI flagViewPI;

  private Object trueValue;
  private Object falseValue;

  public AndroidToggleButtonAdapter( FlagViewPI flagViewPI, ToggleButton toggleButton, UIContainer uiContainer ) {
    super( toggleButton, uiContainer );
    ME = flagViewPI.getComponentID().getName();
    this.flagViewPI = flagViewPI;
    this.toggleButton = toggleButton;
    // Do not listen on changes to avoid flip/flop due to latency of runOnUIThread(): toggleButton.setOnCheckedChangeListener( this );
    toggleButton.setOnClickListener( this );
    Object format = flagViewPI.getFormat();
    if (Helper.isNullOrEmpty( format )) {
      trueValue = BooleanType.TRUE;
      falseValue = BooleanType.FALSE;
    }
    else {
      String formatStr = format.toString();
      int pos = formatStr.indexOf( '|' );
      trueValue = formatStr.substring( 0, pos );
      falseValue = formatStr.substring( pos + 1 );
    }
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (toggleButton != null) {
      toggleButton.setOnCheckedChangeListener( null );
      toggleButton = null;
    }
    flagViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return flagViewPI;
  }

  @Override
  protected View getAndroidView() {
    return toggleButton;
  }

  public void internalSetValue( Object value ) {
    if (toggleButton != null) {
      //--- Read current model value to avoid endless flip flop when using ToggleButton as ButtonGroup
      //    Note: due to run on UI thread model may have changed in between
      Binding binding = ((FlagController) flagViewPI.getController()).getValueBinding();
      if (binding != null) {
        value = binding.fetchModelValue();
      }
      
      boolean boolVal;
      if (value == null) boolVal = false;
      else boolVal = value.equals( trueValue );
      toggleButton.setChecked( boolVal );
      toggleButton.refreshDrawableState();
    }
  }

  public Object internalGetValue() {
    Object modelValue = null;
    if (toggleButton != null) {
      if (toggleButton.isChecked()) modelValue = trueValue;
      else modelValue = falseValue;
    }
    return modelValue;
  }

  public void performOnClick() {
    Boolean isChecked = (Boolean) getValue();
    if (DEBUG_STATES) Logger.info( "in " + ME + " performOnClick. getValue() returned " + isChecked );
    if (isChecked) {
      setValue( falseValue );
    }
    else {
      setValue( trueValue );
    }
  }

  @Override
  public void setLabelValue( final Object value ) {
    toggleButton.post( new Runnable() {
      @Override
      public void run() {
        if (value == null) {
          toggleButton.setText( "" );
        }
        else {
          toggleButton.setText( value.toString() );
        }
      }
    } );
  }

  @Override
  public Object getLabelValue() {
    return this.toggleButton.getText();
  }

  /**
   * Called when a view has been clicked.
   *
   * @param v The view that was clicked.
   */
  @Override
  public void onClick( View v ) {
    flagViewPI.onCheckChanged( this, toggleButton.isChecked(), uiContainer.getDataContext() );
  }
}
