/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.app.*;
import android.view.MenuItem;
import android.view.View;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.activity.PiFragment;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class AndroidFragmentAdapter extends AndroidUIAdapter implements UIFragmentAdapter, AndroidUIContainer {

  private FragmentManager fragmentManager;
  private FragmentTransaction fragmentTransaction;
  private boolean isInited = false;

  private View fragmentContainer;
  private ModuleViewPI moduleViewPI;
  private ModuleGroup activeModule;
  private PiFragment activeFragment;

  public AndroidFragmentAdapter( ModuleViewPI moduleViewPI, View fragmentContainer, FragmentManager fragmentManager, UIContainer uiContainer ) {
    super( fragmentContainer, uiContainer );
    this.moduleViewPI = moduleViewPI;
    this.fragmentContainer = fragmentContainer;
    this.fragmentManager = fragmentManager;
  }

  @Override
  protected View getAndroidView() {
    return fragmentContainer;
  }

  @Override
  public ViewPI getViewPI() {
    return moduleViewPI;
  }

  public ModuleViewPI getModuleViewPI() {
    return moduleViewPI;
  }

  public PiFragment getActiveFragment() {
    return activeFragment;
  }

  public void setActiveFragment( PiFragment activeFragment ) {
    this.activeFragment = activeFragment;
    isInited = true;
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    if (activeModule == null) {
      return null;
    }
    else {
      return activeModule.getModel();
    }
  }

  @Override
  public View findAndroidView( QName componentID ) {
    PiFragment fragment = getActiveFragment();
    if (fragment == null) {
      return null;
    }
    if (componentID == null) {
      return fragment.getView();
    }
    int id = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, componentID );
    View androidView;
    try {
      androidView = fragment.getView().findViewById( id );
    }
    catch (Exception ex) {
      androidView = null;
    }
    return androidView;
  }

  @Override
  public MenuItem findMenuItem( QName viewID ) {
    return null;
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  @Override
  public Activity getActivity() {
    DialogController dlgCtrl = getModuleViewPI().getController().getControllerGroup().getDialogController();
    return ((AndroidDialog) dlgCtrl.getDialogComp()).getActivity();
  }

  @Override
  public FragmentManager getChildFragmentManager() {
    PiFragment fragment = getActiveFragment();
    if (fragment == null) {
      return null;
    }
    else {
      return fragment.getChildFragmentManager();
    }
  }

  @Override
  public void setVisible( boolean visible ) {
    super.setVisible( visible );
  }

  /**
   * Called by ModuleViewPI whenever the module has been replaced
   *
   * @param moduleGroup the new module
   */
  @Override
  public void moduleChanged( final ModuleGroup moduleGroup ) {
    final Fragment oldFragment = activeFragment;
    this.activeModule = moduleGroup;
    if (moduleGroup == null) {
      activeFragment = null;
    }
    else {
      activeFragment = ((UIFactoryAndroid) Platform.getInstance().getUiFactory()).createFragment( moduleGroup, getModuleViewPI() );
    }
    Logger.info( "ModuleChanged: Created new fragment="+activeFragment+", moduleID="+ moduleGroup );
    try {
      Thread.sleep( 100 );  // Without this sleep often post() is not executed !?
    }
    catch (InterruptedException e) {
      // do nothing
    }
    if ((oldFragment != null) || activeFragment != null) {
      fragmentContainer.post( new Runnable() {
        @Override
        public void run() {
          try {
            if (!isInited) {
              if (activeFragment != null) {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add( fragmentContainer.getId(), activeFragment, activeFragment.getClass().getName() );
                isInited = true;
              }
            }
            else {
              if (activeFragment == null) {
                if ((oldFragment instanceof android.app.DialogFragment) && ((android.app.DialogFragment) oldFragment).getShowsDialog()) {
                  Dialog dialog = ((android.app.DialogFragment) oldFragment).getDialog();
                  if (dialog == null) {
                    Logger.info( "ModuleChanged: Cannot close dialog, dialog is null for fragment=" + activeFragment );
                  }
                  else {
                    dialog.cancel();
                    Logger.info( "ModuleChanged: Successfully closed dialog fragment="+activeFragment );
                  }
                  return;
                }
                else {
                  fragmentTransaction = fragmentManager.beginTransaction();
                  fragmentTransaction.remove( oldFragment );
                  isInited = false;
                }
              }
              else {
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace( fragmentContainer.getId(), activeFragment, activeFragment.getClass().getName() );
              }
            }
            if( fragmentTransaction!=null ) {
              fragmentTransaction.commitAllowingStateLoss();
            }
            fragmentManager.executePendingTransactions();
            Logger.info( "ModuleChanged: Successfully loaded fragment="+activeFragment );
          }
          catch (Exception e) {
            Logger.error("ModuleChanged: Error while trying to change a fragment="+activeFragment, e);
          }
        }
      } );
    }
  }
}
