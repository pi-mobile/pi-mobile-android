package de.pidata.gui.android.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.GuiBuilder;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;

public class AndroidImageButtonAdapter extends AndroidUIAdapter implements UIButtonAdapter, View.OnClickListener {

  private ImageButton androidButton;
  private ButtonViewPI buttonViewPI;

  public AndroidImageButtonAdapter( ButtonViewPI buttonView, ImageButton androidButton, UIContainer uiContainer ) {
    super( androidButton, uiContainer );
    this.buttonViewPI = buttonView;
    this.androidButton = androidButton;
    androidButton.setOnClickListener(this);
    androidButton.setOnFocusChangeListener( this );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (androidButton != null) {
      androidButton.setOnClickListener( null );
      androidButton.setOnFocusChangeListener( null );
      androidButton = null;
    }
    buttonViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return buttonViewPI;
  }

  @Override
  protected View getAndroidView() {
    return androidButton;
  }

  @Override
  public void onClick(View view) {
    Logger.info( "AndroidButtonAdapter.onClick...");
    buttonViewPI.onClick( this, uiContainer.getDataContext() );
  }

  @Override
  public void performOnClick() {
    onClick( androidButton );
  }

  @Override
  public void setLabel( final String iconName ) {
    final ImageButton androidButton = this.androidButton;
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        if (Helper.isNullOrEmpty( iconName )) {
          internalSetImage( null, androidButton );
        }
        else {
          internalSetImage( GuiBuilder.NAMESPACE.getQName( iconName ), androidButton );
        }
      }
    } );
  }

  private void internalSetImage( Object value, ImageButton androidButton ) {
    try {
      UIFactoryAndroid.updateImageView( androidButton, value );
    }
    catch (Exception ex) {
      Logger.error( "Error setting image name="+value, ex );
    }
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    setGraphic( text );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( final Object value ) {
    final ImageButton androidButton = this.androidButton;
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        if (value == null) {
          internalSetImage( null, androidButton );
        }
        else {
          internalSetImage( value, androidButton );
        }
      }
    } );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return null;
  }


  @Override
  public void setVisible( final boolean visible ) {
    final ImageButton androidButton = this.androidButton;
    if (androidButton != null) {
      androidButton.post( new Runnable() {
        @Override
        public void run() {
          if (visible) {
            androidButton.setVisibility( View.VISIBLE );
          }
          else {
            androidButton.setVisibility( View.GONE );
          }
        }
      } );
    }
  }

  @Override
  public void setSelected( final boolean selected) {
    androidButton.post( new Runnable() {
      @Override
      public void run() {
        androidButton.setPressed( selected );
      }
    } );
  }

  @Override
  public void setEnabled( final boolean enable) {
    final ImageButton androidButton = this.androidButton;
    if (androidButton != null) {
      androidButton.post( new Runnable() {
        @Override
        public void run() {
          androidButton.setEnabled( enable );
        }
      } );
    }
  }
}

