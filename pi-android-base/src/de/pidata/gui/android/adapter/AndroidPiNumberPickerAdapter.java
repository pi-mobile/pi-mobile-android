package de.pidata.gui.android.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.component.PiNumberPicker;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.string.Helper;

public class AndroidPiNumberPickerAdapter extends AndroidValueAdapter implements UIProgressBarAdapter, TextWatcher, View.OnClickListener {

  private static final boolean DEBUG = false;

  private ProgressBarViewPI progressBarViewPI;
  private PiNumberPicker numberPicker;
  private UIContainer uiContainer;
  private final EditText numberText;
  private Integer minValue;
  private Integer maxValue;
  private final int stepSize;
  private final Button decreaseBtn;
  private final Button increaseBtn;

  public AndroidPiNumberPickerAdapter( ProgressBarViewPI progressBarViewPI, PiNumberPicker androidView, UIContainer uiContainer ) {
    super( androidView, uiContainer );

    this.progressBarViewPI = progressBarViewPI;
    this.numberPicker = androidView;
    this.uiContainer = uiContainer;

    this.minValue = numberPicker.getMinValue();
    this.maxValue = numberPicker.getMaxValue();
    this.stepSize = numberPicker.getStepSize();

    decreaseBtn = androidView.findViewById( getResID( "number_picker_decrease" ) );
    decreaseBtn.setOnClickListener( this );
    increaseBtn = androidView.findViewById( getResID( "number_picker_increase" ) );
    increaseBtn.setOnClickListener( this );
    numberText = androidView.findViewById( getResID( "number_picker_text" ) );
    numberText.addTextChangedListener( this );
  }

  public static int getResID( String id ) {
    return UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_ID, ControllerBuilder.NAMESPACE.getQName( id ) );
  }

  @Override
  public ViewPI getViewPI() {
    return progressBarViewPI;
  }

  @Override
  protected View getAndroidView() {
    return numberPicker;
  }

  @Override
  public void setMaxValue( int maxValue ) {
    this.maxValue = maxValue;
  }

  @Override
  public int getMaxValue() {
    return maxValue;
  }

  @Override
  public void setMinValue( int minValue ) {
    this.minValue = minValue;
  }

  @Override
  public int getMinValue() {
    return minValue;
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    // not supported
  }

  @Override
  public void setProgress( double progress ) {
    numberText.setText( (int) progress );
  }

  @Override
  public void showError( String errorMessage ) {
    // not supported
  }

  @Override
  public void showInfo( String infoMessage ) {
    // not supported
  }

  @Override
  public void showWarning( String warningMessage ) {
    // not supported
  }

  @Override
  public void hideError() {
    // not supported
  }

  @Override
  public void resetColor() {
    // not supported
  }

  /**
   * Used by getValue to read this component's value
   *
   * @return this component's value
   */
  @Override
  protected Object internalGetValue() {
    if (numberText == null) {
      return null;
    }

    try {
      return Integer.valueOf( numberText.getText().toString() );
    }
    catch (Exception e) {
      Logger.warn( this.getClass().getName() + " could not parse value: " + e.getMessage() );
    }

    return null;
  }

  /**
   * Set new value - uss only in UI Thread like done by ComponentUpdateEvent.run()
   *
   * @param value the new value
   */
  @Override
  protected void internalSetValue( Object value ) {
    if (numberText != null) {
      numberText.setText( value.toString() );
    }
  }

  @Override
  public void beforeTextChanged( CharSequence charSequence, int i, int i1, int i2 ) {
    // do nothing
  }

  @Override
  public void onTextChanged( CharSequence charSequence, int i, int i1, int i2 ) {
    if(DEBUG) {
      Logger.info(  "AndroidPiNumberPickerAdapter ON TEXT CHANGED: " + charSequence.toString() );
    }
    String text = charSequence.toString();
    Integer value = null;
    if (!Helper.isNullOrEmpty( text )) {
      value = Integer.parseInt( text );
    }
    progressBarViewPI.onValueChanged( this, value, uiContainer.getDataContext() );
  }

  @Override
  public void afterTextChanged( Editable editable ) {
    //do nothing
  }

  @Override
  public void onClick( View view ) {
    if (view == this.increaseBtn) {
      Logger.info( "AndroidPiNumberPickerAdapter CLICKED INCREASE" );
      Editable text = numberText.getText();
      Integer oldValue = Integer.valueOf( text.toString() );
      Integer newValue = oldValue + stepSize;
      if ( (maxValue != null) && (newValue > maxValue)) {
        newValue = maxValue;
      }
      numberText.setText( newValue.toString() );
    }
    else if (view == decreaseBtn) {
      Logger.info( "AndroidPiNumberPickerAdapter CLICKED DECREASE" );
      Editable text = numberText.getText();
      Integer oldValue = Integer.valueOf( text.toString() );
      Integer newValue = oldValue - stepSize;
      if ( (minValue != null) && (newValue < minValue)) {
        newValue = minValue;
      }
      numberText.setText( newValue.toString() );
    }
  }
}
