/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.RotateDrawable;
import android.graphics.drawable.ShapeDrawable;
import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.rect.Pos;
import de.pidata.rect.Rotation;
import de.pidata.rect.Transformation;

public class AndroidShapeAdapter implements UIShapeAdapter {

  private ShapePI shapePI;
  private ShapeDrawable shapeDrawable;
  private RotateDrawable rotateDrawable = null; // optional

  public AndroidShapeAdapter( ShapeDrawable shapeDrawable, ShapePI shapePI ) {
    this.shapeDrawable = shapeDrawable;
    this.shapePI = shapePI;
  }

  public ShapeDrawable getShapeDrawable() {
    return shapeDrawable;
  }

  public Drawable getDrawable() {
    if (rotateDrawable == null) {
      return shapeDrawable;
    }
    else {
      return rotateDrawable;
    }
  }

  @Override
  public ShapePI getShapePI() {
    return shapePI;
  }

  private void applyRotation( Rotation rotation ) {
    if (rotateDrawable == null) {
      rotateDrawable = new RotateDrawable();
    }
    rotateDrawable.setDrawable( shapeDrawable );
    Pos center = rotation.getCenter();
    rotateDrawable.setPivotX( (float) center.getX() );
    rotateDrawable.setPivotY( (float) center.getY() );
    float angle = (float) rotation.getAngle();
    rotateDrawable.setFromDegrees( angle );
    rotateDrawable.setToDegrees( angle );
    rotateDrawable.setVisible( true, true );
  }

  @Override
  public void addTransformation( Transformation transformation ) {
    if (transformation instanceof Rotation) {
      if (shapeDrawable instanceof de.pidata.gui.android.component.BitmapDrawable) {
        ((de.pidata.gui.android.component.BitmapDrawable) shapeDrawable).applyRotation( (Rotation) transformation );
      }
      else {
        applyRotation( (Rotation) transformation );
      }
    }
    else {
      throw new IllegalArgumentException( "TODO" );
    }
  }

  @Override
  public void transformationChanged( Transformation transformation ) {
    if (transformation instanceof Rotation) {
      if (shapeDrawable instanceof de.pidata.gui.android.component.BitmapDrawable) {
        ((de.pidata.gui.android.component.BitmapDrawable) shapeDrawable).applyRotation( (Rotation) transformation );
      }
      else {
        applyRotation( (Rotation) transformation );
      }
    }
    else {
      throw new IllegalArgumentException( "TODO" );
    }
  }

  public void onClick( int button, float x, float y ) {
    shapePI.getFigure().onMousePressed( button, x, y, shapePI );
  }
}
