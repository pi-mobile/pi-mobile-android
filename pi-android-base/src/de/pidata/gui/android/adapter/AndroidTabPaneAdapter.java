package de.pidata.gui.android.adapter;

import android.content.res.Resources;
import android.view.View;
import android.widget.RadioGroup;
import de.pidata.gui.android.component.AndroidTab;
import de.pidata.gui.android.component.AndroidTabPane;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITabPaneAdapter;
import de.pidata.gui.view.base.TabPaneViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AndroidTabPaneAdapter extends AndroidUIAdapter implements UITabPaneAdapter, RadioGroup.OnCheckedChangeListener {

  private final AndroidTabPane tabPane;
  private RadioGroup tabs;
  private Map<AndroidTab, View> tabContainerMap;
  private final TabPaneViewPI tabPaneViewPI;

  public AndroidTabPaneAdapter( AndroidTabPane tabPane, TabPaneViewPI tabPaneViewPI, UIContainer uiContainer ) {
    super( tabPane, uiContainer );
    this.tabPane = tabPane;
    this.tabPaneViewPI = tabPaneViewPI;
    for (int i = 0; i < tabPane.getChildCount(); i++) {
      View child = tabPane.getChildAt( i );
      if (child instanceof RadioGroup) {
        this.tabs = (RadioGroup) child;
        ((RadioGroup) child).setOnCheckedChangeListener( this );
        tabContainerMap = initMap( tabPane, (RadioGroup) child );
      }
    }
    if(tabs == null){
      Logger.error("Missing RadioGroup inside AndroidTabPane id=" + tabPaneViewPI.getComponentID());
    }
  }

  private Map<AndroidTab, View> initMap( AndroidTabPane tabPane, RadioGroup child ) {
    Map<AndroidTab, View> map = new HashMap<>();
    for (int j = 0; j < child.getChildCount(); j++) {
      View radioChild = child.getChildAt( j );
      if (radioChild instanceof AndroidTab) {
        AndroidTab tab = (AndroidTab) radioChild;
        int id = tab.getId();
        if (id > 0) {
          int containerId = tab.getContainerId();
          View container = tabPane.findViewById( containerId );
          if (container != null) {
            map.put( tab, container );
          }
          else {
            Logger.error( "Missing Attribute tab_container for AndroidTab id=" + tab.getResources().getResourceEntryName( id ) );
          }
        }
        else {
          Logger.error( "Missing Attribute id for child Tab of AndroidTabPane id=" + tabPane.getResources().getResourceEntryName( tabPane.getId() ) );
        }
      }
    }
    return map;
  }

  @Override
  protected View getAndroidView() {
    return tabPane;
  }

  @Override
  public String getSelectedTab() {
    return null;
  }

  @Override
  public List<String> getTabs() {
    return null;
  }

  @Override
  public void setSelectedTab( String tabToSelect ) {

  }

  @Override
  public void setSelectedTab( int index ) {

  }

  @Override
  public void setTabDisabled( String tabToDisable, boolean disabled ) {

  }

  @Override
  public void setTabText( String tabId, String text ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public ViewPI getViewPI() {
    return null;
  }


  /**
   * <p>Called when the checked radio button has changed. When the
   * selection is cleared, checkedId is -1.</p>
   *
   * @param group     the group in which the checked radio button has changed
   * @param checkedId the unique identifier of the newly checked radio button
   */
  @Override
  public void onCheckedChanged( RadioGroup group, int checkedId ) {
    //make corresponding container visible

    View childView = group.findViewById( checkedId );
    if (childView instanceof AndroidTab) {
      updateContainers( (AndroidTab) childView );
    }
    if (childView != null) {
      Resources resources = childView.getContext().getResources();
      String stringId = resources.getResourceEntryName( checkedId );
      tabPaneViewPI.tabChanged( stringId );
    }
  }

  private void updateContainers( AndroidTab selectedTab ) {
    for (Map.Entry<AndroidTab, View> entry : tabContainerMap.entrySet()) {
      AndroidTab tab = entry.getKey();
      View container = entry.getValue();
      int visibility = View.GONE;
      if (tab == selectedTab) {
        visibility = View.VISIBLE;
      }
      container.setVisibility( visibility );
    }
  }
}
