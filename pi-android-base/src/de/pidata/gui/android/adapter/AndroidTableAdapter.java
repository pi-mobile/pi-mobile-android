/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.GestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.List;

public class AndroidTableAdapter extends AndroidListAdapter implements UITableAdapter {

  public AndroidTableAdapter( TableViewPI tableViewPI, ListView androidListView, UIContainer uiContainer, GestureDetector.OnGestureListener gestureListener, SwipeListenerType swipeListenerType ) {
    super( tableViewPI, androidListView, uiContainer, gestureListener, swipeListenerType );
  }

  /**
   * Callback method to be invoked when an item in this AdapterView has
   * been clicked.
   * <p/>
   * Implementers can call getItemAtPosition(position) if they need
   * to access the data associated with the selected item.
   *
   * @param parent   The AdapterView where the click happened.
   * @param view     The view within the AdapterView that was clicked (this
   *                 will be a view provided by the adapter)
   * @param position The position of the view in the adapter.
   * @param id       The row id of the item that was clicked.
   */
  @Override
  public void onItemClick( AdapterView<?> parent, View view, int position, long id ) {
    if (DEBUG_STATES) Logger.info( "in " + ME + " onItemClick. pos is " + position );
    if (oldOnItemClickListener != null) {
      oldOnItemClickListener.onItemClick(parent, view, position, id);
    }
    TableViewPI tableView = (TableViewPI) listViewPI;
    AndroidCellAdapter cellAdapter = findCellAdapter( view.getId() );
    Model selectedRow = getDisplayRow( position );
    setSelected( selectedRow, true );
    if (cellAdapter == null) {
      tableView.onSelectedCell( selectedRow, null );
    }
    else {
      tableView.onSelectedCell( selectedRow, cellAdapter.getColumnInfo() );
    }
  }

  public int getItemViewType( int position) {
    if (androidListView == null) {
      return VIEWTYPE_RENDERROW;
    }
    else {
      if (position == androidListView.getSelectedItemPosition()) {
        return VIEWTYPE_EDITROW;
      }
      else {
        return VIEWTYPE_RENDERROW;
      }
    }
  }

  public int getViewTypeCount() {
    if (isEditable()) return 2;
    else return 1;
  }

  private boolean isEditable() {
    if (listViewPI != null) {
      return ((TableViewPI)listViewPI).isEditable();
    }
    return false;
  }

  /**
   * Called by TableViewPI to start cell editing form application code.
   * This call should do the same like a mouse click on an editable cell.
   *
   * @param rowModel   row model to edit
   * @param columnInfo column to edit
   */
  @Override
  public void editCell( Model rowModel, ColumnInfo columnInfo ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public HashMap<QName, Object> getFilteredValues() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void unselectValues( HashMap<QName, Object> unselectedValuesMap ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public List<Model> getVisibleRowModels() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void columnChanged( ColumnInfo columnInfo ) {


  }

  @Override
  public void resetFilter() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetFilter( ColumnInfo columnInfo ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void finishedUpdateAllRows() {
    // do nothing
  }
}
