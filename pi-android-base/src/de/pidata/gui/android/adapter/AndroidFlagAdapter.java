/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.View;
import android.widget.CompoundButton;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFlagAdapter;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.BooleanType;
import de.pidata.string.Helper;

public class AndroidFlagAdapter extends AndroidValueAdapter implements UIFlagAdapter, CompoundButton.OnCheckedChangeListener {

  private static final boolean DEBUG_STATES = false;
  private static String ME = AndroidFlagAdapter.class.getSimpleName();

  private CompoundButton checkbox;
  private FlagViewPI flagViewPI;

  private Object trueValue;
  private Object falseValue;

  public AndroidFlagAdapter( FlagViewPI flagViewPI, CompoundButton compoundButtonView, UIContainer uiContainer ) {
    super( compoundButtonView, uiContainer );
    ME = flagViewPI.getComponentID().getName();
    this.flagViewPI = flagViewPI;
    this.checkbox = compoundButtonView;
    checkbox.setOnCheckedChangeListener( this );
    Object format = flagViewPI.getFormat();
    if (Helper.isNullOrEmpty( format )) {
      trueValue = BooleanType.TRUE;
      falseValue = BooleanType.FALSE;
    }
    else {
      String formatStr = format.toString();
      int pos = formatStr.indexOf( '|' );
      trueValue = formatStr.substring( 0, pos );
      falseValue = formatStr.substring( pos + 1 );
    }
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (checkbox != null) {
      checkbox.setOnCheckedChangeListener( null );
      checkbox = null;
    }
    flagViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return flagViewPI;
  }

  @Override
  protected View getAndroidView() {
    return checkbox;
  }

  public void internalSetValue( Object value ) {
    if (checkbox != null) {
      boolean boolVal;
      if (value == null) boolVal = false;
      else boolVal = value.equals( trueValue );
      checkbox.setChecked( boolVal );
      checkbox.refreshDrawableState();
    }
  }

  public Object internalGetValue() {
    Object modelValue = null;
    if (checkbox != null) {
      if (checkbox.isChecked()) modelValue = trueValue;
      else modelValue = falseValue;
    }
    return modelValue;
  }

  /**
   * Native Android method called when the checked state of a compound button has changed.
   *
   * @param buttonView The compound button view whose state has changed.
   * @param isChecked  The new checked state of buttonView.
   */
  @Override
  public void onCheckedChanged( CompoundButton buttonView, boolean isChecked ) {
    if (DEBUG_STATES) Logger.info( "in " + ME + " onCheckChanged. value is " + isChecked + "(or " + checkbox.isChecked() + ")");
    if (isChecked) flagViewPI.onCheckChanged( this, true, uiContainer.getDataContext() );
    else flagViewPI.onCheckChanged( this, false, uiContainer.getDataContext() );
  }

  public void performOnClick() {
    Boolean isChecked = (Boolean) getValue();
    if (DEBUG_STATES) Logger.info( "in " + ME + " performOnClick. getValue() returned " + isChecked );
    if (isChecked) {
      setValue( falseValue );
    }
    else {
      setValue( trueValue );
    }
  }

  @Override
  public void setLabelValue( Object value ) {
    if(value != null) {
      checkbox.setText( (String) value );
    }
  }

  @Override
  public Object getLabelValue() {
    return checkbox.getText();
  }
}
