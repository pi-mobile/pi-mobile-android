/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.View;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIValueAdapter;
import de.pidata.models.tree.Model;

public abstract class AndroidValueAdapter extends AndroidUIAdapter implements UIValueAdapter, UpdateEventHandler {

  private ComponentUpdateEvent lastUpdateEvent;

  protected AndroidValueAdapter( View view, UIContainer uiContainer ) {
    super( view, uiContainer );
  }

  /**
   * Used by getValue to read this component's value
   * @return this component's value
   */
  protected abstract Object internalGetValue();

  /**
   * Returns this component's value. If a update event is pending the value
   * is taken from that event
   * @return this component's value
   */
  public final Object getValue() {
    synchronized (this) {
      if (lastUpdateEvent == null) {
        return internalGetValue();
      }
      else {
        return lastUpdateEvent.getValue();
      }
    }
  }

  /**
   * Set new value - uss only in UI Thread like done by ComponentUpdateEvent.run()
   * @param value the new value
   */
  protected abstract void internalSetValue( Object value );

  /**
   * Sets this component's value in GUI thread, i.e. by calling guiHandler.post()
   * The event is stored in this.lastUpdateEvent to allow getValue() to always get
   * the current value, even when pending in an ComponentUpdateEvent.
   * @param value the new value for this component
   */
  public synchronized void setValue( Object value ) {
    this.lastUpdateEvent = new ComponentUpdateEvent( this, value );
    Platform.getInstance().runOnUiThread( this.lastUpdateEvent );
  }


  /**
   * Process update event in UI thread, called by ComponentUpdateEvent.run()
   * @param updateEvent the update event to process
   */
  public final void processUpdateEvent( ComponentUpdateEvent updateEvent ) {
    internalSetValue( updateEvent.getValue() );
    synchronized (this) {
      if (this.lastUpdateEvent == updateEvent) {
        this.lastUpdateEvent = null;
      }
    }
  }
}
