/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public abstract class AndroidUIAdapter implements UIAdapter, View.OnFocusChangeListener {

  private Integer initialBackgroundColor;
  protected UIContainer uiContainer;

  protected abstract View getAndroidView();

  protected AndroidUIAdapter( View view, UIContainer uiContainer ) {
    this.uiContainer = uiContainer;
    view.setOnFocusChangeListener( this );
    Drawable background = view.getBackground();
    if(background instanceof ColorDrawable) {
      this.initialBackgroundColor = ((ColorDrawable) background).getColor();
    }
  }

  @Override
  public UIContainer getUIContainer() {
    return uiContainer;
  }

  @Override
  public void setVisible( boolean visible ) {
    View androidView = getAndroidView();
    if (androidView != null) {
      Platform.getInstance().runOnUiThread( new UIUpdater( androidView, UpdateJob.SET_VISIBLE, visible) );
    }
  }

  @Override
  public boolean isVisible() {
    View androidView = getAndroidView();
    if (androidView != null) {
      return (androidView.getVisibility() == View.VISIBLE);
    }
    return false;
  }

  @Override
  public void setEnabled( boolean enabled ) {
    View androidView = getAndroidView();
    if (androidView != null) {
      Platform.getInstance().runOnUiThread( new UIUpdater( androidView, UpdateJob.SET_ENABLED, enabled) );
    }
  }

  @Override
  public boolean isEnabled() {
    View androidView = getAndroidView();
    if (androidView != null) {
      return androidView.isEnabled();
    }
    return false;
  }

  @Override
  public boolean isFocused() {
    View androidView = getAndroidView();
    if (androidView != null) {
      return androidView.isFocused();
    }
    return false;
  }

  /**
   * Set this UIAdapter's background color
   *
   * @param color new background color
   */
  @Override
  public void setBackground( ComponentColor color ) {
    View androidView = getAndroidView();
    if (androidView != null) {
      Platform.getInstance().runOnUiThread( new UIUpdater( androidView, UpdateJob.SET_BACKGROUND, color.getColor() ) );
    }
  }

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  @Override
  public void resetBackground() {
    View androidView = getAndroidView();
    if (androidView != null) {
      if (this.initialBackgroundColor != null) {
        Platform.getInstance().runOnUiThread( new UIUpdater( androidView, UpdateJob.SET_BACKGROUND, this.initialBackgroundColor ) );
      }
      else {
        setBackground( Platform.getInstance().getColor( ComponentColor.TRANSPARENT ) );
      }
    }
  }

  public void setSelected( boolean selected ) {
    View view = getAndroidView();
    if (view != null) {
      Platform.getInstance().runOnUiThread( new UIUpdater( view, UpdateJob.SET_SELECTED, selected) );
    }
  }

  public void requestFocus() {
    View view = getAndroidView();
    if (view != null) {
      view.requestFocus();
    }
  }

  protected final Activity getCurrentActivity() {
    View view = getAndroidView();
    if (view != null) {
      return scanForActivity( view.getContext() );
    }
    return null;
  }

  private Activity scanForActivity(Context context) {
    if (context == null) {
      return null;
    }
    else if (context instanceof Activity) {
      return (Activity) context;
    }
    else if (context instanceof ContextWrapper) {
      return scanForActivity( ((ContextWrapper) context).getBaseContext() );
    }
    return null;
  }

  /**
   * Called when the focus state of a view has changed.
   *
   * @param v        The view whose state has changed.
   * @param hasFocus The new focus state of v.
   */
  @Override
  public void onFocusChange( View v, boolean hasFocus ) {
    getViewPI().onFocusChanged( this, hasFocus, uiContainer.getDataContext() );
  }

  @Override
  public void detach() {
    View view = getAndroidView();
    if (view != null) {
      view.setOnFocusChangeListener( null );
    }
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    //TODO not implemented To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void repaint() {
    getAndroidView().invalidate();
  }

  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if (enabled != null) {
      setEnabled( enabled.booleanValue() );
    }
    if (visible != null) {
      setVisible( visible.booleanValue() );
    }
    if (focus != null) {
      if (focus.booleanValue()) {
        requestFocus();
      }
    }
    if (selected != null) {
      setSelected( selected.booleanValue() );
    }
  }

  @Override
  public void performOnClick() {
    //setSelected( true );
  }
}
