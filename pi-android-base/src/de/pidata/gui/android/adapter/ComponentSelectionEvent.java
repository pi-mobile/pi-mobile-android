/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;

public class ComponentSelectionEvent implements Runnable {

  protected static final boolean DEBUG = false;
  protected static String ME = ComponentSelectionEvent.class.getSimpleName();

  private AndroidListAdapter receiver;
  private Model row;
  private boolean selected;

  public ComponentSelectionEvent( AndroidListAdapter receiver, Model row, boolean selected ) {
    this.row = row;
    this.selected = selected;
    this.receiver = receiver;
    receiver.getAndroidListView().post( this );
  }

  /**
   * Starts executing the active part of the class' code. This method is
   * called when a thread is started that has been created with a class which
   * implements {@code Runnable}.
   */
  @Override
  public void run() {
    AdapterView receiverView = receiver.getAndroidListView();
    if (receiverView != null) {
      int lastSelection = receiverView.getSelectedItemPosition();
      int index;
      if (selected) {
        index = receiver.indexOf( row );
      }
      else {
        index = -1;
      }
      if (DEBUG)
        Logger.info( "----------\nin " + ME + " setSelection from " + lastSelection + " to " + index + "(of " + receiverView.getChildCount() + " items)" );

      if (receiverView instanceof ListView) {
        //----- Select item and if possible scroll it into visible area
        ListView listView = (ListView) receiverView;
        int firstVisible = listView.getFirstVisiblePosition();
        int lastVisible = listView.getLastVisiblePosition();
        listView.setAdapter( listView.getAdapter() );  // scrolls to Top
        listView.clearFocus();
        listView.setItemChecked( index, true );

        listView.setSelection( firstVisible );   // restore old scroll position (not excactly)
        if ((index < firstVisible) || (index >= lastVisible)) {
          // smooth scrolling is not intended but seems to be the only way to tell ListView
          // to move selection into visible area
          listView.smoothScrollToPositionFromTop( index, 100, 300 );
        }
      }
      else {
        receiverView.setSelection( index );
      }
    }
  }
}
