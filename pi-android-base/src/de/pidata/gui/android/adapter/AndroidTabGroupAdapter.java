/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.view.View;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITabGroupAdapter;
import de.pidata.gui.view.base.TabGroupViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;

public class AndroidTabGroupAdapter extends AndroidUIAdapter implements UITabGroupAdapter {

  private View tabGroupView;
  private TabGroupViewPI tabGroupViewPI;

  public AndroidTabGroupAdapter( TabGroupViewPI tabGroupViewPI, View tabGroupView, UIContainer uiContainer ) {
    super( tabGroupView, uiContainer );
    this.tabGroupViewPI = tabGroupViewPI;
    this.tabGroupView = tabGroupView;
  }

  @Override
  protected View getAndroidView() {
    return tabGroupView;
  }

  @Override
  public ViewPI getViewPI() {
    return tabGroupViewPI;
  }

  @Override
  public Object getValue() {
    //TODO
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO
    throw new RuntimeException( "TODO" );
  }
}
