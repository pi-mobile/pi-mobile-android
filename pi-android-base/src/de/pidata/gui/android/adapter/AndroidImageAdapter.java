/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIImageAdapter;
import de.pidata.gui.view.base.ImageViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class AndroidImageAdapter extends AndroidUIAdapter implements UIImageAdapter {

  private ImageView imageView;
  private ImageViewPI imageViewPI;

  public AndroidImageAdapter( ImageViewPI imageViewPI, ImageView imageView, UIContainer uiContainer ) {
    super( imageView, uiContainer );
    this.imageViewPI = imageViewPI;
    this.imageView = imageView;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    imageView = null;
    imageViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return imageViewPI;
  }

  @Override
  protected View getAndroidView() {
    return imageView;
  }

  @Override
  public void setImageResource( QName imageResourceID ) {
    try {
      UIFactoryAndroid.updateImageView( imageView, imageResourceID );
    }
    catch (Exception ex) {
      Logger.error( "Error setting image name="+imageResourceID, ex );
    }
  }

  @Override
  public void setImage( ComponentBitmap bitmap ) {
    final ComponentBitmap image = bitmap;
    final ImageView imageView = this.imageView;
    if (imageView != null)
      imageView.post( new Runnable() {
        public void run() {
          if (image == null) {
            imageView.setImageBitmap( null );
          }
          else {
            imageView.setImageBitmap( (Bitmap) image.getImage() );
          }
        }
      }
    );
  }

  @Override
  public boolean isZoomEnabled() {
    return false;
  }

  @Override
  public void setZoomFactor( double zoomFactor ) {
    // TODO
  }

  @Override
  public double getZoomFactor() {
    return 1.0;
  }
}

