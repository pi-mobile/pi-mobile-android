/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.graphics.drawable.Drawable;
import android.view.MenuItem;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class AndroidMenuItemAdapter implements UIButtonAdapter, MenuItem.OnMenuItemClickListener {

  private ButtonViewPI buttonViewPI;
  private MenuItem menuItem;
  private AndroidDialog androidDialog;

  public AndroidMenuItemAdapter( ButtonViewPI buttonViewPI, MenuItem menuItem, AndroidDialog androidDialog, UIContainer uiContainer ) {
    this.buttonViewPI = buttonViewPI;
    this.menuItem = menuItem;
    this.androidDialog = androidDialog;
    menuItem.setOnMenuItemClickListener( this );
  }

  @Override
  public UIContainer getUIContainer() {
    return androidDialog;
  }

  /**
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( final String label ) {
    androidDialog.getActivity().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        int pos = label.indexOf( '|' );
        if (pos > 0) {
          String iconName = label.substring( 0, pos );
          String text = label.substring( pos+1 );
          if (iconName.length() > 0) {
            QName iconID = UIFactoryAndroid.NAMESPACE_GUI.getQName( iconName );
            int resID = UIFactoryAndroid.getRessourceID( UIFactoryAndroid.IDCLASS_DRAWABLE, iconID );
            menuItem.setIcon( resID );
          }
          else {
            menuItem.setIcon( null );
          }
          menuItem.setTitle( text );
        }
        else {
          menuItem.setTitle( label );
        }
      }
    } );
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    final String label;
    if (text == null) {
      label = "";
    }
    else {
      label = text.toString();
    }
    androidDialog.getActivity().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        menuItem.setTitle( label );
      }
    } );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( final Object value ) {
    androidDialog.getActivity().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        menuItem.setIcon( (Drawable) value );
      }
    } );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    return menuItem.getTitle().toString();
  }

  @Override
  public ViewPI getViewPI() {
    return buttonViewPI;
  }

  @Override
  public void setVisible( boolean visible ) {
    Platform.getInstance().runOnUiThread( new UIUpdater( menuItem, UpdateJob.SET_VISIBLE, visible) );
  }

  @Override
  public boolean isVisible() {
    return menuItem.isVisible();
  }

  @Override
  public boolean isFocused() {
    // There is no focus.
    return false;
  }

  @Override
  public void setEnabled( boolean enabled ) {
    Platform.getInstance().runOnUiThread( new UIUpdater( menuItem, UpdateJob.SET_ENABLED, enabled) );
  }

  public boolean isSelected() {
    return menuItem.isChecked();
  }

  public void setSelected( boolean selected ) {
    Platform.getInstance().runOnUiThread( new UIUpdater( menuItem, UpdateJob.SET_SELECTED, selected) );
  }

  @Override
  public boolean isEnabled() {
    return menuItem.isEnabled();
  }

  /**
   * Set this UIAdapter's background color
   *
   * @param color new background color
   */
  @Override
  public void setBackground( ComponentColor color ) {
    throw new RuntimeException( "TODO" );
  }

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  @Override
  public void resetBackground() {
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if (menuItem != null) {

    }
    menuItem = null;
  }

  @Override
  public void repaint() {
    menuItem.getActionView().invalidate();
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  @Override
  public void performOnClick() {
    menuItem.getActionView().performClick();
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   *
   * @param posX     the x position of the source event
   * @param posY     the y position of the source event
   * @param focus    set focus on/off
   * @param selected set selected if true, deselected if false, do not change if null
   * @param enabled  set enabled if true, disabled if false, do not change if null
   * @param visible  set visible if true, invisible if false, do not change if null
   */
  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if (enabled != null) {
      setEnabled( enabled.booleanValue() );
    }
    if (visible != null) {
      setVisible( visible.booleanValue() );
    }
    if (focus != null) {
      // do nothing
    }
    if (selected != null) {
      setSelected( selected );
    }
  }

  /**
   * Called when a menu item has been invoked.  This is the first code
   * that is executed; if it returns true, no other callbacks will be
   * executed.
   *
   * @param item The menu item that was invoked.
   * @return Return true to consume this click and prevent others from
   * executing.
   */
  @Override
  public boolean onMenuItemClick( MenuItem item ) {
    Logger.info( "AndroidMenuItemAdapter.onClick...");
    buttonViewPI.onClick( this, androidDialog.getDataContext() );
    return true;
  }
}
