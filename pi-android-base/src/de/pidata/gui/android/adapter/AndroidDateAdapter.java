package de.pidata.gui.android.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIDateAdapter;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.util.Calendar;

public class AndroidDateAdapter extends AndroidTextAdapter implements UIDateAdapter, View.OnClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

  // Contains the current date
  private Calendar calendar = SystemManager.getInstance().getCalendar();

  // represent the data type: DATE, TIME, DATETIME
  private QName dateFormat = DateTimeType.TYPE_DATE;

  private QName format; // TODO: durchreichen

  private AlertDialog dialog;

  // supported formats: update the list, if necessary
  // 31.12.03 instead of 31.12.2003
  public static final QName FORMAT_SHORTYEAR = NAMESPACE.getQName("SHORTYEAR");
  public static final QName FORMAT_TIME = NAMESPACE.getQName("TIME");
  private long minDate = 0;

  public AndroidDateAdapter( TextViewPI dateViewPI , TextView androidTextView, UIContainer uiContainer ) {
    super( dateViewPI, androidTextView, uiContainer );
    getAndroidView().setOnClickListener( this );
    setKeyboardInput( false );
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    View view = getAndroidView();
    if (view != null) {
      view.setOnClickListener( null );
    }
    super.detach();
  }

  @Override
  public void setDateFormat( QName dateFormat ) {
    this.dateFormat = dateFormat;
  }

  @Override
  public void setMinDate( long minDate ) {
    this.minDate = minDate;
  }

  /**
   * Returns the date value in canonical string format.
   * @return
   */
  public Object internalGetValue() {
    DateObject dateObject = new DateObject( dateFormat, calendar.getTimeInMillis() );
    return dateObject.toString();
  }

  /**
   * Expects the date value in canonical string format.
   * @param value
   */
  @Override
  public void internalSetValue( Object value ) {
    String displayStr;
    if (value == null) {
      displayStr = "";
    }
    else {
      String temp = value.toString();
      if (temp.length() == 0) {
        displayStr = "";
      }
      else {
        String valueString = value.toString();
        String dateWithoutSeconds = valueString.substring( 0, valueString.lastIndexOf( ':' ) );
        DateObject dateObject = new DateObject( dateFormat, dateWithoutSeconds );
        displayStr = dateObject.toDisplayString( calendar, dateFormat, false );
        if (dialog != null) {
          if (dialog instanceof DatePickerDialog) {
            int year = calendar.get( Calendar.YEAR );
            int monthOfYear = calendar.get( Calendar.MONTH );
            int dayOfMonth = calendar.get( Calendar.DAY_OF_MONTH );
            ((DatePickerDialog) dialog).updateDate( year, monthOfYear, dayOfMonth );
          }
          else if (dialog instanceof TimePickerDialog) {
            int hourOfDay = calendar.get( Calendar.HOUR_OF_DAY );
            int minute = calendar.get( Calendar.MINUTE );
            ((TimePickerDialog) dialog).updateTime( hourOfDay, minute );
          }
        }
      }
    }
    super.internalSetValue( displayStr );
  }

  public void showDatePicker( DatePickerDialog.OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth ) {
    Activity currentActivity = getCurrentActivity();
    dialog = new DatePickerDialog( currentActivity, callBack, year, monthOfYear, dayOfMonth );
    if (minDate > 0) {
      ((DatePickerDialog) dialog).getDatePicker().setMinDate( minDate );
    }
    dialog.show();
  }

  public void showTimePicker( TimePickerDialog.OnTimeSetListener callBack, int hourOfDay, int minute ) {
    Activity currentActivity = getCurrentActivity();
    TimePickerDialog dialog = new TimePickerDialog( currentActivity, callBack, hourOfDay, minute, true );
    dialog.show();
  }

  @Override
  public void onFocusChange( View view, boolean hasFocus ) {
    super.onFocusChange( view, hasFocus );
    if (hasFocus) {
      onClick( view );
    }
    else {
      dialog = null;
    }
  }

  public void onClick( View view ) {
    if (isEnabled()) {
      if (dateFormat == DateTimeType.TYPE_DATE || dateFormat == DateTimeType.TYPE_DATETIME) {
        int year = calendar.get( Calendar.YEAR );
        int monthOfYear = calendar.get( Calendar.MONTH );
        int dayOfMonth = calendar.get( Calendar.DAY_OF_MONTH );
        showDatePicker( this, year, monthOfYear, dayOfMonth );
      }
      else if (dateFormat == DateTimeType.TYPE_TIME) {
        int hourOfDay = calendar.get( Calendar.HOUR_OF_DAY );
        int minute = calendar.get( Calendar.MINUTE );
        showTimePicker( this, hourOfDay, minute );
      }
    }
  }

  public void onDateSet( DatePicker datePicker, int year, int monthOfYear, int dayOfMonth ) {
    dialog = null;
    calendar.set( Calendar.YEAR, year );
    calendar.set( Calendar.MONTH, monthOfYear );
    calendar.set( Calendar.DAY_OF_MONTH, dayOfMonth );
    setValue( new DateObject( dateFormat, calendar.getTimeInMillis() ).toString() );

    // additionally get time  TODO not tested ;-)
    if (dateFormat == DateTimeType.TYPE_DATETIME) {
      int hourOfDay = calendar.get( Calendar.HOUR_OF_DAY );
      int minute = calendar.get( Calendar.MINUTE );
      showTimePicker( this, hourOfDay, minute );
    }
    else {
      // Value changed, write to model
      ((TextViewPI) getViewPI()).saveValue();
    }
  }

  public void onTimeSet( TimePicker timePicker, int hourOfDay, int minute ) {
    dialog = null;
    calendar.set( Calendar.HOUR_OF_DAY, hourOfDay );
    calendar.set( Calendar.MINUTE, minute );
    setValue( new DateObject( dateFormat, calendar.getTimeInMillis() ).toString() );
    // Value changed, write to model
    ((TextViewPI) getViewPI()).saveValue();
  }
}
