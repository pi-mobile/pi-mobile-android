/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.platform.android.AndroidColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.view.base.TextViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public class AndroidTextAdapter extends AndroidValueAdapter implements UITextAdapter {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  private TextView androidTextView;
  private TextViewPI textViewPI;
  private boolean keyboardInput = false;
  private TextWatcher textWatcher;

  public AndroidTextAdapter( TextViewPI textViewPI, TextView androidTextView, UIContainer uiContainer ) {
    super( androidTextView, uiContainer );
    this.textViewPI = textViewPI;
    this.androidTextView = androidTextView;
    if(androidTextView instanceof EditText){
      keyboardInput = true;
    }
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    super.detach();
    if (androidTextView != null) {
      androidTextView.setOnFocusChangeListener( null );
      androidTextView = null;
    }
    textViewPI = null;
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    // do nothing - defined in view  TODO: ???
  }

  @Override
  public ViewPI getViewPI() {
    return textViewPI;
  }

  @Override
  protected View getAndroidView() {
    return androidTextView;
  }

  public Object internalGetValue() {
    if (androidTextView != null) return androidTextView.getText();
    else return null;
  }

  /**
   * Process update event in UI thread, called by ComponentUpdateEvent.run()
   *
   * @param value
   */
  public void internalSetValue( Object value ) {
    TextView androidTextView = this.androidTextView;
    if (androidTextView != null) {
      if (value == null) androidTextView.setText( "" );
      else androidTextView.setText( value.toString() );
      androidTextView.refreshDrawableState();
    }
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    if (androidTextView instanceof EditText) {
      ((EditText) androidTextView).setSelection( posX );
    }
  }

  @Override
  public void select( int fromPos, int toPos ) {
    if (androidTextView instanceof EditText) {
      ((EditText) androidTextView).setSelection( fromPos, toPos );
    }
  }

  @Override
  public void selectAll() {
    if (androidTextView instanceof EditText) {
      ((EditText) androidTextView).setSelection( 0, androidTextView.getText().length() );
    }
  }

  public void setEnabled( boolean enabled ) {
    // TextView (Label) gets unreadable light gray if disabled and its no use of disabling it (UseCase: readOnly)
    if ((androidTextView != null) && (this.androidTextView.getClass() != TextView.class)) {
      super.setEnabled( enabled );
    }
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    androidTextView.setTextColor( ((AndroidColor)color).getColorInt() );
  }

  public boolean isKeyboardInput() {
    return keyboardInput;
  }

  public void setKeyboardInput( boolean keyboardInput ) {
    this.keyboardInput = keyboardInput;
  }

  protected void toggleKeyBoard( boolean hasFocus ) {
    if (keyboardInput && (isEnabled())) {
      Platform.getInstance().toggleKeyboard( hasFocus );
    }
    else {
      Platform.getInstance().toggleKeyboard( false );
    }
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    if (listenTextChanges) {
      if (textWatcher == null) {
        textWatcher = new TextWatcher() {
          @Override
          public void beforeTextChanged( CharSequence charSequence, int i, int i1, int i2 ) {

          }

          @Override
          public void onTextChanged( CharSequence charSequence, int i, int i1, int i2 ) {
            String oldvalue = textViewPI.getValue();
            textViewPI.textChanged( oldvalue, charSequence.toString() );
          }

          @Override
          public void afterTextChanged( Editable editable ) {

          }
        };
        this.androidTextView.addTextChangedListener( textWatcher );
      }
    }
    else {
      if (textWatcher != null) {
        this.androidTextView.removeTextChangedListener( textWatcher );
        textWatcher = null;
      }
    }
  }

  //------------------- Interface View.OnFocusChangeListener ------------------------
  public void onFocusChange( View view, boolean hasFocus ) {
    super.onFocusChange( view, hasFocus );
    if (view == getAndroidView()) {
      toggleKeyBoard( hasFocus );
    }
  }
}
