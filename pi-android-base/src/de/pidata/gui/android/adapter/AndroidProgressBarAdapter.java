/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.adapter;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIProgressBarAdapter;
import de.pidata.gui.view.base.ProgressBarViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.DecimalObject;
import de.pidata.qnames.Namespace;

/**
 * <add description here>
 *
 * @author Doris Schwarzmaier
 */
public class AndroidProgressBarAdapter extends AndroidValueAdapter implements UIProgressBarAdapter, SeekBar.OnSeekBarChangeListener {

  public static final Namespace NAMESPACE = Namespace.getInstance("de.pidata.gui");

  private ProgressBar androidProgressBar;
  private ProgressBarViewPI progressBarViewPI;

  public AndroidProgressBarAdapter( ProgressBarViewPI progressBarViewPI, ProgressBar androidProgressBar, UIContainer uiContainer ) {
    super( androidProgressBar, uiContainer );
    this.progressBarViewPI = progressBarViewPI;
    this.androidProgressBar = androidProgressBar;
    if (androidProgressBar instanceof SeekBar) {
      ((SeekBar) androidProgressBar).setOnSeekBarChangeListener( this );
      androidProgressBar.setMax( 100 );
      androidProgressBar.setEnabled( true );
    }
  }

  @Override
  public void detach() {
    super.detach();
    if (androidProgressBar != null) {
      androidProgressBar.setOnFocusChangeListener( null );
      androidProgressBar = null;
    }
    progressBarViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return progressBarViewPI;
  }

  @Override
  protected View getAndroidView() {
    return androidProgressBar;
  }

  @Override
  public void setProgressMessage( Object progressMessage ) {
    // TODO: not implemented yet
    // throw new UnsupportedOperationException( "FIXME: not implemented yet" );
  }

  @Override
  protected Object internalGetValue() {
    if (androidProgressBar != null) {
      int intValue = androidProgressBar.getProgress();
      return new DecimalObject( intValue, 0 );
    }
    return null;
  }

  @Override
  protected void internalSetValue( Object value ) {
    if (androidProgressBar != null) {
      if (value == null) androidProgressBar.setProgress( 0 );
      else {
        int intValue = ((Number)value).intValue();
        int maxValue = androidProgressBar.getMax();
        if (intValue < maxValue) {
          androidProgressBar.setProgress( intValue );
        }
        else {
          androidProgressBar.setProgress( maxValue );
        }
      }
      //androidProgressBar.refreshDrawableState();
    }
  }

  @Override
  public void setProgress( double progress ) {
    DecimalObject progressObj = new DecimalObject( progress, 2 );
    setValue( progressObj );
  }

  @Override
  public void setMaxValue( int maxValue ) {
    if (androidProgressBar != null) {
      androidProgressBar.setMax( maxValue );
    }
  }

  @Override
  public int getMaxValue() {
    if (androidProgressBar == null) {
      return 0;
    }
    else {
      return androidProgressBar.getMax();
    }
  }

  @Override
  public void setMinValue( int minValue ) {
    // do nothing - android's progressbar is fixed to 0
  }

  @Override
  public int getMinValue() {
    // android does not support values beyond 0
    return 0;
  }

  @Override
  public void showError( String errorMessage ) {
    setProgressMessage( errorMessage );
    setProgress( getMaxValue() );
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (androidProgressBar != null) {
          androidProgressBar.setProgressTintList( ColorStateList.valueOf( 0xFF990000 ) );
        }
      }
    } );
  }

  @Override
  public void hideError() {
    setProgressMessage( "" );
  }

  @Override
  public void showInfo( String infoMessage ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (androidProgressBar != null) {
          androidProgressBar.setProgressTintList( ColorStateList.valueOf( 0xFF009900 ) );
        }
      }
    } );
  }

  @Override
  public void showWarning( String warningMessage ) {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (androidProgressBar != null) {
          androidProgressBar.setProgressTintList( ColorStateList.valueOf( 0xFFbbbb00 ) );
        }
      }
    } );
  }

  @Override
  public void resetColor() {
    Platform.getInstance().runOnUiThread( new Runnable() {
      @Override
      public void run() {
        if (androidProgressBar != null) {
          androidProgressBar.setProgressTintList( ColorStateList.valueOf( 0xFF006199 ) );
        }
      }
    } );
  }

  /**
   * Notification that the progress level has changed. Clients can use the fromUser parameter
   * to distinguish user-initiated changes from those that occurred programmatically.
   *
   * @param seekBar  The SeekBar whose progress has changed
   * @param progress The current progress level. This will be in the range min..max where min
   *                 and max were set by {@link ProgressBar#setMin(int)} and
   *                 {@link ProgressBar#setMax(int)}, respectively. (The default values for
   *                 min is 0 and max is 100.)
   * @param fromUser True if the progress change was initiated by the user.
   */
  @Override
  public void onProgressChanged( SeekBar seekBar, int progress, boolean fromUser ) {
    if (fromUser) {
      progressBarViewPI.onValueChanged( this, progress, uiContainer.getDataContext() );
    }
  }

  /**
   * Notification that the user has started a touch gesture. Clients may want to use this
   * to disable advancing the seekbar.
   *
   * @param seekBar The SeekBar in which the touch gesture began
   */
  @Override
  public void onStartTrackingTouch( SeekBar seekBar ) {
    // do nothing
  }

  /**
   * Notification that the user has finished a touch gesture. Clients may want to use this
   * to re-enable advancing the seekbar.
   *
   * @param seekBar The SeekBar in which the touch gesture began
   */
  @Override
  public void onStopTrackingTouch( SeekBar seekBar ) {
    // do nothing
  }
}
