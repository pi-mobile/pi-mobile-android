/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.alt;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.android.controller.AndroidDialogControllerBuilder;
import de.pidata.gui.controller.base.ControllerGroup;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;

public class PIMobileTab extends Activity {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  protected DialogController dialogController;
  protected ControllerGroup tabController;

  protected void onCreate( Bundle savedInstanceState ) {
    super.onCreate( savedInstanceState );

    Uri dataUri = getIntent().getData();
    if (dataUri == null) {
      throw new IllegalArgumentException( "Must not call PIMobileTab without parameter" );
    }
    dialogController = (DialogController) AndroidApplication.getInstance().getFromCache( dataUri );
    AndroidApplication.getInstance().removeFromCache( dataUri );
    String action = getIntent().getAction();
    int pos1 = action.indexOf( '#' );
    int pos2 = action.indexOf( ',', pos1+1 );
    QName tabID = NAMESPACE.getQName( action.substring( 0, pos1 ) );
    setContentView( Integer.parseInt( action.substring( pos1+1, pos2 ) ) );
    View contentView = findViewById( Integer.parseInt( action.substring( pos2+1 ) ) );
    ((AndroidDialog) dialogController.getDialogComp()).addContentView( contentView );
    tabController = ((AndroidDialogControllerBuilder) dialogController.getControllerBuilder()).addTab( dialogController, tabID );
  }
}
