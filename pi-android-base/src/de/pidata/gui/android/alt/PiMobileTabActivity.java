/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.android.alt;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.view.View;
import de.pidata.gui.android.activity.DialogActivity;
import de.pidata.gui.android.activity.PiMobileActivity;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.android.controller.AndroidDialogControllerBuilder;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.DialogControllerDelegate;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.qnames.Namespace;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;

public class PiMobileTabActivity extends TabActivity implements DialogActivity {

  public static final Namespace NAMESPACE = Namespace.getInstance( "de.pidata.gui" );

  protected DialogController dialogController;

  protected void createDialogController( int layoutResourceID, int contentViewID, QName dialogID ) {
    setContentView( layoutResourceID );
    View contentView = findViewById( contentViewID );

    Context context = AndroidApplication.getInstance().getContext();
    AndroidDialog dialog = new AndroidDialog();
    dialog.init( this, contentView );
    AndroidDialogControllerBuilder builder = (AndroidDialogControllerBuilder) Platform.getInstance().getControllerBuilder();
    DialogController parentDlgCtrl = null;
    Activity parentActivity = getParent();
    if ((parentActivity != null) && (parentActivity instanceof DialogActivity)) {
      parentDlgCtrl = ((DialogActivity) parentActivity).getDialogController();
    }
    dialogController = builder.createDialogController( dialogID, context, dialog, parentDlgCtrl );

    DialogControllerDelegate delegate = dialogController.getDelegate();
    if (delegate == null) {
      throw new IllegalArgumentException( "Cannot create dialog id="+dialogID+", delegate is missing" );
    }
    else {
      try {
        ParameterList parameterList = PiMobileActivity.intentToParamList( getIntent() );
        dialogController.setParameterList( parameterList );
      }
      catch (Exception ex) {
        String msg = "Error extracting result ParameterList from Intent";
        Logger.error( msg, ex );
        throw new IllegalArgumentException( msg );
      }
    }
  }

  public DialogController getDialogController() {
    return dialogController;
  }

  protected void onStart() {
    super.onStart();
    //TODO Abgleich mit SkinDialogController.show()
  }

  protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
    super.onActivityResult( requestCode, resultCode, data );
    AndroidDialog androidDialog = (AndroidDialog) dialogController.getDialogComp();
    androidDialog.childClosed( resultCode, data );
  }

}
