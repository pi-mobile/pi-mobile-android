package de.pidata.system.android.tree;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import android.widget.ScrollView;
import de.pidata.gui.view.base.TreeNodePI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public class AndroidTreeView {
  private static final String NODES_PATH_SEPARATOR = ";";

  protected TreeNode rootNode;
  private ViewGroup view;
  private Class<? extends BaseNodeViewHolder> defaultViewHolderClass = SimpleViewHolder.class;
  private TreeNodeClickListener nodeClickListener;
  private TreeNodeLongClickListener nodeLongClickListener;
  private boolean selectionModeEnabled;
  private boolean useDefaultAnimation = true;
  private boolean use2dScroll = false;
  private boolean enableAutoToggle = true;

  public AndroidTreeView( ViewGroup view ) {
    this.view = view;
  }

  public void setRootNode( TreeNode rootNode ) {
    this.rootNode = rootNode;

    ViewGroup scrollView;
    if (use2dScroll) {
      scrollView = new TwoDScrollView( this.view.getContext() );
    }
    else {
      scrollView = new ScrollView( this.view.getContext() );
    }
    final LinearLayout viewTreeItems = new LinearLayout( this.view.getContext(), null );
    viewTreeItems.setOrientation( LinearLayout.VERTICAL );
    viewTreeItems.setLayoutParams( new ViewGroup.LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT) );
    scrollView.addView( viewTreeItems );
    this.view.addView( scrollView );

    rootNode.setViewHolder( new BaseNodeViewHolder() {
      @Override
      public View createNodeView( TreeNode node, Object value ) {
        return null;
      }
      @Override
      public void updateView( View nodeView, TreeNode node ) {
        // do nothing
      }
      @Override
      public ViewGroup getNodeItemsView() {
        return viewTreeItems;
      }
    });
    expandNode( rootNode, false );
    setSelectionModeEnabled( true );
  }

  public void setDefaultAnimation( boolean defaultAnimation ) {
    this.useDefaultAnimation = defaultAnimation;
  }

  public void setUse2dScroll( boolean use2dScroll ) {
    this.use2dScroll = use2dScroll;
  }

  public boolean is2dScrollEnabled() {
    return use2dScroll;
  }

  public void setUseAutoToggle( boolean enableAutoToggle ) {
    this.enableAutoToggle = enableAutoToggle;
  }

  public boolean isAutoToggleEnabled() {
    return enableAutoToggle;
  }

  public void setDefaultViewHolder( Class<? extends BaseNodeViewHolder> viewHolder ) {
    defaultViewHolderClass = viewHolder;
  }

  public void setDefaultNodeClickListener( TreeNodeClickListener listener ) {
    nodeClickListener = listener;
  }

  public void setDefaultNodeLongClickListener( TreeNodeLongClickListener listener ) {
    nodeLongClickListener = listener;
  }

  public void expandAll() {
    expandNode( rootNode, true );
  }

  public void collapseAll() {
    for (TreeNode n : rootNode.getChildren()) {
      collapseNode( n, true );
    }
  }


  public View getView( int style ) {
    return view;
  }

  public View getView() {
    return getView( -1 );
  }


  public void expandLevel( int level ) {
    for (TreeNode n : rootNode.getChildren()) {
      expandLevel( n, level );
    }
  }

  private void expandLevel( TreeNode node, int level ) {
    if (node.getLevel() <= level) {
      expandNode( node, false );
    }
    for (TreeNode n : node.getChildren()) {
      expandLevel( n, level );
    }
  }

  public void expandNode( TreeNode node ) {
    expandNode( node, false );
  }

  public void collapseNode( TreeNode node ) {
    collapseNode( node, false );
  }

  public String getSaveState() {
    final StringBuilder builder = new StringBuilder();
    getSaveState( rootNode, builder );
    if (builder.length() > 0) {
      builder.setLength( builder.length() - 1 );
    }
    return builder.toString();
  }

  public void restoreState( String saveState ) {
    if (!TextUtils.isEmpty( saveState )) {
      collapseAll();
      final String[] openNodesArray = saveState.split( NODES_PATH_SEPARATOR );
      final Set<String> openNodes = new HashSet<>( Arrays.asList( openNodesArray ) );
      restoreNodeState( rootNode, openNodes );
    }
  }

  private void restoreNodeState( TreeNode node, Set<String> openNodes ) {
    for (TreeNode n : node.getChildren()) {
      if (openNodes.contains( n.getPath() )) {
        expandNode( n );
        restoreNodeState( n, openNodes );
      }
    }
  }

  private void getSaveState( TreeNode root, StringBuilder sBuilder ) {
    for (TreeNode node : root.getChildren()) {
      if (node.isExpanded()) {
        sBuilder.append( node.getPath() );
        sBuilder.append( NODES_PATH_SEPARATOR );
        getSaveState( node, sBuilder );
      }
    }
  }

  public void toggleNode( TreeNode node ) {
    if (node.isExpanded()) {
      collapseNode( node, false );
    }
    else {
      expandNode( node, false );
    }

  }

  private void collapseNode( TreeNode node, final boolean includeSubnodes ) {
    node.setExpanded( false );
    BaseNodeViewHolder nodeViewHolder = getViewHolderForNode( node );

    if (useDefaultAnimation) {
      collapse( nodeViewHolder.getNodeItemsView() );
    }
    else {
      nodeViewHolder.getNodeItemsView().setVisibility( View.GONE );
    }
    nodeViewHolder.toggle( false );
    if (includeSubnodes) {
      for (TreeNode n : node.getChildren()) {
        collapseNode( n, includeSubnodes );
      }
    }
  }

  private void expandNode( final TreeNode node, boolean includeSubnodes ) {
    node.getValue().loadChildren();
    node.setExpanded( true );

    final BaseNodeViewHolder parentViewHolder = getViewHolderForNode( node );
    ViewGroup childItemsView = parentViewHolder.getNodeItemsView();
    childItemsView.removeAllViews();
    parentViewHolder.toggle( true );

    for (TreeNode n : node.getChildren()) {
      addNode( childItemsView, n );

      if (n.isExpanded() || includeSubnodes) {
        expandNode( n, includeSubnodes );
      }

    }
    if (useDefaultAnimation) {
      expand( childItemsView );
    }
    else {
      childItemsView.setVisibility( View.VISIBLE );
    }

  }

  private void addNode( ViewGroup container, final TreeNode n ) {
    final BaseNodeViewHolder viewHolder = getViewHolderForNode( n );
    final View nodeView = viewHolder.getView();
    container.addView( nodeView );
    if (selectionModeEnabled) {
      viewHolder.toggleSelectionMode( selectionModeEnabled );
    }

    nodeView.setOnClickListener( new View.OnClickListener() {
      @Override
      public void onClick( View v ) {
        if (n.getClickListener() != null) {
          n.getClickListener().onClick( n, n.getValue() );
        }
        else if (nodeClickListener != null) {
          nodeClickListener.onClick( n, n.getValue() );
        }
        if (enableAutoToggle) {
          toggleNode( n );
        }
      }
    } );

    nodeView.setOnLongClickListener( new View.OnLongClickListener() {
      @Override
      public boolean onLongClick( View view ) {
        if (n.getLongClickListener() != null) {
          return n.getLongClickListener().onLongClick( n, n.getValue() );
        }
        else if (nodeLongClickListener != null) {
          return nodeLongClickListener.onLongClick( n, n.getValue() );
        }
        if (enableAutoToggle) {
          toggleNode( n );
        }
        return false;
      }
    } );
  }

  //------------------------------------------------------------
  //  Selection methods

  public void setSelectionModeEnabled( boolean selectionModeEnabled ) {
    if (!selectionModeEnabled) {
      // TODO fix double iteration over tree
      deselectAll();
    }
    this.selectionModeEnabled = selectionModeEnabled;

    for (TreeNode node : rootNode.getChildren()) {
      toggleSelectionMode( node, selectionModeEnabled );
    }

  }

  public <E> List<E> getSelectedValues( Class<E> clazz ) {
    List<E> result = new ArrayList<>();
    List<TreeNode> selected = getSelected();
    for (TreeNode n : selected) {
      TreeNodePI value = n.getValue();
      if (value != null && value.getClass().equals( clazz )) {
        result.add( (E) value );
      }
    }
    return result;
  }

  public boolean isSelectionModeEnabled() {
    return selectionModeEnabled;
  }

  private void toggleSelectionMode( TreeNode parent, boolean mSelectionModeEnabled ) {
    toogleSelectionForNode( parent, mSelectionModeEnabled );
    if (parent.isExpanded()) {
      for (TreeNode node : parent.getChildren()) {
        toggleSelectionMode( node, mSelectionModeEnabled );
      }
    }
  }

  public List<TreeNode> getSelected() {
    if (selectionModeEnabled) {
      return getSelected( rootNode );
    }
    else {
      return new ArrayList<>();
    }
  }

  // TODO Do we need to go through whole tree? Save references or consider collapsed nodes as not selected
  private List<TreeNode> getSelected( TreeNode parent ) {
    List<TreeNode> result = new ArrayList<>();
    for (TreeNode n : parent.getChildren()) {
      if (n.isSelected()) {
        result.add( n );
      }
      result.addAll( getSelected( n ) );
    }
    return result;
  }

  public void selectAll( boolean skipCollapsed ) {
    makeAllSelection( true, skipCollapsed );
  }

  public void deselectAll() {
    makeAllSelection( false, false );
  }

  private void makeAllSelection( boolean selected, boolean skipCollapsed ) {
    if (selectionModeEnabled) {
      for (TreeNode node : rootNode.getChildren()) {
        selectNode( node, selected, skipCollapsed );
      }
    }
  }

  public void selectNode( TreeNode node, boolean selected ) {
    if (selectionModeEnabled) {
      node.setSelected( selected );
      toogleSelectionForNode( node, true );
    }
  }

  private void selectNode( TreeNode parent, boolean selected, boolean skipCollapsed ) {
    parent.setSelected( selected );
    toogleSelectionForNode( parent, true );
    boolean toContinue = skipCollapsed ? parent.isExpanded() : true;
    if (toContinue) {
      for (TreeNode node : parent.getChildren()) {
        selectNode( node, selected, skipCollapsed );
      }
    }
  }

  private void toogleSelectionForNode( TreeNode node, boolean makeSelectable ) {
    BaseNodeViewHolder holder = getViewHolderForNode( node );
    if (holder.isInitialized()) {
      getViewHolderForNode( node ).toggleSelectionMode( makeSelectable );
    }
  }

  private BaseNodeViewHolder getViewHolderForNode( TreeNode node ) {
    BaseNodeViewHolder viewHolder = node.getViewHolder();
    if (viewHolder == null) {
      try {
        final Object object = defaultViewHolderClass.newInstance();
        viewHolder = (BaseNodeViewHolder) object;
        node.setViewHolder( viewHolder );
      }
      catch (Exception e) {
        throw new RuntimeException( "Could not instantiate class " + defaultViewHolderClass );
      }
    }
    if (viewHolder.getTreeView() == null) {
      viewHolder.setTreeViev( this );
    }
    return viewHolder;
  }

  private static void expand( final View v ) {
    v.measure( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
    final int targetHeight = v.getMeasuredHeight();

    v.getLayoutParams().height = 0;
    v.setVisibility( View.VISIBLE );
    Animation a = new Animation() {
      @Override
      protected void applyTransformation( float interpolatedTime, Transformation t ) {
        v.getLayoutParams().height = interpolatedTime == 1
            ? LinearLayout.LayoutParams.WRAP_CONTENT
            : (int) (targetHeight * interpolatedTime);
        v.requestLayout();
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    // 1dp/ms
    a.setDuration( (int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density) );
    v.startAnimation( a );
  }

  private static void collapse( final View v ) {
    final int initialHeight = v.getMeasuredHeight();

    Animation a = new Animation() {
      @Override
      protected void applyTransformation( float interpolatedTime, Transformation t ) {
        if (interpolatedTime == 1) {
          v.setVisibility( View.GONE );
        }
        else {
          v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
          v.requestLayout();
        }
      }

      @Override
      public boolean willChangeBounds() {
        return true;
      }
    };

    // 1dp/ms
    a.setDuration( (int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density) );
    v.startAnimation( a );
  }

  //-----------------------------------------------------------------
  //Add / Remove

  public void addNode( TreeNode parent, final TreeNode nodeToAdd ) {
    parent.addChild( nodeToAdd );
    if (parent.isExpanded()) {
      final BaseNodeViewHolder parentViewHolder = getViewHolderForNode( parent );
      addNode( parentViewHolder.getNodeItemsView(), nodeToAdd );
    }
  }

  public void removeNode( TreeNode node ) {
    if (node.getParent() != null) {
      TreeNode parent = node.getParent();
      int index = parent.deleteChild( node );
      if (parent.isExpanded() && index >= 0) {
        final BaseNodeViewHolder parentViewHolder = getViewHolderForNode( parent );
        parentViewHolder.getNodeItemsView().removeViewAt( index );
      }
    }
  }
}
