package de.pidata.system.android.tree;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public interface TreeNodeClickListener {

  void onClick( TreeNode node, Object value );
}