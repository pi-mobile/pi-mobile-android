package de.pidata.system.android.tree;

import de.pidata.gui.view.base.TreeNodePI;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public class TreeNode {
  public static final String NODES_ID_SEPARATOR = ":";

  private int id;
  private int lastId;
  private TreeNode parent;
  private boolean isSelected;
  private boolean selectable = true;
  private final List<TreeNode> children;
  private BaseNodeViewHolder mViewHolder;
  private TreeNodeClickListener clickListener;
  private TreeNodeLongClickListener longClickListener;
  private TreeNodePI value;
  private boolean expanded;

  public static TreeNode root() {
    TreeNode root = new TreeNode( null );
    root.setSelectable( false );
    return root;
  }

  private int generateId() {
    return ++lastId;
  }

  public TreeNode( TreeNodePI value ) {
    children = new ArrayList<>();
    this.value = value;
  }

  public TreeNode addChild( TreeNode childNode ) {
    childNode.parent = this;
    childNode.id = generateId();
    children.add( childNode );
    return this;
  }

  public TreeNode addChildren( TreeNode... nodes ) {
    for (TreeNode n : nodes) {
      addChild( n );
    }
    return this;
  }

  public TreeNode addChildren( Collection<TreeNode> nodes ) {
    for (TreeNode n : nodes) {
      addChild( n );
    }
    return this;
  }

  public int deleteChild( TreeNode child ) {
    for (int i = 0; i < children.size(); i++) {
      if (child.id == children.get( i ).id) {
        children.remove( i );
        return i;
      }
    }
    return -1;
  }

  public List<TreeNode> getChildren() {
    return children;
  }

  public int size() {
    return children.size();
  }

  public TreeNode getParent() {
    return parent;
  }

  public int getId() {
    return id;
  }

  public boolean isLeaf() {
    return size() == 0;
  }

  public TreeNodePI getValue() {
    return value;
  }

  public boolean isExpanded() {
    return expanded;
  }

  public TreeNode setExpanded( boolean expanded ) {
    this.expanded = expanded;
    return this;
  }

  public void setSelected( boolean selected ) {
    isSelected = selected;
  }

  public boolean isSelected() {
    return selectable && isSelected;
  }

  public void setSelectable( boolean selectable ) {
    this.selectable = selectable;
  }

  public boolean isSelectable() {
    return selectable;
  }

  public String getPath() {
    final StringBuilder path = new StringBuilder();
    TreeNode node = this;
    while (node.parent != null) {
      path.append( node.getId() );
      node = node.parent;
      if (node.parent != null) {
        path.append( NODES_ID_SEPARATOR );
      }
    }
    return path.toString();
  }


  public int getLevel() {
    int level = 0;
    TreeNode root = this;
    while (root.parent != null) {
      root = root.parent;
      level++;
    }
    return level;
  }

  public boolean isLastChild() {
    if (!isRoot()) {
      int parentSize = parent.children.size();
      if (parentSize > 0) {
        final List<TreeNode> parentChildren = parent.children;
        return parentChildren.get( parentSize - 1 ).id == id;
      }
    }
    return false;
  }

  public TreeNode setViewHolder( BaseNodeViewHolder viewHolder ) {
    mViewHolder = viewHolder;
    if (viewHolder != null) {
      viewHolder.treeNode = this;
    }
    return this;
  }

  public TreeNode setClickListener( TreeNodeClickListener listener ) {
    clickListener = listener;
    return this;
  }

  public TreeNodeClickListener getClickListener() {
    return this.clickListener;
  }

  public TreeNode setLongClickListener( TreeNodeLongClickListener listener ) {
    longClickListener = listener;
    return this;
  }

  public TreeNodeLongClickListener getLongClickListener() {
    return longClickListener;
  }

  public BaseNodeViewHolder getViewHolder() {
    return mViewHolder;
  }

  public boolean isFirstChild() {
    if (!isRoot()) {
      List<TreeNode> parentChildren = parent.children;
      return parentChildren.get( 0 ).id == id;
    }
    return false;
  }

  public boolean isRoot() {
    return parent == null;
  }

  public TreeNode getRoot() {
    TreeNode root = this;
    while (root.parent != null) {
      root = root.parent;
    }
    return root;
  }

  public void update() {
    if (mViewHolder != null) {
      mViewHolder.update();
    }
  }
}
