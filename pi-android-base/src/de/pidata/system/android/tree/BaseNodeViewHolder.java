package de.pidata.system.android.tree;

import android.view.View;
import android.view.ViewGroup;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public abstract class BaseNodeViewHolder<E> {

  protected AndroidTreeView androidTreeView;
  protected TreeNode treeNode;
  private View nodeView = null;
  private TreeNodeWrapperView nodeWrapperView;
  protected int containerStyle;

  public TreeNodeWrapperView getView() {
    if (nodeWrapperView == null) {
      final View nodeView = getNodeView();
      nodeWrapperView = new TreeNodeWrapperView( nodeView.getContext(), getContainerStyle() );
      nodeWrapperView.insertNodeView( nodeView );
    }
    return this.nodeWrapperView;
  }

  public void setTreeViev( AndroidTreeView treeViev ) {
    this.androidTreeView = treeViev;
  }

  public AndroidTreeView getTreeView() {
    return androidTreeView;
  }

  public void setContainerStyle( int style ) {
    containerStyle = style;
  }

  public View getNodeView() {
    if (nodeView == null) {
      nodeView = createNodeView( treeNode, (E) treeNode.getValue() );
    }
    return nodeView;
  }

  public ViewGroup getNodeItemsView() {
    TreeNodeWrapperView wrapperView = getView();
    return wrapperView.getNodeItemsContainer();
  }

  public boolean isInitialized() {
    return (nodeWrapperView != null);
  }

  public int getContainerStyle() {
    return containerStyle;
  }


  public abstract View createNodeView( TreeNode node, E value );

  public abstract void updateView( View nodeView, TreeNode node );

  public void toggle( boolean active ) {
    // empty
  }

  public void toggleSelectionMode( boolean editModeEnabled ) {
    // empty
  }

  public void update() {
    updateView( getNodeView(), this.treeNode );
    getView().invalidate();
  }
}