package de.pidata.system.android.tree;

import android.content.Context;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public class TreeNodeWrapperView extends LinearLayout {

  private LinearLayout nodeItemsContainer;
  private ViewGroup nodeContainer;
  private final int containerStyle;

  public TreeNodeWrapperView( Context context, int containerStyle ) {
    super( context );
    this.containerStyle = containerStyle;
    init();
  }

  private void init() {
    setOrientation( LinearLayout.VERTICAL );

    nodeContainer = new RelativeLayout( getContext() );
    nodeContainer.setLayoutParams( new LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT ) );
    nodeContainer.setPadding( 10, 5, 5, 5 );

    ContextThemeWrapper newContext = new ContextThemeWrapper( getContext(), containerStyle );
    nodeItemsContainer = new LinearLayout( newContext, null, containerStyle );
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT );
    layoutParams.setMargins( 40, 0, 0, 0 );
    nodeItemsContainer.setLayoutParams( layoutParams );
    nodeItemsContainer.setOrientation( LinearLayout.VERTICAL );
    nodeItemsContainer.setVisibility( View.GONE );

    addView( nodeContainer );
    addView( nodeItemsContainer );
  }


  public void insertNodeView( View nodeView ) {
    nodeContainer.addView( nodeView );
  }

  public ViewGroup getNodeContainer() {
    return nodeContainer;
  }

  public ViewGroup getNodeItemsContainer() {
    return nodeItemsContainer;
  }
}
