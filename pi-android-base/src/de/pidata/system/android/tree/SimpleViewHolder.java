package de.pidata.system.android.tree;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import de.pidata.gui.android.UIFactoryAndroid;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

/**
 * Based on work of Bogdan Melnychuk (Apache-2.0 License).
 * See https://github.com/bmelnychuk/AndroidTreeView
 */
public class SimpleViewHolder extends BaseNodeViewHolder<Object> {

  @Override
  public View createNodeView( TreeNode node, Object value ) {
    View nodeView;
    TreeNodePI treeNodePI = node.getValue();
    QName cellViewDefID = treeNodePI.getCellViewDefID();
    if (cellViewDefID == null) {
      TextView textView = new TextView( androidTreeView.getView().getContext() );
      nodeView = textView;
    }
    else {
      ViewGroup parentView = node.getParent().getViewHolder().getNodeItemsView();
      nodeView = ((UIFactoryAndroid) Platform.getInstance().getUiFactory()).createView( cellViewDefID, parentView, false );
    }
    nodeView.setFocusable( false );
    nodeView.setClickable( false );
    updateView( nodeView, node );
    return nodeView;
  }

  public void updateView( View nodeView, TreeNode node ) {
    Model nodeModel = node.getValue().getNodeModel();
    if (nodeView instanceof ViewGroup) {
      ViewGroup group = (ViewGroup) nodeView;
      for (int i = 0; i < group.getChildCount(); i++) {
        updateView( group.getChildAt( i ), node );
      }
    }
    else if (nodeView instanceof TextView) {
      QName valueID = node.getValue().getDisplayValueID();
      Object value;
      if (valueID == null) {
        value = nodeModel.toString();
      }
      else {
        value = nodeModel.get( valueID );
      }
      String cellString = node.getValue().getTreeViewPI().render( value, valueID );
      ((TextView) nodeView).setText( cellString );
    }
  }


  @Override
  public void toggle( boolean active ) {

  }
}
