package de.pidata.system.android;


import de.pidata.log.Logger;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.io.FilenameFilter;

public class AndroidPackages {

  public static final String PROP_SOFTWARE_DOWNLOAD_DIR = "softwareDownloadDir";
  public static final String PROP_UPDATE_TO_VERSION = "updateToVersion";
  public static final String SUFFIX = ".apk";
  public static final String PLATFORM_NAME = "AndroidPlatform";

  public static final String PROP_APK_FILENAME = "android.apk_filename";
  public static final String PROP_DOWNLOAD_URL = "android.download_url";

  private static String getApkFileName() {
    return SystemManager.getInstance().getProperty(PROP_APK_FILENAME, "dummy");
  }

  private static String[] getApkFiles( File dir ) {
    String[] files = dir.list( new FilenameFilter() {
      public boolean accept( File dir, String filename ) {
        return (filename.endsWith( SUFFIX ) && filename.startsWith( getApkFileName() ));
      }
    } );
    if (files == null) {
      return new String[0];
    }
    else {
      return files;
    }
  }

  public static String getDownloadPath( int version ) {
    String filename = getApkFileName() + "v" + version + SUFFIX;
    return SystemManager.getInstance().getProperty( PROP_DOWNLOAD_URL, "" ) + "/" + filename;
  }

  /**
   * Returns version number for given filename or -1 if filename does not end with SUFFIX
   * or does not contain a valid version number.
   * @param filename the filename to extract version number from
   * @return the version number or -1
   */
  private static int getVersionNumber( String filename ) {
    int suffixPos = filename.indexOf( SUFFIX );
    if (suffixPos > 0) {
      try {
        int startPosVersion = getApkFileName().length()+1;
        int endPosVersion = filename.indexOf( '-', startPosVersion );
        if (endPosVersion < 0) endPosVersion = suffixPos;
        String version = filename.substring( startPosVersion, endPosVersion );
        return Integer.parseInt( version );
      }
      catch (Exception ex) {
        Logger.warn( "Cannot extract version number from filename=" + filename );
      }
    }
    return -1;
  }

  public static int check4Update( File dir, int currentVersionNum ) {
    int maxVersionNum = currentVersionNum;
    if (dir.exists()) {
      String[] files = getApkFiles( dir );
      for (int i = 0; i < files.length; i++) {
        int versionNum = getVersionNumber( files[i] );
        if (versionNum > maxVersionNum) {
          maxVersionNum = versionNum;
        }
      }
    }
    if (maxVersionNum > currentVersionNum ) return maxVersionNum;
    else return 0;
  }

  /**
   * Return the first file matching package name and version.
   * @param downloadDir the folder containing downloaded apk files
   * @param version     the required version
   * @return the matching file name
   */
  public static String getDownloadedFilename( File downloadDir, int version ) {
    String[] files = getApkFiles( downloadDir );
    for (int i = 0; i < files.length; i++) {
      int versionNum = getVersionNumber( files[i] );
      if (versionNum == version) {
        return files[i];
      }
    }
    return null;
  }

  public static void cleanup( File downloadDir, int currentVersionNum ) {
    String[] files = getApkFiles( downloadDir );
    for (int i = 0; i < files.length; i++) {
      int versionNum = getVersionNumber( files[i] );
      if ((versionNum >= 0) && (versionNum < currentVersionNum)) {
        File file = new File( downloadDir, files[i] );
        file.delete();
      }
    }
  }
}
