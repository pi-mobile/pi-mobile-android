/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.models.tree.Context;
import de.pidata.connect.base.ConnectionController;
import de.pidata.system.base.SystemManager;

import java.util.UUID;

public class AndroidConnectionController implements ConnectionController {

  private boolean connected = false;
  private String clientID = null;

  @TargetApi(Build.VERSION_CODES.M)
  public static String deviceID( ContentResolver contentResolver, Activity currentActivity ) {

    if (currentActivity.checkSelfPermission( Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
      // Permission is not granted
      currentActivity.requestPermissions( new String[]{Manifest.permission.READ_PHONE_STATE}, 1 );
    }
    try {
      //--- Returns the unique device ID, for example, the IMEI for GSM and the MEID or ESN for CDMA phones.
      TelephonyManager tm = (TelephonyManager) currentActivity.getSystemService( android.content.Context.TELEPHONY_SERVICE );
      String tmDevice = "" + tm.getDeviceId();

      //String tmSerial = "" + tm.getSimSerialNumber(); Should not be used - is null in flight mode

      //--- A 64-bit number (as a hex string) that is randomly generated on the device's first boot and should remain constant for the lifetime of the device.
      String androidId = "" + android.provider.Settings.Secure.getString( contentResolver, android.provider.Settings.Secure.ANDROID_ID );

      //UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());

      UUID deviceUuid = new UUID( androidId.hashCode(), tmDevice.hashCode() );
      return deviceUuid.toString();
    }
    catch (Exception ex) {
      return null;
    }
  }

  public AndroidConnectionController( Context baseContext ) {
    connect();
  }

  public String connect() {
    if (!connected) {
      Activity currentActivity = AndroidApplication.getInstance().getCurrentActivity();
      //String androidID = Settings.Secure.getString( androidContext.getContentResolver(), Settings.Secure.ANDROID_ID );
      String androidID = deviceID( currentActivity.getContentResolver(), currentActivity );
      if (androidID != null) {
        connected = true;
        clientID = "And-" + androidID;
        SystemManager.getInstance().setClientID( clientID );
      }
    }
    return this.clientID;
  }

  public void disconnect() {
  }

  public boolean isConnected() {
    return connected;
  }
}
