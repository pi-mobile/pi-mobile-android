/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import de.pidata.system.base.Storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class AssetStorage extends Storage {

  private AssetManager assetMgr;

  public AssetStorage( AssetManager assetMgr, String name ) {
    super( name );
    this.assetMgr = assetMgr;
  }

  public AssetManager getAssetMgr() {
    return assetMgr;
  }

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage
   * @return the path for fileName
   */
  @Override
  public String getPath( String fileName ) {
    String path = getName();
    if (fileName == null) {
      if ((path == null) || (path.length() == 0)) {
        fileName = ".";
      }
      else {
        fileName = path;
      }
    }
    else if ((path != null) && (path.length() > 0)) {
      if (path.endsWith( "/" ) || path.endsWith("\\")) {
        fileName = path + fileName;
      }
      else {
        fileName = path + "/" + fileName;
      }
    }
    return fileName;
  }

  /**
   * Returns an input stream to source within this storage
   *
   * @param source the name source name within this storage
   * @return an input stream to source within this storage
   * @throws java.io.IOException if source does not exist or cannot be accessed
   */
  public InputStream read( String source ) throws IOException {
    return assetMgr.open( getPath( source ) );
  }

  /**
   * Returns true if source exists.
   *
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  public boolean exists( String source ) {
    if (source == null) {
      try {
        list();
        return true;
      }
      catch (IOException ex) {
        return false;
      }
    }
    try {
      InputStream in = read( source );
      in.close();
      return true;
    }
    catch (IOException e) {
      return false;
    }
  }

  /**
   * Returns size of source in bytes.
   *
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  public long size( String source ) throws IOException {
    AssetFileDescriptor fd = assetMgr.openFd( getPath( source ) );
    long len = fd.getLength();
    fd.close();
    return len;
  }

  /**
   * Returns true if source is a directory
   *
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  public boolean isDirectory( String source ) throws IOException {
    throw new RuntimeException( "ToDo" );
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  @Override
  public Storage getSubDirectory( String childName ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  @Override
  public Storage createSubDirectory( String childName ) throws IOException {
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns last modification timestamp of source in millis.
   *
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  public long lastModified( String source ) throws IOException {
    throw new RuntimeException( "ToDo" );
  }

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append
   * @return an input stream to source within this storage
   * @throws java.io.IOException if destination cannot be written
   */
  public OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException {
    throw new IllegalArgumentException( "Storage is read only" );
  }

  /**
   * Returns an array representing the directory of this storage (e.g. file names)
   *
   * @return a unordered array of Strings, if sourcePath is not a directoy or empty an array of size 0 is returned
   */
  public String[] list() throws IOException {
    return assetMgr.list( getPath(null) );
  }

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   *
   * @param resource the name of the resxource to be deleted
   * @throws java.io.IOException if resource could not be deleted
   */
  public void delete( String resource ) throws IOException {
    throw new IllegalArgumentException( "Storage is read only" );
  }

  /**
   * Renames filename to newName
   *
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws java.io.IOException if filename could not be renamed
   */
  public void rename( String filename, String newName ) throws IOException {
    throw new IllegalArgumentException( "Storage is read only" );
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  @Override
  public void move( String fileName, Storage destStorage, String newName ) throws IOException {
    throw new IllegalArgumentException( "Storage is read only" );
  }
}
