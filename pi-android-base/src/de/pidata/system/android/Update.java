/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.controller.base.ButtonController;
import de.pidata.gui.controller.base.MutableControllerGroup;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.service.base.ServiceException;
import de.pidata.qnames.QName;
import de.pidata.system.base.SystemManager;

import java.io.File;
import java.io.IOException;

/**
 1) Immer wenn das Mobilgerät eine Bestellung abliefert oder einen Auftrag holt überträgt es die aktuelle
    Programmversion. Findet der Mobilserver im Download-Pfad (Property "android.download_url") eine apk-Datei mit
    neuerer Version, so schickt er dem Client eine Update-Nachricht.

 2) Wenn der Client eine Update-Nachricht empfängt prüft er noch mal ob die in der Nachricht angegebene Version
    tatsächlich neuer ist als seine eigene trägt er in der application.properties diese Version ein.

 3) Immer wenn die Login-Maske einen Refresh erhält prüft sie, ob im download-Verzeichnis eine apk-Datei mit neuerer
    Version liegt, als die aktuelle. Ist diese vorhanden und nicht kleiner als die in den application.properties
    eingetragene Version, so erscheint der Button "Installiere vXX". Ansonsten erscheint "Download vXX", falls die
    Version aus application.properties neuer ist als die eigene.

 Nun noch ein paar Hinweise:
 - Beim Wechsel des Systems (test/prod) wird die application.properties nicht gelöscht. Sie wird nur gelöscht, wenn
   die Android-Applikation deinistalliert wird.
 - Der Download-Ordner wird nur beim Klick auf "Download vXX" bereinigt.
 - Der Install-Button erscheint auch, wenn man manuell eine Version ins download-Verzeichnis des Android-Geräts
   kopiert hat.
 */
public class Update extends GuiOperation {

  private ButtonController updateCtrl;
  private int state = 0;

  public void configure( ButtonController updateCtrl ) {
    this.updateCtrl = updateCtrl;
    int updateToVersion = getUpdateToVersion();
    int currentVersion = Integer.parseInt( SystemManager.getInstance().getProgramVersion().substring( 1 ) );
    int maxAvailableVersion = AndroidPackages.check4Update( getDownloadDir(), currentVersion );
    if ((maxAvailableVersion >= currentVersion) && (maxAvailableVersion >= updateToVersion)) {
      updateCtrl.setVisible( true );
      updateCtrl.setValue( "Installiere v"+maxAvailableVersion );
      this.state = 2;
    }
    else if (updateToVersion > 0) {
      if (currentVersion >= updateToVersion) {
        updateToVersion = 0;
      }
      else {
        updateCtrl.setVisible( true );
        updateCtrl.setValue( "Download v" + updateToVersion );
        this.state = 1;
      }
    }
    else {
      updateCtrl.setVisible( false );
      this.state = 0;
    }
  }

  private int getUpdateToVersion() {
    int updateToVersion = 0;
    String temp = SystemManager.getInstance().getProperty( AndroidPackages.PROP_UPDATE_TO_VERSION, null );
    if (temp != null) {
      try {
        updateToVersion = Integer.parseInt( temp.substring( 1 ) );
      }
      catch (Exception ex) {
        Logger.warn( "Error reading android update property, value="+temp+", ex="+ex );
        resetUpdateProperty();
      }
    }
    return updateToVersion;
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    int currentVersionNum = Integer.parseInt( SystemManager.getInstance().getProgramVersion().substring( 1 ) );
    if (state == 1) {
      int updateToVersion = getUpdateToVersion();
      if (updateToVersion > 0) {
        AndroidPackages.cleanup( getDownloadDir(), currentVersionNum );
        AndroidPackages.cleanup( getDownloadDir(), updateToVersion );
        download( updateToVersion );
        resetUpdateProperty();
      }
    }
    else if (state == 2) {
      File downloadDir = getDownloadDir();
      int newVersion = AndroidPackages.check4Update( downloadDir, currentVersionNum );
      if (newVersion > 0) {
        String updateFile = AndroidPackages.getDownloadedFilename( downloadDir, newVersion );
        install( downloadDir, updateFile );
      }
    }
    updateCtrl.setVisible( false );
    state = 0;
  }

  private void resetUpdateProperty() {
    try {
      SystemManager.getInstance().setProperty( AndroidPackages.PROP_UPDATE_TO_VERSION, "v0", true );
    }
    catch (IOException ex) {
      Logger.error( "Error setting android update property " + SystemManager.getInstance(), ex );
    }
  }

  private static File getDownloadDir() {
    return new File( Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() );
  }

  private void download( int version ) {
    SystemManager sysMan = SystemManager.getInstance();
    String protocol = sysMan.getProperty( "protocol", "http" );
    String host = sysMan.getProperty( "serverName", null );
    String port = sysMan.getProperty( "port", "80" );
    String urlStr = protocol + "://" + host + ":" + port + AndroidPackages.getDownloadPath( version );
    Uri myUrl = Uri.parse( urlStr );
    Intent update1 = new Intent( Intent.ACTION_VIEW ).setData( myUrl );
    update1.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
    AndroidApplication.getInstance().startActivity( update1 );
  }

  private void install( File dir, String updateFilename ) {
    File apkFile = new File( dir, updateFilename );
    Intent intent = new Intent( Intent.ACTION_VIEW );
    intent.setDataAndType( Uri.fromFile( apkFile ), "application/vnd.android.package-archive" );
    intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
    AndroidApplication.getInstance().startActivity( intent );
  }
}
