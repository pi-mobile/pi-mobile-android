/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.os.Environment;
import android.util.Log;
import de.pidata.log.Level;
import de.pidata.log.LoggerInterface;
import de.pidata.models.types.simple.DateObject;
import de.pidata.models.types.simple.DateTimeType;
import de.pidata.system.base.Storage;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by dsc on 13.07.17.
 */
public class AndroidLogger implements LoggerInterface {

  private String programmName;
  private String logfilePrefix;
  private int expireDays;
  private Level logLevel = Level.INFO;
  private Storage logStorage;
  private PrintStream logfile = null;
  private long logDateMillis;

  public AndroidLogger( String programmName, String logfilePrefix, int expireDays, Level logLevel ) {
    this.logfilePrefix = logfilePrefix;
    this.programmName = programmName;
    this.expireDays = expireDays;
    this.logLevel = logLevel;

    openLogFile();
  }

  @Override
  public void close() {
    if (logfile != null) {
      logfile.flush();
      logfile.close();
      logfile = null;
    }
  }

  private void openLogFile() {

    DateObject today = new DateObject( DateTimeType.TYPE_DATE );
    long expiredDate = DateObject.subtractDays( today.getDate(), expireDays );
    if (logStorage == null) {
      logStorage = new ExternalFileStorage( Environment.DIRECTORY_DOWNLOADS, "logs" );
    }

    //--- Close current log file
    close();

    //--- remove old logfiles
    String[] logfileList = null;
    try {
      logfileList = logStorage.list();
    }
    catch (Exception ex) {
      Log.e( programmName, "Error listing log file storage=" + logStorage, ex );
    }
    for (String fileName : logfileList) {
      if (fileName.startsWith( logfilePrefix )) {
        try {
          int pos = fileName.lastIndexOf( '_' );
          int pos2 = fileName.lastIndexOf( '.' );
          if ((pos > 0) && pos2 > 0) {
            DateObject logDate = new DateObject( DateTimeType.TYPE_DATE, fileName.substring( pos + 1, pos2 ) );
            if (logDate.getDate() < expiredDate) {
              logStorage.delete( fileName );
            }
          }
        }
        catch (Exception ex) {
          Log.e( programmName, "Error removing expired log name=" + fileName, ex );
        }
      }
    }

    //--- Open or create new log file
    String dateStr = DateTimeType.toDateString( today );
    String logfileName = logfilePrefix + "_" + dateStr + ".log";
    try {
      Log.i( programmName, "logfile=" + logfileName );
      OutputStream logFileStream = logStorage.write( logfileName, false, true );
      this.logfile = new PrintStream( logFileStream );
      this.logDateMillis = today.getDate();
    }
    catch (Exception e) {
      Log.e( programmName, "Error opening log file name=" + logfileName, e );
    }
  }

  @Override
  public void log( Level logLevel, String message, Throwable cause ) {
    if (logLevel.getLevelValue() >= getLogLevel().getLevelValue()) {
      String logTag = programmName;
      switch (logLevel) {
        case FATAL:
          Log.e( logTag, "FATAL: " + message, cause );
          break;
        case ERROR:
          Log.e( logTag, message, cause );
          break;
        case WARN:
          Log.w( logTag, message, cause );
          break;
        case INFO:
          Log.i( logTag, message, cause );
          break;
        case DEBUG:
          Log.d( logTag, message, cause );
          break;
      }
      if (logfile != null) {
        if ((System.currentTimeMillis() - DateTimeType.ONE_DAY_MILLIS) > logDateMillis) {
          openLogFile();
        }
        String timeStr = getTimeString();
        if (cause == null) {
          this.logfile.println( timeStr + " " + message );
        }
        else {
          this.logfile.println( timeStr + " " + message + " Cause: " + cause );
          cause.printStackTrace( logfile );
        }
        this.logfile.flush();
      }
    }
  }

  private String getTimeString() {
    long time = System.currentTimeMillis();
    DateObject date = new DateObject( DateTimeType.TYPE_DATETIME, time );
    return DateTimeType.toDateTimeString( date, true );
  }

  @Override
  public void setLogLevel( Level loglevel ) {
    this.logLevel = loglevel;
  }

  @Override
  public Level getLogLevel() {
    return logLevel;
  }

}
