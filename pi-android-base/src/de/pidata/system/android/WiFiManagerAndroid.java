package de.pidata.system.android;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.system.base.WifiConnectionInfo;
import de.pidata.system.base.WifiConnectionState;
import de.pidata.system.base.WifiManager;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class WiFiManagerAndroid extends BroadcastReceiver implements WifiManager {

  private boolean scanning = false;
  private ArrayList<String> networkList = new ArrayList<String>();

  public WiFiManagerAndroid() {
    checkRights();
  }

  private void checkRights() {
    final AndroidApplication app = AndroidApplication.getInstance();

    if ((ContextCompat.checkSelfPermission( app, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        || (ContextCompat.checkSelfPermission(app, Manifest.permission.CHANGE_WIFI_STATE) != PackageManager.PERMISSION_GRANTED)) {
      //Request permission
      ActivityCompat.requestPermissions( app.getCurrentActivity(),
          new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION,
              Manifest.permission.ACCESS_FINE_LOCATION,
              Manifest.permission.ACCESS_WIFI_STATE,
              Manifest.permission.CHANGE_WIFI_STATE,
              Manifest.permission.ACCESS_NETWORK_STATE },
          123 );
    }

  }

  public boolean checkConnectionState() {
    final AndroidApplication app = AndroidApplication.getInstance();
    LocationManager lm = (LocationManager) app.getSystemService(Context.LOCATION_SERVICE);
    boolean network_enabled = false;

    try {
      network_enabled = lm.isProviderEnabled( LocationManager.NETWORK_PROVIDER );
    }
    catch (Exception ex) {
      // do nothing
    }

    return network_enabled;
  }

  public void enableWifi() {
    final AndroidApplication app = AndroidApplication.getInstance();
    final Intent myIntent = new Intent( Settings.ACTION_WIRELESS_SETTINGS );
    app.getCurrentActivity().startActivity( myIntent );
  }

  @Override
  public List<String> scanWiFi() {
    try {
      AndroidApplication app = AndroidApplication.getInstance();
      Context appContext = app.getApplicationContext();
      android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager) appContext.getSystemService( AndroidApplication.WIFI_SERVICE );
      if (!scanning) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction( android.net.wifi.WifiManager.SCAN_RESULTS_AVAILABLE_ACTION );
        appContext.registerReceiver( this, intentFilter );
        scanning = true;
      }
      wifiManager.startScan();
    }
    catch (Exception e) {
      Logger.error( "Error scanning WiFi", e );
    }
    return networkList;
  }

  public void onReceive( Context context, Intent intent ) {
    boolean success = intent.getBooleanExtra( android.net.wifi.WifiManager.EXTRA_RESULTS_UPDATED, false );
    if (success) { // seem to be FALSE if no updates received
      AndroidApplication app = AndroidApplication.getInstance();
      Context appContext = app.getApplicationContext();
      android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager) appContext.getSystemService( AndroidApplication.WIFI_SERVICE );
      List<ScanResult> scanResultList = wifiManager.getScanResults();
      networkList.clear();
      for (ScanResult scanResult : scanResultList) {
        if (!networkList.contains( scanResult.SSID )) {
          networkList.add( scanResult.SSID );
        }
      }
    }
  }

  @Override
  public String setActiveWifi( String ssid ) {
    try {
      AndroidApplication app = AndroidApplication.getInstance();
      android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager) app.getApplicationContext().getSystemService( AndroidApplication.WIFI_SERVICE );

      List<WifiConfiguration> confList = wifiManager.getConfiguredNetworks();
      for (WifiConfiguration conf : confList) {
        if (conf.SSID != null && conf.SSID.equals( "\"" + ssid + "\"" )) {
          doConnect( app, wifiManager, conf );
          return null;
        }
      }
      WifiConfiguration conf = new WifiConfiguration();
      conf.SSID = "\"" + ssid + "\"";   // Please note the quotes. String should contain ssid in quotes
      conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
      wifiManager.addNetwork( conf );
      return doConnect( app, wifiManager, conf );
    }
    catch (Exception ex) {
      Logger.error( "Error activating WiFi", ex );
      return "ERROR: " + ex.getMessage();
    }
  }

  private String doConnect( AndroidApplication app, android.net.wifi.WifiManager wifiManager, WifiConfiguration conf ) throws Exception {
    wifiManager.disconnect();
    wifiManager.enableNetwork( conf.networkId, true );
    if (wifiManager.reconnect()) {
      ConnectivityManager connManager = (ConnectivityManager) app.getSystemService( Context.CONNECTIVITY_SERVICE );
      NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
      int count = 10;
      while ((networkInfo == null) || !networkInfo.isConnected()) {
        Thread.sleep( 1000 );
        count--;
        if (count <= 0) {
          return "ERROR: Timeout connecting to ssid='"+conf.SSID+"'";
        }
        networkInfo = connManager.getActiveNetworkInfo();
      }
      return null;
    }
    else {
      return "ERROR: Could not connect to ssid='"+conf.SSID+"'";
    }
  }

  public WifiConnectionInfo getWiFiConnectionInfo() throws IOException {
    try {
      AndroidApplication app = AndroidApplication.getInstance();
      android.net.wifi.WifiManager wifiManager = (android.net.wifi.WifiManager) app.getApplicationContext().getSystemService( AndroidApplication.WIFI_SERVICE );

      if (wifiManager.isWifiEnabled()) {
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return new WifiConnectionInfo( WifiConnectionState.CONNECTED, wifiInfo.getSSID().replace( "\"", "" ), wifiInfo.getNetworkId(), wifiInfo.getLinkSpeed(), wifiInfo.getIpAddress(), wifiInfo.getMacAddress(), wifiInfo.getRssi() );
      }
      else {
        return new WifiConnectionInfo( WifiConnectionState.DISABLED, null, 0, 0, 0, null, 0 );
      }

    }
    catch (Exception e) {
      throw new IOException( "WiFi connection could not be initialised." );
    }
  }

  public URLConnection openWiFiConnection( URL url ) throws IOException {
    ConnectivityManager connMgr = (ConnectivityManager) AndroidApplication.getInstance().getSystemService( android.content.Context.CONNECTIVITY_SERVICE );
    for (Network network : connMgr.getAllNetworks()) {
      NetworkInfo networkInfo = connMgr.getNetworkInfo( network );
      if (networkInfo.isConnectedOrConnecting() && networkInfo.getType() == ConnectivityManager.TYPE_VPN) {
        return url.openConnection();
      }
    }
    for (Network network : connMgr.getAllNetworks()) {
      NetworkInfo networkInfo = connMgr.getNetworkInfo( network );
      if (networkInfo.isConnectedOrConnecting() && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
        return network.openConnection( url );
      }
    }
    return url.openConnection();
  }
}
