/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.SharedPreferences;
import de.pidata.settings.Settings;

public class AndroidSettings implements Settings {

  private SharedPreferences preferences;

  public AndroidSettings( SharedPreferences preferences ) {
    this.preferences = preferences;
  }

  @Override
  public String getString( String key, String defaultValue ) {
    return preferences.getString( key, defaultValue );
  }

  @Override
  public boolean getBoolean( String key, boolean defaultValue ) {
    return preferences.getBoolean( key, defaultValue );
  }

  @Override
  public int getInt( String key, int defaultValue ) {
    return preferences.getInt( key, defaultValue );
  }

  @Override
  public double getDouble( String key, double defaultValue ) {
    return (double) preferences.getFloat( key, (float) defaultValue );
  }

  @Override
  public void setString( String key, String value ) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putString( key, value );
    editor.apply();
  }

  @Override
  public void setBoolean( String key, boolean value ) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putBoolean( key, value );
    editor.apply();
  }

  @Override
  public void setInt( String key, int value ) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putInt( key, value );
    editor.apply();
  }

  @Override
  public void setDouble( String key, double value ) {
    SharedPreferences.Editor editor = preferences.edit();
    editor.putFloat( key, (float) value );
    editor.apply();
  }
}
