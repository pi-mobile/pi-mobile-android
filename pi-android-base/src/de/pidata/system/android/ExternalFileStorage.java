/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.Context;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.system.base.Storage;
import de.pidata.system.filebased.AbstractFileStorage;

import java.io.File;
import java.io.IOException;

/**
 * Created by pru on 30.01.15.
 */
public class ExternalFileStorage extends AbstractFileStorage {

  private String storageName;

  public ExternalFileStorage( String storageName, String path ) {
    super( storageName, path, null );
    this.storageName = storageName;
  }

  /**
   * Returns the File object representing fileName
   *
   * @param fileName the file name within this storage
   * @return the File object representing fileName
   */
  @Override
  protected File getFile( String fileName ) {
    if (fileName == null) {
      return getStorageFile();
    }
    else {
      return new File( getStorageFile(), fileName );
    }
  }

  /**
   * Return file object for this storage self
   *
   * @return dir file object
   */
  @Override
  protected File getStorageFile() {
    Context androidContext = AndroidApplication.getInstance();
    if (path == null) {
      return androidContext.getExternalFilesDir( storageName );
    }
    else {
      return new File( androidContext.getExternalFilesDir( storageName ), path );
    }
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  @Override
  public Storage getSubDirectory( String childName ) throws IOException {
    File testFile = getFile(childName);
    if (!testFile.exists()) {
      testFile.mkdirs();
    }
    if (testFile.isDirectory()) {
      return new ExternalFileStorage( storageName, getChildPath( childName ) );
    }
    else {
      throw new IOException( "File is not a directory, name=" + childName );
    }
  }
}
