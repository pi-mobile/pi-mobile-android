/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.res.AssetManager;
import de.pidata.system.base.Storage;
import de.pidata.system.filebased.AbstractFileStorage;

import java.io.File;
import java.io.IOException;

public class FileStorage extends AbstractFileStorage {

  public FileStorage( AssetManager assetMgr, String storageName, String basePath ) {
    super( storageName, getStoragePath( storageName, basePath ), new AssetStorage( assetMgr, storageName ) );
  }

  private FileStorage( String name, String path, AssetStorage assetStorage ) {
    super( name, path,  assetStorage );
  }

  public AssetManager getAssetMgr() {
    return ((AssetStorage) getAltReadOnlyStorage()).getAssetMgr();
  }

  public static String getStoragePath( String storageName, String basePath ) {
    String path;
    if ((basePath == null) || (basePath.length() == 0)) {
      if ((storageName == null) || (storageName.length() == 0)) {
        path = null;
      }
      else {
        path = storageName;
      }
    }
    else {
      if ((storageName == null) || (storageName.length() == 0)) {
        path = basePath;
      }
      else {
        // support absolute paths (starting with /)
        if (storageName.charAt(0) == '/') {
          path = storageName;
        }
        else {
          if (basePath.equals(".")) path = storageName;
          else path = basePath + "/" + storageName;
        }
      }
    }
    return path;
  }

  /**
   * Returns the File object representing fileName
   * @param fileName the file name within this storage
   * @return the File object representing fileName
   */
  protected File getFile(String fileName) {
    if (fileName == null) {
      if ((path == null) || (path.length() == 0)) {
        fileName = ".";
      }
      else {
        fileName = path;
      }
    }
    else if ((path != null) && (path.length() > 0)) {
      if (path.endsWith( "/" ) || path.endsWith("\\")) {
        fileName = path + fileName;
      }
      else {
        fileName = path + "/" + fileName;
      }
    }
    return new File(fileName);
  }

  /**
   * Return file object for this storage self
   *
   * @return dir file object
   */
  @Override
  protected File getStorageFile() {
    return new File( path );
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  @Override
  public Storage getSubDirectory( String childName ) throws IOException {
    File testFile = getFile(childName);
    if (testFile.isDirectory()) {
      return new FileStorage( childName, testFile.getPath(), new AssetStorage( getAssetMgr(), childName )  );
    }
    else {
      throw new IOException( "File is not a directory, name="+childName );
    }
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  @Override
  public Storage createSubDirectory( String childName ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
