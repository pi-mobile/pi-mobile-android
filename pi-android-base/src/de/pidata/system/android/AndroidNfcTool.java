package de.pidata.system.android;

import de.pidata.connect.nfc.NfcScanListener;
import de.pidata.gui.android.activity.ReadTagActivity;
import de.pidata.gui.android.controller.AndroidDialog;
import de.pidata.gui.component.base.NfcTool;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.DialogController;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ParameterList;
import de.pidata.service.base.ServiceException;

public class AndroidNfcTool extends GuiOperation implements NfcTool {

  private NfcScanListener listener;

  private DialogController dialogController;

  @Override
  public void scanNFCTag( NfcScanListener nfcScanListener ) {
    dialogController = AndroidApplication.getInstance().getCurrentActivity().getDialogController();
    dialogController.subAction( this, ControllerBuilder.NAMESPACE.getQName( "scan_nfc" ), dialogController, null );
    this.listener = nfcScanListener;
  }

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    AndroidDialog dialog = (AndroidDialog) dialogController.getDialogComp();
    dialog.showNfcTool();
  }

  /**
   * Called after a dialog has been closed that was requested to open within this guiOperation.
   * <p>
   * IMPORTANT: Do not use member variables except for messageBox handles. On some platforms (e.g. Android)
   * the system may reload the whole dialog between calling a child dialog and returning from that!
   *
   * @param parentDlgCtrl parent of the closed dialog
   * @param resultOK      if true dialog was closed with ok, false if closed with cancel
   * @param resultList    result parameter list, never null
   */
  @Override
  public void dialogClosed( DialogController parentDlgCtrl, boolean resultOK, ParameterList resultList ) throws Exception {
    if (resultOK) {
      QName paramID = ControllerBuilder.NAMESPACE.getQName( ReadTagActivity.TAG_ID );
      String serNr = resultList.getString( paramID );
      serNr = serNr.replace( ":", "" );
      Logger.info( "Scanned NFC Tag ID="+serNr );
      listener.scannedSerialNumber( serNr );
    }
  }
}
