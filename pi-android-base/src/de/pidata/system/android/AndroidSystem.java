/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.android;

import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import de.pidata.gui.platform.android.AndroidApplication;
import de.pidata.log.FileOwner;
import de.pidata.log.Level;
import de.pidata.log.Logger;

import de.pidata.qnames.Namespace;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.base.WifiManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

public class AndroidSystem extends SystemManager {

  private static final boolean DEBUG = false;
  private Properties applicationProps;
  private Properties  systemProps;
  private WiFiManagerAndroid wiFiManager = null;

  public static final Namespace NAMESPACE_ANDROID = Namespace.getInstance( "android" );

  public AndroidSystem( String basePath, FileStorage rootStorage, String programName, String programVersion, String programDate ) {
    super( basePath, rootStorage, programName, programVersion, programDate );
    initProperties();
    initLogging();
    loadFactories();
    Logger.debug( "factories loaded");
  }

  public Storage getStorage( String storageName ) {
    if (storageName == null) {
      return this.systemStorage;
    }
    else if (storageName.startsWith( STORAGE_PREFIX_PI )) {
      int pos = storageName.indexOf( ':' );
      String storageType = storageName.substring( 0, pos );
      String storagePath = storageName.substring( pos+1 );
      return getStorage( storageType, storagePath );
    }
    else if (storageName.startsWith( "@" )) {
      throw new IllegalArgumentException( "Resources not supported as storage" );
    }
    else {
      return getStorage( STORAGE_CLASSPATH, storageName );
    }
  }

  public Storage getStorage( String storageType, String storagePath ) {
    if (storageType.equals( STORAGE_CLASSPATH )) {
      return new FileStorage( ((FileStorage) systemStorage).getAssetMgr(), storagePath, basePath );
    }
    else if (storageType.equals( STORAGE_PRIVATE_PICTURES )) {
      return new ExternalFileStorage( Environment.DIRECTORY_PICTURES, storagePath );
    }
    else if (storageType.equals( STORAGE_PRIVATE_DOWNLOADS )) {
      return new ExternalFileStorage( Environment.DIRECTORY_DOWNLOADS, storagePath );
    }
    else {
      throw new IllegalArgumentException( "Unknown storage type="+storageType );
    }
  }

  public void initProperties() {
    if (systemProps == null) {
      systemProps = new Properties();
      InputStream in = null;
      try {
        Logger.debug( "loading system properties: '" + SYSTEM_PROPFILE + "'" );
        in = systemStorage.read( SYSTEM_PROPFILE );
        systemProps.load( in );
        in.close();
      }
      catch (IOException e) {
        Logger.warn( "Could not load system properties." );
      }
      finally {
        StreamHelper.close( in );
      }
    }

    // set system base path
    // TODO basePath kommt eigentlich über den Konstruktor und wird vorher schon verwendet. Derzeit braucht aber
    // mindestens der Server folgendes Property!
    String path = systemProps.getProperty( KEY_BASEPATH );
    if (path != null) {
      this.basePath = path;
    }
    else if (basePath == null) {
      basePath = ".";
    }
    Logger.debug( "basePath: '" + basePath + "'" );

    if (applicationProps == null) {
      loadApplicationProperties();
    }

    Logger.debug( "starting system...");
  }

  private void initLogging() {
    String programName = getProgramName();

    String logfilePrefix = getProperty( SystemManager.KEY_LOGFILE, null );
    if ((logfilePrefix == null) || (logfilePrefix.length() == 0)) {
      Log.i( programName, "logfile not configured" );
      return;
    }

    int expireDays = getPropertyInt( KEY_LOGFILE_EXPIREDAYS, 10 );

    Level logLevel = Level.INFO;
    String logLevelProp = getProperty( KEY_LOGLEVEL, null );
    if (logLevelProp != null) {
      System.out.print( "loglevel=" + logLevelProp );
      logLevel = Level.fromName( logLevelProp );
      System.out.println( " (" + logLevel + ")" );
    }

    Logger.setLogger( new AndroidLogger( programName, logfilePrefix, expireDays, logLevel ) );
  }

  private void loadApplicationProperties() {
    applicationProps = new Properties();

    // TODO: support database properties

    String propfileName = getSystemProperty( KEY_APPLICATION_PROPFILE );
    if (propfileName == null) {
      propfileName = APPLICATION_PROPFILE;
    }

    if (systemStorage.exists( propfileName )) {
      InputStream in = null;
      try {
         in = systemStorage.read( propfileName );
        applicationProps.load( in );
        in.close();
      }
      catch (IOException e) {
        // nothing to to be done.
        // applicationProps file could not be read.
        // we simply need to initialize empty properties.
      }
      finally {
        StreamHelper.close( in );
      }
    }
  }

  /**
   * Retrieve a system wide property. Properties are not bound to the lifetime
   * of one instance. The implementing class has to supply a mechanism to make
   * them persistent, so they can be accessed across several sessions.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  public String getApplicationProperty( String key ) {
    return applicationProps.getProperty( key );
  }

  /**
   * Retrieve a system property. System Properties are read only properties used to
   * configure non-application specific system behaviour, such as database connection parameters.
   *
   * @param key The key identifying this property.
   * @return A property bound to that key or null if none has been defined.
   */
  public String getSystemProperty(String key) {
    String value = System.getProperty( key );
    if (value == null) {
      value = systemProps.getProperty( key );
    }
    return value;
  }

  /**
   * Set a system wide property. The system has to ensure properties are made persistent.
   *
   * @param key The key identifying this property.
   * @param value The value to be bound to that key.
   * @param save indicates if this property should be made persistent immediately or not.
   * @throws IOException If properties could not be saved in case <code>save</code> is
   *           set to <code>true</code>.
   */
  public void setProperty(String key, String value, boolean save) throws IOException {
    applicationProps.put( key, value );
    if (save) {
      saveProperties();
    }
  }

  /**
   * Commit all system properties to some persistent storage, e.g. file system.
   *
   * @throws IOException If properties could not be saved.
   */
  public void saveProperties() throws IOException {
    OutputStream fout = systemStorage.write( APPLICATION_PROPFILE, true, false );
    applicationProps.save( fout, "APPLICATION PROPERTY FILE ------------" );
    fout.close();
  }

  /**
   * Returns properties containing language specific properties
   *
   * @param propFileName base file name of properties
   * @param language     language short name, e.g. "de" or "en"
   * @return properties or null if no properties existing for given propFileName and language
   */
  @Override
  public Properties getLanguageProps( String propFileName, String language ) {
    if (language == null) {
      language = Locale.getDefault().getLanguage();
    }
    String path = "lang/" + language + "/" + propFileName+ ".properties";
    try {
      android.content.Context androidContext = AndroidApplication.getInstance();
      InputStream langPropStream = androidContext.getAssets().open( path );
      if (langPropStream == null) {
        Logger.warn( "Language properties not found, resource path='"+path+"'" );
      }
      else {
        Properties langProps = new Properties();
        langProps.load( langPropStream );
        return langProps;
      }
    }
    catch (Exception ex) {
      Logger.warn( "Error loading properties from resource '"+path+"'" );
    }
    return null;
  }

  /**
   * Perform some cleanup and exit runtime system.
   */
  public void exit() {
    doExit();
    java.lang.System.exit( 0 );
  }

  /**
   * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
   * timezone=Germany/Berlin
   * @return the Calender instance
   */
  public Calendar getCalendar() {
    String timeZoneString = getProperty( "timezone", "Europe/Berlin" );
    String localeLanguage = getProperty("localeLanguage", "de");
    String localeCountry = getProperty("localeCountry", "DE");

    TimeZone timeZone = TimeZone.getTimeZone( timeZoneString );
    Locale locale = new Locale( localeLanguage, localeCountry );
    Calendar calendar = Calendar.getInstance( timeZone, locale );

    if (LOG_CALENDARINFO) {
      Logger.info( "----- available TimeZones" );
      String[] availableTimeZones = TimeZone.getAvailableIDs();
      for (int i = 0; i<availableTimeZones.length; i++) {
        Logger.info( availableTimeZones[i] );
      }

      Logger.info( "----- available Locales" );
      Locale[] availableLocales = Locale.getAvailableLocales();
      for (int i=0; i<availableLocales.length; i++) {
        Logger.info( availableLocales[i].toString() );
      }
    }

    if (calendar == null) {
      Logger.warn( "No Calendar instance found for ["+timeZoneString+"/"+localeLanguage+"_"+localeCountry+"]" );
    }
    else if (DEBUG) {
      Logger.info( "Use Calendar ["+calendar.toString()+"]" );
    }
    return calendar;
  }

  @Override
  public WifiManager getWifiManager() {
    if (wiFiManager == null) {
      wiFiManager = new WiFiManagerAndroid();
    }
    return wiFiManager;
  }

  @Override
  public boolean sendMail( String mailAddr, String title, String body, List<FileOwner> attachmentProviderList ) {
    try {
      Intent emailIntent = new Intent( Intent.ACTION_SEND );
      // set the type to 'email'
      emailIntent.setType( "vnd.android.cursor.dir/email" );
      if (mailAddr != null) {
        String[] to = mailAddr.split( ";" );
        emailIntent.putExtra( Intent.EXTRA_EMAIL, to );
      }
      if (title != null) {
        emailIntent.putExtra( Intent.EXTRA_SUBJECT, title );
      }
      if (attachmentProviderList != null) {
        ArrayList<String> pathList = new ArrayList<>();
        for (FileOwner attachmentProvider : attachmentProviderList) {
          pathList.addAll( attachmentProvider.getOwnedFiles() );
        }
        if (pathList.size() == 1) {
          emailIntent.putExtra( Intent.EXTRA_STREAM, pathList.get( 0 ) );
        }
        else if (pathList.size() > 0) {
          emailIntent.putStringArrayListExtra( Intent.EXTRA_STREAM, pathList );
        }
      }
      AndroidApplication.getInstance().getCurrentActivity().startActivity( Intent.createChooser( emailIntent, "Send email..." ) );
      return true;
    }
    catch (Exception ex) {
      return false;
    }
  }
}
