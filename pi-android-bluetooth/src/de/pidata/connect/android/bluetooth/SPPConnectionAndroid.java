/*
 * This file is part of PI-Mobile Android (https://gitlab.com/pi-mobile/pi-mobile-android).
 * Copyright (C) 2011-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.connect.android.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import de.pidata.connect.base.AbstractConnection;
import de.pidata.connect.base.ConnectionListener;
import de.pidata.connect.bluetooth.SPPConnection;
import de.pidata.connect.stream.StreamHandler;
import de.pidata.connect.stream.StreamReceiver;
import de.pidata.log.Logger;
import de.pidata.models.tree.Context;
import de.pidata.stream.StreamHelper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.UUID;

public class SPPConnectionAndroid extends AbstractConnection implements SPPConnection {

  public static final int READ_LOOP_IDLE_SLEEP_MILLIS = 50;
  public static final int TIMEOUT_MILLIS = -1;

  //--- List of connection steps
  public static final String STEP_INIT_ADAPTER = "Init Adapter";
  public static final String STEP_CONNECT_DEVICE = "Connect BT Device";
  public static final String STEP_CREATE_SOCKET = "Create SPP Socket";
  public static final String STEP_OPEN_SOCKET = "Open SPP Socket";
  public static final String STEP_OPEN_STREAM = "Open SPP Stream";
  public static final String STEP_START_RECEIVE = "Start Receive";

  //vector containing the devices discovered
  private String connectionURL = null;
  private String uuidString;
  private BluetoothDevice bluetoothDevice;
  private BluetoothSocket sock = null;
  private StreamHandler streamHandler;
  private StreamReceiver streamReceiver;
  private InputStream inputStream;
  private OutputStream outputStream;
  private ResourceBundle resourceBundle;
  private String clientID = null;

  public SPPConnectionAndroid( String connectionURL, String uuidString, StreamHandler streamHandler ) {
    this.connectionURL = connectionURL;
    this.uuidString = uuidString;
    this.streamHandler = streamHandler;
    this.resourceBundle = ResourceBundle.getBundle( "connectionSteps", Locale.GERMAN );
    resetConnectionSteps();
  }

  /**
   * Returns unique ID for this connection, e.g. URL plus counter
   *
   * @return unique ID for this connection
   */
  @Override
  public String getConnectionID() {
    return connectionURL;
  }

  /**
   * Called by connectionListener to get all connection steps in order.
   * Implementation has to call connectionListener.addStep() in correct
   * order for each step.
   *
   * The list of steps are exact that steps passed when establishing a
   * connection. While connection each registered listener will be called
   * on success or failure of these steps.
   *
   * @param connectionListener listener to add steps to
   */
  @Override
  public void getConnectionSteps( ConnectionListener connectionListener ) {
    connectionListener.addStep( this, STEP_INIT_ADAPTER );
    connectionListener.addStep( this, STEP_CONNECT_DEVICE );
    connectionListener.addStep( this, STEP_CREATE_SOCKET );
    connectionListener.addStep( this, STEP_OPEN_SOCKET );
    connectionListener.addStep( this, STEP_OPEN_STREAM );
    connectionListener.addStep( this, STEP_START_RECEIVE );
  }

  public void listDevices() throws IOException {
    throw new RuntimeException( "TODO" );
    //find devices
  }

  @Override
  public String getDeviceName() {
    String deviceAlias = bluetoothDevice.getName();
    try {
      Method method = bluetoothDevice.getClass().getMethod( "getAliasName" );
      if (method != null) {
        deviceAlias = (String) method.invoke( bluetoothDevice );
      }
    }
    catch (Exception e) {
      Logger.error( "Internal error reading device name", e );
    }
    return deviceAlias;
  }

  public BluetoothDevice getDevice() {
    return bluetoothDevice;
  }

  public String connect() throws IOException {
    //----- Connect to connectionURL
    try {
      resetConnectionSteps();
      startConnectionStep( STEP_INIT_ADAPTER, "Processing..." );
      final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
      clientID = btAdapter.getAddress();
      startConnectionStep( STEP_CONNECT_DEVICE, connectionURL );
      bluetoothDevice = btAdapter.getRemoteDevice(connectionURL);
      successConnectionStep( bluetoothDevice.getName() + "("+bluetoothDevice.getAddress() + ")" );
      Logger.debug( "SPPConnectionAndroid init, local device address=" + bluetoothDevice.getAddress() + ", name=" + bluetoothDevice.getName() );
      /*
       * Establish Bluetooth connection
       *
       * Because discovery is a heavyweight procedure for the Bluetooth adapter,
       * this method should always be called before attempting to connect to a
       * remote device with connect(). Discovery is not managed by the Activity,
       * but is run as a system service, so an application should always call
       * cancel discovery even if it did not directly request a discovery, just to
       * be sure. If Bluetooth state is not STATE_ON, this API will return false.
       *
       * see
       * http://developer.android.com/reference/android/bluetooth/BluetoothAdapter
       * .html#cancelDiscovery()
       */
      Logger.debug( "Stopping Bluetooth discovery." );
      while (btAdapter.isDiscovering()) {
        btAdapter.cancelDiscovery();
        if (btAdapter.isDiscovering()) {
          Thread.sleep( 50 );
        }
      }
    }
    catch (Exception ex) {
      errorConnectionStep( ex.getMessage() );
      throw new IOException( "Could not initialize Bluetooth", ex );
    }

    //----- Open SPP connection
    Logger.info( "Starting Bluetooth SPP connection...");
    startConnectionStep( STEP_CREATE_SOCKET, uuidString );
    try {
      // Instantiate a BluetoothSocket for the remote device and connect it.
      sock = bluetoothDevice.createRfcommSocketToServiceRecord( UUID.fromString( uuidString ) );
    }
    catch (Exception ex) {
      errorConnectionStep( ex.getMessage(), resourceBundle.getString( "spp.activateBluetoothSource" ) );
      throw new IOException( "There was an error while establishing Bluetooth connection", ex );
    }
    if (sock == null) {
      errorConnectionStep( "Got NULL socket" );
      throw new IOException( "Got NULL socket while establishing Bluetooth connection" );
    }
    else {
      startConnectionStep( STEP_OPEN_SOCKET, "Normal" );
      try{
        sock.connect();
        Logger.info( "Bluetooth SPP connected." );
      }
      catch (Exception e1) {
        Logger.info( "There was an error while establishing Bluetooth connection. Falling back.. msg="+e1.getMessage() );
        startConnectionStep( STEP_OPEN_SOCKET, "Fallback" );
        Class<?> clazz = sock.getRemoteDevice().getClass();
        Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
        try {
          Method m = clazz.getMethod( "createRfcommSocket", paramTypes );
          Object[] params = new Object[]{Integer.valueOf( 1 )};
          BluetoothSocket sockFallback = (BluetoothSocket) m.invoke( sock.getRemoteDevice(), params );
          sockFallback.connect();
          sock = sockFallback;
          successConnectionStep( null );
        }
        catch (Exception e2) {
          Logger.info( "Couldn't fallback while establishing Bluetooth connection. Canceling connection.. msg="+e2.getMessage() );
          errorConnectionStep( e2.getMessage(), resourceBundle.getString( "spp.activateBluetoothTarget" ) );
          disconnect();
          throw new IOException( "Could not connect to Bluetooth", e2 );
        }
      }
    }
    return clientID;
  }

  @Override
  public OutputStream getOutputStream() throws IOException {
    if (outputStream == null) {
      outputStream = sock.getOutputStream();
    }
    return outputStream;
  }

  public void disconnect() {
    close();
  }

  public boolean isConnected() {
    return (sock != null);
  }

  @Override
  public void start() throws IOException {
    Logger.info( "SPPConnectionAndroid["+getConnectionID()+"]: connect to address=" + bluetoothDevice.getAddress() + ", name=" + bluetoothDevice.getName() );
    startConnectionStep( STEP_OPEN_STREAM, "Processing..." );
    inputStream = sock.getInputStream();
    startConnectionStep( STEP_START_RECEIVE, "Processing..." );
    streamReceiver = new StreamReceiver( this, READ_LOOP_IDLE_SLEEP_MILLIS, TIMEOUT_MILLIS );
    streamReceiver.startReceive( getDevice().getName(), inputStream, streamHandler );
    successConnectionStep( null );
  }

  /**
   * Schließt die Verbindung --> STATE_CLOSED
   */
  @Override
  public void close() {
    if (streamReceiver != null) {
      streamReceiver.stop();
      streamReceiver = null;
    }
    StreamHelper.close( inputStream );
    inputStream = null;
    StreamHelper.close( outputStream );
    outputStream = null;
    try {
      if (sock != null) {
        sock.close();
      }
    }
    catch (Exception ex) {
      Logger.error( "Exception closing bluetooth socket", ex );
    }
    sock = null;
    setStateDisconnected();
  }
}